# VEGAS JS System OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.0] - 2018-03-07
### Added
* First version based on VEGAS JS 1.0.17
* Unit tests based on Webpack 3.11.0, Mocha and Istanbul

