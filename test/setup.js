const jsdom = require("jsdom");

const { JSDOM } = jsdom;

const { window } = new JSDOM(`...`);

global.window   = window ;
global.document = window.document;

window.console = global.console;

Object.keys(document.defaultView).forEach((property) =>
{
    if (typeof global[property] === 'undefined')
    {
        global[property] = document.defaultView[property];
    }
});

global.navigator =
{
    userAgent: 'node.js'
};