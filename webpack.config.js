const path = require( 'path' ) ;
var nodeExternals = require('webpack-node-externals');

module.exports =
{
    // mode    : 'development' , // Webpack 4 only
    watch     : false ,
    target    : 'node' ,
    externals :
    [
        nodeExternals()
    ],
    resolve   :
    {
        alias :
        {
            'core'     : path.resolve( __dirname, './node_modules/vegas-js-core/src/core/') ,
            'polyfill' : path.resolve( __dirname, './node_modules/vegas-js-polyfill/src/polyfill/') ,
            'signals'  : path.resolve( __dirname, './node_modules/vegas-js-signals/src/system/signals') ,
            'system'   : path.resolve( __dirname, './src/system/' )
        }
    },
    stats : { colors : true } ,
    module:
    {
        loaders : // rules on webpack 4
        [
            {
                loader : "babel-loader" ,
                test   : /\.js$/
            }
        ]
    }
};