# VEGAS JS | System

**Vegas JS | System** - **version 1.0.0** is an *Opensource* Library based on **ECMAScript** for develop crossplatform **Rich Internet Applications** and **Games**.

This library is the root package for the **VEGAS JS** framework. It is the starting point of our RIA framework structure : signals, data, IoC, logger, tasks, transitions, logics, rules, models, etc.

### About

 * Author : Marc ALCARAZ (aka eKameleon) - Creative Technologist and Digital Architect
 * Mail : ekameleon[at]gmail.com
 * LinkedIn : [https://www.linkedin.com/in/ekameleon/](https://www.linkedin.com/in/ekameleon/)

### License

Under tree opensource licenses :

 * [License MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
 * [License GPL 2.0+](https://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1+](https://www.gnu.org/licenses/lgpl-2.1.html)

## Resources

#### ⌜ Download

Download on **Bitbucket** the latest code, report an issue, ask a question or contribute :

 * [https://bitbucket.org/ekameleon/vegas-js-system](https://bitbucket.org/ekameleon/vegas-js-system)

#### ⌜ Documentation

Get started with the the **Vegas JS** API :

 * [https://vegasjs.ooopener.com/](https://vegasjs.ooopener.com/)

#### ⌜ Slack Community

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the **VEGAS** community on Slack !

## Dependencies

 * [VEGAS JS Polyfill](https://bitbucket.org/ekameleon/vegas-js-polyfill)
 * [VEGAS JS CORE](https://bitbucket.org/ekameleon/vegas-js-core)
 * [VEGAS JS Signals](https://bitbucket.org/ekameleon/vegas-js-signals)

## Install

#### ⌜ YARN / NPM

You can install VEGAS JS with [Yarn](https://yarnpkg.com/) or [NPM](https://www.npmjs.com/package/vegas-js-system).

```console
yarn add vegas-js-system
```

or

```console
npm install vegas-js-system
```

## Example

1 - Load the *./dist/vegas-system.js* library in your HTML page.

The *index.html* file :
```html
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Test VEGAS JS | System</title>
    </head>
    <body>
        <script src="./js/vegas-system.js"></script>
        <script src="./js/index.js"></script>
    </body>
</html>
```

2 - Use the system package in your javascript application. 

The *index.js* file :

```javascript
"use strict" ;

window.onload = function()
{
    if( !system )
    {
        throw new Error("The VEGAS JS - System library is undefined.") ;
    }
    
    const signals = system.signals ;
    
    // ------

    function Slot( name )
    {
        this.name = name ;
    }

    Slot.prototype = Object.create( signals.Receiver.prototype );
    Slot.prototype.constructor = Slot;

    Slot.prototype.receive = function ( message )
    {
        console.log( this + " : " + message ) ;
    };

    Slot.prototype.toString = function ()
    {
        return "[Slot name:" + this.name + "]" ;
    };

    // ------

    let slot1 = new Slot("slot1") ;

    let slot2 = function( message )
    {
        console.log( this + " : " + message ) ;
    };

    let signal = new signals.Signal() ;

    //signal.proxy = slot1 ;

    signal.connect( slot1 , 0 ) ;
    signal.connect( slot2 , 2 ) ;

    console.log( "signal.connected : " + signal.connected() ) ;
    console.log( "signal.length : "    + signal.length ) ;
    console.log( "signal.hasReceiver(slot1) : " + signal.hasReceiver(slot1) ) ;
    console.log( "signal.hasReceiver(slot2) : " + signal.hasReceiver(slot2) ) ;

    signal.emit( "hello world" ) ;

    signal.disconnect( slot1 ) ;

    signal.emit( "Bonjour monde" ) ;
};
```

## Building and test the libraries

**VEGAS JS | Signals** use [Yarn](https://yarnpkg.com/) with a set of powerful packages (Babel, Mocha, etc.) to compile and build this library.

#### ⌜ Simple Build

1 - The first time, initialize the project and run yarn :
```console
yarn
```

2 - Run the Unit Tests + Compile all the libraries + Generates the documentation :
```console
yarn build
```

#### ⌜ VEGAS (only) Build

1 - Build the **./dist/vegas-system.js** : not minified + no comments + sourcemap.
```console
yarn dev
```

2 - Build the **./dist/vegas-system.js** and watch the changing into the **./src** folder.
```console
yarn watch
```

3 - Build the **./dist/vegas-system.min.js** : minified + no comments.
```console
yarn prod
```

#### ⌜ Unit tests

We use the [Mocha](https://mochajs.org) and the Chai (http://chaijs.com/) tools to run the unit tests of the VEGAS JS libraries.

1 - Run all unit tests
```
$ yarn test
```

2 - Run a specific library, use one of this command :

The **--match** option trigger the unit test engine (based on **[Mocha](https://mochajs.org/)**) to only run tests matching the given pattern which is internally compiled to a RegExp, for examples :

```
$ yarn test -g system.signals.Receiver
```

The **--reporter** option define the unit test result rendering in the terminal with the values : 'spec', 'dot', 'landing', 'dot', 'nyan', 'list', 'mochawesome'. By default the 'spec' value is used.

```console
yarn test --reporter nyan
```
![vegas-js-signals-nyan.png](https://bitbucket.org/repo/7ErBBx6/images/979441029-vegas-js-signals-nyan.png)

3 - Cover the library source code with [Istanbul](https://istanbul.js.org/) :
```console
yarn cover
```

#### ⌜ Generates the documentation

The documentation of the framework is based on [JSDoc](http://usejsdoc.org/).

Run the documentation build with gulp :
```console
yarn doc
```

The documentation is generated in the folder : **./docs/bin**

## History

 * 1998 : Flash
 * 2000 : First framework concept and first libraries (components, tools, design patterns)
 * 2004 : First official SVN repository
 * 2007 : Fusion with the Maashaack framework (eden, etc.)
 * 2015 : Google Code must die - **VEGAS** move from an old Google Code SVN repository to this Bitbucket GIT repository and REBOOT this source code.
 * 2016 : Begin the new JS architecture of the VEGAS JS library based on ES6
 * 2018 : Cut the JS packages of VEGAS in a set of independent libraries.
 