"use strict" ;

import 'polyfill/index.js' ;

import { ucFirst } from 'core/strings/ucFirst.js' ;

/**
 * The vegas-signals JS library.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
export { Enum }        from './system/Enum.js' ;
export { Equatable }   from './system/Equatable.js' ;
export { Evaluable }   from './system/Evaluable.js' ;
export { Formattable } from './system/Formattable.js' ;

export { isEvaluable   } from './system/Evaluable.js' ;
export { isFormattable } from './system/Formattable.js' ;

export { data }        from './system/data.js' ;
export { errors }      from './system/errors.js' ;
export { evaluators }  from './system/evaluators.js' ;
export { events }      from './system/events.js' ;
export { formatters }  from './system/formatters.js' ;
export { ioc }         from './system/ioc.js' ;
export { logging }     from './system/logging.js' ;
export { logics }      from './system/logics.js' ;
export { models }      from './system/models.js' ;
export { numeric }     from './system/numeric.js' ;
export { process }     from './system/process.js' ;
export { rules }       from './system/rules.js' ;
export { signals }     from './system/signals.js' ;
export { transitions } from './system/transitions.js' ;


/**
 * The string expression of the current VEGAS version.
 * @name version
 * @type string
 * @global
 */
export const version = '<@VERSION@>' ;

/**
 * The metadatas object to describe the <b>VEGAS JS</b> framework.
 * @name metas
 * @property {string} name - The name of the library
 * @property {string} description - The description of the library
 * @property {string} version - The version of the library
 * @property {string} license - The license of the library
 * @property {string} url - The url of the library
 * @type Object
 * @global
 * @example
 * trace( core.dump( metas ) ) ;
 */
export const metas = Object.defineProperties( {} ,
{
    name        : { enumerable : true , value : ucFirst('<@NAME@>') } ,
    description : { enumerable : true , value : "<@DESCRIPTION@>" },
    version     : { enumerable : true , value : version } ,
    license     : { enumerable : true , value : "<@LICENSE@>" } ,
    url         : { enumerable : true , value : '<@HOMEPAGE@>' }
});
