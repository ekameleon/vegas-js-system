"use strict" ;

import { Property } from './Property.js' ;

/**
 * Defines a basic <b>method</b> definition.
 * @summary Defines a basic <b>method</b> definition.
 * @name Method
 * @class
 * @memberof system.data
 * @implements system.data.Property
 * @param {string} name The name of the method.
 * @param {array} [args=null] The optional array of arguments of the method.
 * @see system.data.Attribute
 * @see system.process.Cache
 */
export function Method( name = null , args = null )
{
    /**
     * The name of the method.
     * @name name
     * @memberof system.data.Method
     * @type {string}
     */
    this.name = (name instanceof String || typeof(name) === 'string') ? name : null ;

    /**
     * The optional array of arguments of the method.
     * @name args
     * @memberof system.data.Method
     * @type {array}
     */
    this.args = (args instanceof Array) ? args : null ;
}

Method.prototype = Object.create( Property.prototype );
Method.prototype.constructor = Method;
Method.prototype.toString = function () { return "[Method]" ; };