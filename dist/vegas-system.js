/**
 * The system package is the root for the VEGAS JS framework. It is the starting point of our RIA framework structure : signals, W3C events, datas and collections (ADT), IoC container (Dependency Injection), logger, tasks, transitions, logics, rules, models, etc. - version: 1.0.0 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.system = {})));
}(this, (function (exports) { 'use strict';

if (!(Date.now && Date.prototype.getTime))
{
    Date.now = function now()
    {
        return new Date().getTime();
    };
}

if ( !Function.prototype.bind )
{
    Function.prototype.bind = function (oThis)
    {
        if (typeof this !== "function")
        {
            throw new TypeError( 'Function.prototype.bind called on incompatible ' + this );
        }
        let aArgs = Array.prototype.slice.call(arguments, 1),
           fToBind = this,
              fNOP = function () {},
            fBound = function ()
            {
                return fToBind.apply
                (
                    this instanceof fNOP ? this : oThis ,
                    aArgs.concat( Array.prototype.slice.call(arguments) )
                );
             };
        if ( this.prototype )
        {
            fNOP.prototype = this.prototype;
        }
        fBound.prototype = new fNOP();
        return fBound;
  };
}
if ( Function.prototype.name === undefined )
{
    Object.defineProperty( Function.prototype, 'name',
    {
        get : function ()
        {
            return this.toString().match( /^\s*function\s*(\S*)\s*\(/ )[ 1 ];
        }
    } );
}

if ( Math.sign === undefined )
{
    Math.sign = function ( x )
    {
        return ( x < 0 ) ? - 1 : ( x > 0 ) ? 1 : + x;
    };
}

if ( Object.assign === undefined )
{
    ( function () {
        Object.assign = function ( target )
        {
            if ( target === undefined || target === null )
            {
                throw new TypeError( 'Cannot convert undefined or null to object' );
            }
            let output = Object( target );
            for ( let index = 1; index < arguments.length; index ++ )
            {
                let source = arguments[ index ];
                if ( source !== undefined && source !== null )
                {
                    for ( let nextKey in source )
                    {
                        if ( Object.prototype.hasOwnProperty.call( source, nextKey ) )
                        {
                            output[ nextKey ] = source[ nextKey ];
                        }
                    }
                }
            }
            return output;
        };
    } )();
}

(function( global )
{
    if( typeof global.Uint32Array !== "function" )
    {
        let CheapArray = function(type)
        {
            let proto = [];
            global[type] = function( arg )
            {
                let i;
                if (typeof(arg) === "number")
                {
                    Array.call(this, arg);
                    this.length = arg;
                    for ( i = 0 ; i < this.length; i++)
                    {
                        this[i] = 0;
                    }
                }
                else
                {
                    Array.call( this , arg.length );
                    this.length = arg.length;
                    for ( i = 0; i < this.length ; i++ )
                    {
                        this[i] = arg[i];
                    }
                }
            };
            global[type].prototype = proto;
            global[type].constructor = global[type];
        };
        CheapArray('Float32Array');
        CheapArray('Uint32Array');
        CheapArray('Uint16Array');
        CheapArray('Int16Array');
        CheapArray('ArrayBuffer');
    }
})( window || global || {} ) ;

function ucFirst( str )
{
    if( !(str instanceof String || typeof(str) === 'string' ) || str === "" )
    {
        return '' ;
    }
    return str.charAt(0).toUpperCase() + str.substring(1) ;
}

function Enum(value, name) {
    Object.defineProperties(this, {
        _name: {
            value: typeof name === "string" || name instanceof String ? name : "",
            enumerable: false,
            writable: true,
            configurable: true
        },
        _value: {
            value: isNaN(value) ? 0 : value,
            enumerable: false,
            writable: true,
            configurable: true
        }
    });
}
Enum.prototype = Object.create(Object.prototype);
Enum.prototype.constructor = Enum;
Enum.prototype.equals = function (object) {
    if (object === this) {
        return true;
    }
    if (object instanceof Enum) {
        return object.toString() === this.toString() && object.valueOf() === this.valueOf();
    }
    return false;
};
Enum.prototype.toString = function () {
    return this._name;
};
Enum.prototype.valueOf = function () {
    return this._value;
};

function isEquatable(target) {
  if (target) {
    return target.equals && target.equals instanceof Function || target instanceof Equatable;
  }
  return false;
}
function Equatable() {}
Equatable.prototype = Object.create(Object.prototype);
Equatable.prototype.constructor = Equatable;
Equatable.prototype.equals = function (object) {
};

function isEvaluable(target) {
  if (target) {
    return target instanceof Evaluable || 'eval' in target && target.eval instanceof Function;
  }
  return false;
}
function Evaluable() {}
Evaluable.prototype = Object.create(Object.prototype, {
  constructor: { writable: true, value: Evaluable },
  eval: { writable: true, value: function value(_value) {} },
  toString: { writable: true, value: function value() {
      return '[' + this.constructor.name + ']';
    } }
});

function isFormattable(target) {
  if (target) {
    return target instanceof Formattable || 'format' in target && target.format instanceof Function;
  }
  return false;
}
function Formattable() {}
Formattable.prototype = Object.create(Object.prototype);
Formattable.prototype.constructor = Formattable;
Formattable.prototype.format = function (value) {};

function isIdentifiable(target) {
    if (target) {
        return target instanceof Identifiable || 'id' in target;
    }
    return false;
}
function Identifiable() {
    Object.defineProperties(this, {
        id: { value: null, writable: true }
    });
}
Identifiable.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Identifiable }
});

function isIterator(target) {
  if (target) {
    return target instanceof Iterator ||
    Boolean(target['delete']) && target.delete instanceof Function && Boolean(target['hasNext']) && target.hasNext instanceof Function && Boolean(target['key']) && target.key instanceof Function && Boolean(target['next']) && target.next instanceof Function && Boolean(target['reset']) && target.reset instanceof Function && Boolean(target['seek']) && target.seek instanceof Function
    ;
  }
  return false;
}
function Iterator() {}
Iterator.prototype = Object.create(Object.prototype, {
  constructor: { writable: true, value: Iterator },
  delete: { writable: true, value: function value() {} },
  hasNext: { writable: true, value: function value() {} },
  key: { writable: true, value: function value() {} },
  next: { writable: true, value: function value() {} },
  reset: { writable: true, value: function value() {} },
  seek: { writable: true, value: function value(position) {} },
  toString: {
    writable: true, value: function value() {
      return '[' + this.constructor.name + ']';
    }
  }
});

function isOrderedIterator(target) {
  var bool = false;
  if (target) {
    bool = target instanceof OrderedIterator || 'hasNext' in target && target.hasNext instanceof Function && 'hasPrevious' in target && target.hasPrevious instanceof Function && 'key' in target && target.key instanceof Function && 'next' in target && target.next instanceof Function && 'previous' in target && target.previous instanceof Function && 'remove' in target && target.remove instanceof Function && 'reset' in target && target.reset instanceof Function && 'seek' in target && target.seek instanceof Function;
  }
  return bool;
}
function OrderedIterator() {}
OrderedIterator.prototype = Object.create(Iterator.prototype);
OrderedIterator.prototype.constructor = OrderedIterator;
OrderedIterator.prototype.hasPrevious = function () {};
OrderedIterator.prototype.previous = function () {};
OrderedIterator.prototype.toString = function () {
  return '[OrderedIterator]';
};

function isValidator(target) {
  if (target) {
    if (target instanceof Validator) {
      return true;
    }
    return 'supports' in target && target.supports instanceof Function && 'validate' in target && target.validate instanceof Function;
  }
  return false;
}
function Validator() {}
Validator.prototype = Object.create(Object.prototype);
Validator.prototype.constructor = Validator;
Validator.prototype.supports = function (value) {};
Validator.prototype.validate = function (value) {};

function KeyValuePair() {}
KeyValuePair.prototype = Object.create(Object.prototype, {
  constructor: { writable: true, value: KeyValuePair },
  length: { get: function get() {
      return 0;
    } },
  clear: { value: function value() {}, writable: true },
  clone: { value: function value() {
      return new KeyValuePair();
    }, writable: true },
  copyFrom: { value: function value(map) {}, writable: true },
  delete: { value: function value(key) {}, writable: true },
  forEach: { value: function value(callback) {
      
    }, writable: true },
  get: { value: function value(key) {
      return null;
    }, writable: true },
  has: { value: function value(key) {
      return false;
    }, writable: true },
  hasValue: { value: function value(_value) {
      return false;
    }, writable: true },
  isEmpty: { value: function value() {
      return false;
    }, writable: true },
  iterator: { value: function value()             {
      return null;
    }, writable: true },
  keyIterator: { value: function value()             {
      return null;
    }, writable: true },
  keys: { value: function value() {
      return null;
    }, writable: true },
  set: { value: function value(key, _value2) {}, writable: true },
  toString: { value: function value() {
      return '[' + this.constructor.name + ']';
    }, writable: true },
  values: { value: function value() {}, writable: true }
});

function Property() {}
Property.prototype = Object.create(Object.prototype);
Property.prototype.constructor = Property;

function Attribute() {
  var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
  this.name = name instanceof String || typeof name === 'string' ? name : null;
  this.value = value;
}
Attribute.prototype = Object.create(Property.prototype);
Attribute.prototype.constructor = Attribute;
Attribute.prototype.toString = function () {
  return "[Attribute]";
};

function Method() {
  var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  this.name = name instanceof String || typeof name === 'string' ? name : null;
  this.args = args instanceof Array ? args : null;
}
Method.prototype = Object.create(Property.prototype);
Method.prototype.constructor = Method;
Method.prototype.toString = function () {
  return "[Method]";
};

function ValueObject() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    Identifiable.call(this);
    if (init) {
        this.setTo(init);
    }
}
ValueObject.prototype = Object.create(Identifiable.prototype, {
    constructor: { writable: true, value: ValueObject },
    formatToString: { value: function value(className) {
            if (!className) {
                if (this._constructorName === undefined) {
                    Object.defineProperties(this, { _constructorName: { value: this.constructor.name } });
                }
                className = this._constructorName;
            }
            var ar = [className];
            for (var _len = arguments.length, rest = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                rest[_key - 1] = arguments[_key];
            }
            var len = rest.length;
            for (var i = 0; i < len; ++i) {
                if (rest[i] in this) {
                    ar.push(rest[i] + ":" + this[rest[i]]);
                }
            }
            return "[" + ar.join(' ') + "]";
        } },
    setTo: { writable: true, value: function value(init) {
            if (init) {
                for (var prop in init) {
                    if (prop in this) {
                        this[prop] = init[prop];
                    }
                }
            }
            return this;
        } },
    toString: { writable: true, value: function value() {
            return this.formatToString(null);
        } }
});

function ArrayIterator(array) {
    if (!(array instanceof Array)) {
        throw new ReferenceError(this + " constructor failed, the passed-in Array argument not must be 'null'.");
    }
    Object.defineProperties(this, {
        _a: { value: array, writable: true },
        _k: { value: -1, writable: true }
    });
}
ArrayIterator.prototype = Object.create(Iterator.prototype, {
    constructor: { value: ArrayIterator },
    delete: { value: function value() {
            return this._a.splice(this._k--, 1)[0];
        } },
    hasNext: { value: function value() {
            return this._k < this._a.length - 1;
        } },
    key: { value: function value() {
            return this._k;
        } },
    next: { value: function value() {
            return this._a[++this._k];
        } },
    reset: { value: function value() {
            this._k = -1;
        } },
    seek: { value: function value(position) {
            position = Math.max(Math.min(position - 1, this._a.length), -1);
            this._k = isNaN(position) ? -1 : position;
        } }
});

function MapIterator(map) {
    if (map && map instanceof KeyValuePair) {
        Object.defineProperties(this, {
            _m: { value: map, writable: true },
            _i: { value: new ArrayIterator(map.keys()), writable: true },
            _k: { value: null, writable: true }
        });
    } else {
        throw new ReferenceError(this + " constructor failed, the passed-in KeyValuePair argument not must be 'null'.");
    }
}
MapIterator.prototype = Object.create(Iterator.prototype, {
    constructor: { writable: true, value: MapIterator },
    delete: { value: function value() {
            this._i.delete();
            return this._m.delete(this._k);
        } },
    hasNext: { value: function value() {
            return this._i.hasNext();
        } },
    key: { value: function value() {
            return this._k;
        } },
    next: { value: function value() {
            this._k = this._i.next();
            return this._m.get(this._k);
        } },
    reset: { value: function value() {
            this._i.reset();
        } },
    seek: { value: function value(position) {
            throw new Error("This Iterator does not support the seek() method.");
        } }
});

function MapEntry(key, value) {
  Object.defineProperties(this, {
    key: { value: key, writable: true },
    value: { value: value, writable: true }
  });
}
MapEntry.prototype = Object.create(Object.prototype, {
  constructor: { value: MapEntry },
  clone: { value: function value() {
      return new MapEntry(this.key, this.value);
    } },
  toString: { value: function value() {
      return "[MapEntry key:" + this.key + " value:" + this.value + "]";
    } }
});

function MapFormatter() {}
MapFormatter.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: MapFormatter },
    format: { value: function value(_value) {
            if (_value instanceof KeyValuePair) {
                var r = "{";
                var keys = _value.keys();
                var len = keys.length;
                if (len > 0) {
                    var values = _value.values();
                    for (var i = 0; i < len; i++) {
                        r += keys[i] + ':' + values[i];
                        if (i < len - 1) {
                            r += ",";
                        }
                    }
                }
                r += "}";
                return r;
            } else {
                return "{}";
            }
        } }
});
var formatter = new MapFormatter();

function ArrayMap() {
    var keys = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var values = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    Object.defineProperties(this, {
        _keys: {
            value: [],
            writable: true
        },
        _values: {
            value: [],
            writable: true
        }
    });
    if (keys === null || values === null) {
        this._keys = [];
        this._values = [];
    } else {
        var b = keys instanceof Array && values instanceof Array && keys.length > 0 && keys.length === values.length;
        this._keys = b ? [].concat(keys) : [];
        this._values = b ? [].concat(values) : [];
    }
}
ArrayMap.prototype = Object.create(KeyValuePair.prototype, {
    constructor: { writable: true, value: ArrayMap },
    length: { get: function get() {
            return this._keys.length;
        } },
    clear: { value: function value() {
            this._keys = [];
            this._values = [];
        } },
    clone: { value: function value() {
            return new ArrayMap(this._keys, this._values);
        } },
    copyFrom: { value: function value(map) {
            if (!map || !(map instanceof KeyValuePair)) {
                return;
            }
            var keys = map.keys();
            var values = map.values();
            var l = keys.length;
            for (var i = 0; i < l; i = i - -1) {
                this.set(keys[i], values[i]);
            }
        } },
    delete: { value: function value(key) {
            var v = null;
            var i = this.indexOfKey(key);
            if (i > -1) {
                v = this._values[i];
                this._keys.splice(i, 1);
                this._values.splice(i, 1);
            }
            return v;
        } },
    forEach: { value: function value(callback) {
            var thisArg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (typeof callback !== "function") {
                throw new TypeError(callback + ' is not a function');
            }
            var l = this._keys.length;
            for (var i = 0; i < l; i++) {
                callback.call(thisArg, this._values[i], this._keys[i], this);
            }
        } },
    get: { value: function value(key) {
            return this._values[this.indexOfKey(key)];
        } },
    getKeyAt: { value: function value(index) {
            return this._keys[index];
        } },
    getValueAt: { value: function value(index         ) {
            return this._values[index];
        } },
    has: { value: function value(key) {
            return this.indexOfKey(key) > -1;
        } },
    hasValue: { value: function value(_value) {
            return this.indexOfValue(_value) > -1;
        } },
    indexOfKey: { value: function value(key) {
            var l = this._keys.length;
            while (--l > -1) {
                if (this._keys[l] === key) {
                    return l;
                }
            }
            return -1;
        } },
    indexOfValue: { value: function value(_value2) {
            var l = this._values.length;
            while (--l > -1) {
                if (this._values[l] === _value2) {
                    return l;
                }
            }
            return -1;
        } },
    isEmpty: { value: function value() {
            return this._keys.length === 0;
        } },
    iterator: { value: function value() {
            return new MapIterator(this);
        } },
    keyIterator: { value: function value()
        {
            return new ArrayIterator(this._keys);
        } },
    keys: { value: function value() {
            return this._keys.concat();
        } },
    set: { value: function value(key, _value3) {
            var r = null;
            var i = this.indexOfKey(key);
            if (i < 0) {
                this._keys.push(key);
                this._values.push(_value3);
            } else {
                r = this._values[i];
                this._values[i] = _value3;
            }
            return r;
        } },
    setKeyAt: { value: function value(index, key) {
            if (index >= this._keys.length) {
                throw new RangeError("ArrayMap.setKeyAt(" + index + ") failed with an index out of the range.");
            }
            if (this.containsKey(key)) {
                return null;
            }
            var k = this._keys[index];
            if (k === undefined) {
                return null;
            }
            var v = this._values[index];
            this._keys[index] = key;
            return new MapEntry(k, v);
        } },
    setValueAt: { value: function value(index, _value4) {
            if (index >= this._keys.length) {
                throw new RangeError("ArrayMap.setValueAt(" + index + ") failed with an index out of the range.");
            }
            var v = this._values[index];
            if (v === undefined) {
                return null;
            }
            var k = this._keys[index];
            this._values[index] = _value4;
            return new MapEntry(k, v);
        } },
    toString: { value: function value() {
            return formatter.format(this);
        } },
    values: { value: function value() {
            return this._values.concat();
        } }
});

/**
 * The {@link system.data} library provides a framework unified for representing and manipulating <b>collections</b>, enabling them to be manipulated independently of the details of their representation.
 * <p>It reduces programming effort while increasing performance. It enables interoperability among unrelated APIs, reduces effort in designing and learning new APIs, and fosters software reuse.</p>
 * <p>The framework is based on a set of interfaces. It includes implementations of these interfaces and algorithms to manipulate them.</p></br>
 * <p>An <strong>abstract data type</strong> (<b>ADT</b>) is a model for a certain class of data structures that have similar behavior; or for certain data types of one or more programming languages that have similar semantics. The collections framework is a unified architecture for representing and manipulating collections, allowing them to be manipulated independently of the details of their representation. It reduces programming effort while increasing performance.</p>
 * <p>Originally the {@link system.data} collection framework is loosely inspired on the <b>JAVA Collections Framework</b> and the <b>Jakarta Collections Framework</b> but with the new ES6 standard we change the basic implementation of the new <b>VEGAS</b> framework in the JS version of the library.</p>
 * <p>This framework is inspired on interfaces to defines the different types of collections : * Map * Bag * Collections * Iterator * Set * Queue & Stack... </p>
 * @summary The {@link system.data} library provides a framework unified for representing and manipulating <b>collections</b>.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.data
 * @memberof system
 */
var data = Object.assign({
  isIdentifiable: isIdentifiable,
  isIterator: isIterator,
  isOrderedIterator: isOrderedIterator,
  isValidator: isValidator,
  Identifiable: Identifiable,
  Iterator: Iterator,
  KeyValuePair: KeyValuePair,
  OrderedIterator: OrderedIterator,
  Property: Property,
  Validator: Validator,
  Attribute: Attribute,
  Method: Method,
  ValueObject: ValueObject,
  iterators: {
    ArrayIterator: ArrayIterator,
    MapIterator: MapIterator
  },
  maps: {
    ArrayMap: ArrayMap
  }
});

function ConcurrencyError(message, fileName, lineNumber) {
  this.name = 'ConcurrencyError';
  this.message = message || 'concurrency error';
  this.fileName = fileName;
  this.lineNumber = lineNumber;
  this.stack = new Error().stack;
}
ConcurrencyError.prototype = Object.create(Error.prototype);
ConcurrencyError.prototype.constructor = ConcurrencyError;

function InvalidChannelError(message, fileName, lineNumber) {
  this.name = 'InvalidChannelError';
  this.message = message || 'invalid channel error';
  this.fileName = fileName;
  this.lineNumber = lineNumber;
  this.stack = new Error().stack;
}
InvalidChannelError.prototype = Object.create(Error.prototype);
InvalidChannelError.prototype.constructor = InvalidChannelError;

function InvalidFilterError(message, fileName, lineNumber) {
  this.name = 'InvalidFilterError';
  this.message = message || 'invalid filter error';
  this.fileName = fileName;
  this.lineNumber = lineNumber;
  this.stack = new Error().stack;
}
InvalidFilterError.prototype = Object.create(Error.prototype);
InvalidFilterError.prototype.constructor = InvalidFilterError;

function fastformat( pattern , ...args )
{
    if( (pattern === null) || !(pattern instanceof String || typeof(pattern) === 'string' ) )
    {
        return "" ;
    }
    if( args.length > 0 )
    {
        args = [].concat.apply( [] , args ) ;
        let len  = args.length;
        for( var i = 0 ; i < len ; i++ )
        {
            pattern = pattern.replace( new RegExp( "\\{" + i + "\\}" , "g" ), args[i] );
        }
    }
    return pattern;
}

function NonUniqueKeyError(key, pattern, fileName, lineNumber) {
  this.name = 'NonUniqueKeyError';
  this.key = key;
  this.pattern = pattern || NonUniqueKeyError.PATTERN;
  this.message = fastformat(this.pattern, key);
  this.fileName = fileName;
  this.lineNumber = lineNumber;
  this.stack = new Error().stack;
}
NonUniqueKeyError.PATTERN = "attempting to insert the key '{0}'";
NonUniqueKeyError.prototype = Object.create(Error.prototype);
NonUniqueKeyError.prototype.constructor = NonUniqueKeyError;

function NoSuchElementError(message, fileName, lineNumber) {
  this.name = 'NoSuchElementError';
  this.message = message || 'no such element error';
  this.fileName = fileName;
  this.lineNumber = lineNumber;
  this.stack = new Error().stack;
}
NoSuchElementError.prototype = Object.create(Error.prototype);
NoSuchElementError.prototype.constructor = NoSuchElementError;

/**
 * The {@link system.errors} package contains error classes that are part of the <strong>VEGAS JS</strong> Application Programming Interface (<strong>API</strong>), rather than part of the Javascript core language. The <strong>Javascript</strong> core language is the part of the language that complies with the <strong>ECMAScript</strong> standard.
 * @summary The {@link system.errors} package contains error classes that are part of the <strong>VEGAS JS</strong> Application Programming Interface (<strong>API</strong>).
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.errors
 * @memberof system
 */
var errors = Object.assign({
  ConcurrencyError: ConcurrencyError,
  InvalidChannelError: InvalidChannelError,
  InvalidFilterError: InvalidFilterError,
  NonUniqueKeyError: NonUniqueKeyError,
  NoSuchElementError: NoSuchElementError
});

function MultiEvaluator() {
    var elements = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    Object.defineProperties(this, {
        autoClear: { value: false, writable: true },
        _evaluators: { value: [], writable: true }
    });
    if (elements instanceof Array && elements.length > 0) {
        this.add.apply(this, elements);
    }
}
MultiEvaluator.prototype = Object.create(Evaluable.prototype, {
    length: {
        get: function get() {
            return this._evaluators.length;
        }
    },
    add: { value: function value() {
            if (this.autoClear) {
                this.clear();
            }
            for (var _len = arguments.length, evaluators = Array(_len), _key = 0; _key < _len; _key++) {
                evaluators[_key] = arguments[_key];
            }
            var l = evaluators.length;
            if (l > 0) {
                var c, i, j;
                var e;
                for (i = 0; i < l; i++) {
                    e = evaluators[i];
                    if (e instanceof Evaluable) {
                        this._evaluators.push(e);
                    } else if (e instanceof Array) {
                        c = e.length;
                        for (j = 0; j < c; j++) {
                            if (e[j] instanceof Evaluable) {
                                this._evaluators.push(e[j]);
                            }
                        }
                    }
                }
            }
        } },
    clear: { value: function value() {
            this._evaluators = [];
        } },
    eval: { value: function value(_value) {
            this._evaluators.forEach(function (element) {
                if (element instanceof Evaluable) {
                    _value = element.eval(_value);
                }
            });
            return _value;
        } },
    remove: { value: function value(evaluator) {
            if (evaluator instanceof Evaluable) {
                var index = this._evaluators.indexOf(evaluator);
                if (index > -1) {
                    this._evaluators.splice(index, 1);
                    return true;
                }
            }
            return false;
        } }
});
MultiEvaluator.prototype.constructor = MultiEvaluator;
MultiEvaluator.prototype.toString = function () {
    return "[MultiEvaluator]";
};

function PropertyEvaluator(target) {
    Object.defineProperties(this, {
        separator: { value: ".", writable: true },
        target: { value: target, writable: true, configurable: true },
        throwError: { value: false, writable: true },
        undefineable: { value: null, writable: true }
    });
}
PropertyEvaluator.prototype = Object.create(Evaluable.prototype);
PropertyEvaluator.prototype.constructor = PropertyEvaluator;
PropertyEvaluator.prototype.eval = function (o) {
    if (o !== null && (typeof o === "string" || o instanceof String) && this.target !== null) {
        var exp = String(o);
        if (exp.length > 0) {
            var value = this.target;
            var members = exp.split(this.separator);
            var len = members.length;
            for (var i = 0; i < len; i++) {
                if (members[i] in value) {
                    value = value[members[i]];
                } else {
                    if (this.throwError) {
                        throw new EvalError(this + " eval failed with the expression : " + o);
                    }
                    return this.undefineable;
                }
            }
            return value;
        }
    }
    return this.undefineable;
};
PropertyEvaluator.prototype.toString = function () {
    return "[PropertyEvaluator]";
};

function RomanNumber() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    Object.defineProperties(this, {
        _num: { value: 0, writable: true }
    });
    if (typeof value === "string" || value instanceof String) {
        this._num = RomanNumber.parseRomanString(value);
    } else if (typeof value === "number" || value instanceof Number) {
        if (value > RomanNumber.MAX) {
            throw new RangeError("Max value for a RomanNumber is " + RomanNumber.MAX);
        }
        if (value < RomanNumber.MIN) {
            throw new RangeError("Min value for a RomanNumber is " + RomanNumber.MIN);
        }
        this._num = value;
    }
}
Object.defineProperties(RomanNumber, {
    MAX: { value: 3999, enumerable: true },
    MIN: { value: 0, enumerable: true },
    NUMERIC: { value: [1000, 500, 100, 50, 10, 5, 1], enumerable: true },
    ROMAN: { value: ["M", "D", "C", "L", "X", "V", "I"], enumerable: true },
    parse: {
        value: function value(num) {
            var MAX = RomanNumber.MAX;
            var MIN = RomanNumber.MIN;
            var NUMERIC = RomanNumber.NUMERIC;
            var ROMAN = RomanNumber.ROMAN;
            var n = 0;
            var r = "";
            if (typeof num === "number" || num instanceof Number) {
                if (num > RomanNumber.MAX) {
                    throw new RangeError("Max value for a RomanNumber is " + MAX);
                } else if (num < RomanNumber.MIN) {
                    throw new RangeError("Min value for a RomanNumber is " + MIN);
                }
                n = num;
            }
            var i = void 0;
            var rank = void 0;
            var bellow = void 0;
            var roman = void 0;
            var romansub = void 0;
            var size = NUMERIC.length;
            for (i = 0; i < size; i++) {
                if (n === 0) {
                    break;
                }
                rank = NUMERIC[i];
                roman = ROMAN[i];
                if (String(rank).charAt(0) === "5") {
                    bellow = rank - NUMERIC[i + 1];
                    romansub = ROMAN[i + 1];
                } else {
                    bellow = rank - NUMERIC[i + 2];
                    romansub = ROMAN[i + 2];
                }
                if (n >= rank || n >= bellow) {
                    while (n >= rank) {
                        r += roman;
                        n -= rank;
                    }
                }
                if (n > 0 && n >= bellow) {
                    r += romansub + roman;
                    n -= bellow;
                }
            }
            return r;
        }
    },
    parseRomanString: {
        value: function value(roman) {
            var NUMERIC = RomanNumber.NUMERIC;
            var ROMAN = RomanNumber.ROMAN;
            if (roman === null || roman === "") {
                return 0;
            }
            roman = roman.toUpperCase();
            var n = 0;
            var pos = 0;
            var ch = "";
            var next = "";
            var ich = void 0;
            var inext = void 0;
            while (pos >= 0) {
                ch = roman.charAt(pos);
                next = roman.charAt(pos + 1);
                if (ch === "") {
                    break;
                }
                ich = ROMAN.indexOf(ch);
                inext = ROMAN.indexOf(next);
                if (ich < 0) {
                    return 0;
                } else if (ich <= inext || inext === -1) {
                    n += NUMERIC[ich];
                } else {
                    n += NUMERIC[inext] - NUMERIC[ich];
                    pos++;
                }
                pos++;
            }
            return n;
        }
    }
});
RomanNumber.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: RomanNumber },
    parse: { value: function value(_value) {
            return RomanNumber.parse(typeof _value === "number" || _value instanceof Number ? _value : this._num);
        } },
    toString: { value: function value() {
            return this.parse(this._num);
        } },
    valueOf: { value: function value()
        {
            return this._num;
        } }
});

function RomanEvaluator() {}
RomanEvaluator.prototype = Object.create(Evaluable.prototype);
RomanEvaluator.prototype.constructor = RomanEvaluator;
RomanEvaluator.prototype.eval = function (value) {
  if (typeof value === 'string' || value instanceof String) {
    return RomanNumber.parseRomanString(value);
  } else if (typeof value === 'number' || value instanceof Number) {
    return RomanNumber.parse(value);
  } else {
    return null;
  }
};
RomanEvaluator.prototype.toString = function () {
  return "[RomanEvaluator]";
};

/**
 * The {@link system.evaluators} library contains classes to evaluates some objects with a collection of specific strategies.
 * @summary The {@link system.evaluators} library contains classes to evaluates some objects.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.evaluators
 * @memberof system
 */
var evaluators = Object.assign({
  MultiEvaluator: MultiEvaluator,
  PropertyEvaluator: PropertyEvaluator,
  RomanEvaluator: RomanEvaluator
});

function Event(type) {
  var bubbles = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var cancelable = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  Object.defineProperties(this, {
    _bubbles: { writable: true, value: Boolean(bubbles) },
    _cancelable: { writable: true, value: Boolean(cancelable) },
    _currentTarget: { writable: true, value: null },
    _defaultPrevented: { writable: true, value: false },
    _eventPhase: { writable: true, value: 0 },
    _propagationStopped: { writable: true, value: false },
    _immediatePropagationStopped: { writable: true, value: false },
    _target: { writable: true, value: null },
    _type: { writable: true, value: type instanceof String || typeof type === 'string' ? type : null }
  });
  ValueObject.call(this);
}
Event.prototype = Object.create(ValueObject.prototype, {
  constructor: { writable: true, value: Event },
  bubbles: { get: function get() {
      return this._bubbles;
    } },
  cancelable: { get: function get() {
      return this._cancelable;
    } },
  currentTarget: { get: function get() {
      return this._currentTarget;
    } },
  eventPhase: { get: function get() {
      return this._eventPhase;
    } },
  target: { get: function get() {
      return this._target;
    } },
  timestamp: { value: new Date().valueOf() },
  type: { get: function get() {
      return this._type;
    } },
  clone: { writable: true, value: function value() {
      return new Event(this._type, this._bubbles, this._cancelable);
    } },
  isDefaultPrevented: { value: function value() {
      return this._defaultPrevented;
    } },
  isImmediatePropagationStopped: { value: function value() {
      return this._immediatePropagationStopped;
    } },
  isPropagationStopped: { value: function value() {
      return this._propagationStopped;
    } },
  preventDefault: { value: function value() {
      if (this._cancelable) {
        this._defaultPrevented = true;
      }
    } },
  stopImmediatePropagation: { value: function value() {
      this._immediatePropagationStopped = true;
    } },
  stopPropagation: { value: function value() {
      this._propagationStopped = true;
    } },
  toString: { writable: true, value: function value() {
      return this.formatToString(null, "type", "bubbles", "cancelable");
    } },
  withTarget: { value: function value(target) {
      var event = this.target ? this.clone() : this;
      event._target = target;
      return event;
    } },
  withCurrentTarget: { value: function value(currentTarget) {
      this._currentTarget = currentTarget;
      return this;
    } }
});
Object.defineProperties(Event, {
  ACTIVATE: { value: "activate" },
  ADDED: { value: "added" },
  ADDED_TO_STAGE: { value: "addedToStage" },
  CANCEL: { value: "cancel" },
  CHANGE: { value: "change" },
  CLEAR: { value: "clear" },
  CLICK: { value: "click" },
  CLOSE: { value: "close" },
  COMPLETE: { value: "complete" },
  CONNECT: { value: "connect" },
  COPY: { value: "copy" },
  CUT: { value: "cut" },
  DEACTIVATE: { value: "deactivate" },
  FULLSCREEN: { value: "fullScreen" },
  INIT: { value: "init" },
  OPEN: { value: "open" },
  PASTE: { value: "paste" },
  REMOVED: { value: "removed" },
  REMOVED_FROM_STAGE: { value: "removedFromStage" },
  RENDER: { value: "render" },
  RESIZE: { value: "resize" },
  SCROLL: { value: "scroll" },
  SELECT: { value: "select" },
  UNLOAD: { value: "unload" }
});

function EventListener() {}
EventListener.prototype = Object.create(Object.prototype, {
  constructor: { writable: true, value: EventListener },
  handleEvent: { writable: true, value: function value() {} },
  toString: { writable: true, value: function value() {
      return '[' + this.constructor.name + ']';
    } }
});

var EventPhase = Object.defineProperties({}, {
  AT_TARGET: { value: 2, enumerable: true },
  BUBBLING_PHASE: { value: 3, enumerable: true },
  CAPTURING_PHASE: { value: 1, enumerable: true },
  NONE: { value: 0, enumerable: true }
});

function IEventDispatcher() {}
IEventDispatcher.prototype = Object.create(Object.prototype, {
  constructor: { writable: true, value: IEventDispatcher },
  addEventListener: { writable: true, value: function value(type, listener) {
      
    } },
  dispatchEvent: { writable: true, value: function value(event) {} },
  hasEventListener: { writable: true, value: function value(type) {} },
  removeEventListener: { writable: true, value: function value(type, listener) {
      
    } },
  willTrigger: { writable: true, value: function value(type) {} }
});

function EventDispatcher(target) {
    Object.defineProperties(this, {
        target: { writable: true, value: target instanceof IEventDispatcher ? target : null },
        _captureListeners: { value: {} },
        _listeners: { value: {} }
    });
}
EventDispatcher.prototype = Object.create(IEventDispatcher.prototype, {
    constructor: { writable: true, value: EventDispatcher },
    addEventListener: { writable: true, value: function value(type, listener) {
            var useCapture = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
            var priority = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
            if (!(type instanceof String || typeof type === 'string')) {
                throw new TypeError(this + " addEventListener failed, the type argument must be a valid String expression.");
            }
            if (!(listener instanceof Function || listener instanceof EventListener)) {
                throw new TypeError(this + " addEventListener failed, the listener must be a valid Function or EventListener reference.");
            }
            var collection = useCapture ? this._captureListeners : this._listeners;
            var entry = {
                type: type,
                listener: listener,
                useCapture: useCapture,
                priority: priority
            };
            if (!(type in collection)) {
                collection[type] = [entry];
            } else {
                collection[type].push(entry);
            }
            collection[type].sort(this.compare);
        } },
    dispatchEvent: { writable: true, value: function value(event) {
            if (!(event instanceof Event)) {
                throw new TypeError(this + " dispatchEvent failed, the event argument must be a valid Event object.");
            }
            event = event.withTarget(this.target || this);
            var ancestors = this.createAncestorChain();
            event._eventPhase = EventPhase.CAPTURING_PHASE;
            EventDispatcher.internalHandleCapture(event, ancestors);
            if (!event.isPropagationStopped()) {
                event._eventPhase = EventPhase.AT_TARGET;
                event.withCurrentTarget(event._target);
                var listeners = this._listeners[event.type];
                if (this._listeners[event.type]) {
                    EventDispatcher.processListeners(event, listeners);
                }
            }
            if (event.bubbles && !event.isPropagationStopped()) {
                event._eventPhase = EventPhase.BUBBLING_PHASE;
                EventDispatcher.internalHandleBubble(event, ancestors);
            }
            return !event.isDefaultPrevented();
        } },
    hasEventListener: { writable: true, value: function value(type) {
            return Boolean(this._listeners[type] || this._captureListeners[type]);
        } },
    removeEventListener: { writable: true, value: function value(type, listener) {
            var useCapture = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
            if (!(type instanceof String || typeof type === 'string')) {
                throw new TypeError(this + " removeEventListener failed, the type must be a valid String expression.");
            }
            if (!(listener instanceof Function || listener instanceof EventListener)) {
                throw new TypeError(this + " removeEventListener failed, the listener must be a valid Function or EventListener reference.");
            }
            var collection = useCapture ? this._captureListeners : this._listeners;
            var listeners = collection[type];
            if (listeners && listeners.length > 0) {
                var len = listeners.length;
                for (var i = 0; i < len; ++i) {
                    if (listeners[i].listener === listener) {
                        if (len === 1) {
                            delete collection[type];
                        } else {
                            listeners.splice(i, 1);
                        }
                        break;
                    }
                }
            }
        } },
    toString: { writable: true, value: function value() {
            var exp = '[' + this.constructor.name;
            if (this.target) {
                exp += ' target:' + this.target;
            }
            return exp + ']';
        } },
    willTrigger: { writable: true, value: function value(type) {
            var parents = this.createAncestorChain();
            if (parents instanceof Array && parents.length > 0) {
                var parent = void 0;
                var len = parents.length;
                while (--len > -1) {
                    parent = parents[len];
                    if (parent instanceof IEventDispatcher && parent.hasEventListener(type)) {
                        return true;
                    }
                }
            }
            return this.hasEventListener(type);
        } },
    createAncestorChain: { writable: true, value: function value() {
            return null;
        } },
    compare: { value: function value(entry1, entry2) {
            if (entry1.priority > entry2.priority) {
                return -1;
            } else if (entry1.priority < entry2.priority) {
                return 1;
            } else {
                return 0;
            }
        } },
    processCapture: { value: function value(event) {
            event.withCurrentTarget(this.target || this);
            var listeners = this._captureListeners[event.type];
            if (listeners) {
                EventDispatcher.processListeners(event, listeners);
            }
        } },
    processBubble: { value: function value(event) {
            event.withCurrentTarget(this.target || this);
            var listeners = this._listeners[event.type];
            if (listeners) {
                EventDispatcher.processListeners(event, listeners);
            }
        } }
});
Object.defineProperties(EventDispatcher, {
    processListeners: { value: function value(event, listeners) {
            if (listeners instanceof Array && listeners.length > 0) {
                var len = listeners.length;
                var listener = void 0;
                for (var i = 0; i < len; ++i) {
                    listener = listeners[i].listener;
                    var flag = void 0;
                    if (listener instanceof EventListener) {
                        flag = listener.handleEvent(event);
                    } else if (listener instanceof Function) {
                        flag = listener(event);
                    }
                    if (flag === false) {
                        event.stopPropagation();
                        event.preventDefault();
                    }
                    if (event.isImmediatePropagationStopped()) {
                        break;
                    }
                }
            }
        } },
    internalHandleCapture: { value: function value(event, ancestors) {
            if (!(ancestors instanceof Array) || ancestors.length <= 0) {
                return;
            }
            var dispatcher = void 0;
            var len = ancestors.length - 1;
            for (var i = len; i >= 0; i--) {
                dispatcher = ancestors[i];
                dispatcher.processCapture(event);
                if (event.isPropagationStopped()) {
                    break;
                }
            }
        } },
    internalHandleBubble: { value: function value(event, ancestors) {
            if (!ancestors || ancestors.length <= 0) {
                return;
            }
            var dispatcher = void 0;
            var len = ancestors.length;
            for (var i = 0; i < len; i++) {
                dispatcher = ancestors[i];
                dispatcher.processBubble(event);
                if (event.isPropagationStopped()) {
                    break;
                }
            }
        } }
});

/**
 * The {@link system.events} package provides a W3C Event Model implementation.
 * @summary The {@link system.events} package provides an W3C Event Model library.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.events
 * @memberof system
 * @example <caption>Basic usage with a <code>callback</code> function</caption>
 * var click = function( event )
 * {
 *     trace( "click: " + event ) ;
 * };
 *
 * var dispatcher = new EventDispatcher() ;
 *
 * dispatcher.addEventListener( Event.CLICK , click ) ;
 *
 * dispatcher.dispatchEvent( new Event( Event.CLICK ) ) ;
 * @example <caption>Use the W3C DOM {@link system.events.EventListener|EventListener} interface</caption>
 * var Click = function( name )
 * {
 *     this.name = name ;
 * }
 *
 * Click.prototype = Object.create( EventListener.prototype ,
 * {
 *     constructor : { value : Click } ,
 *     handleEvent : { value : function( event )
 *     {
 *         trace( this + ' ' + this.name + ' event:' + event ) ;
 *     }}
 * });
 *
 * var click1 = new Click( '#1') ;
 * var click2 = new Click( '#2') ;
 *
 * var dispatcher = new EventDispatcher() ;
 *
 * dispatcher.addEventListener( Event.CLICK , click1 ) ;
 * dispatcher.addEventListener( Event.CLICK , click2 ) ;
 *
 * dispatcher.dispatchEvent( new Event( Event.CLICK ) ) ;
 *
 * dispatcher.removeEventListener( Event.CLICK , click2 ) ;
 * dispatcher.dispatchEvent( new Event( Event.CLICK ) ) ;
 */
var events = Object.assign({
  Event: Event,
  EventDispatcher: EventDispatcher,
  EventListener: EventListener,
  EventPhase: EventPhase,
  IEventDispatcher: IEventDispatcher
});

function ExpressionFormatter() {
    Object.defineProperties(this, {
        expressions: { value: new ArrayMap() },
        _beginSeparator: { value: '{', writable: true },
        _endSeparator: { value: '}', writable: true },
        _pattern: { value: "{0}((\\w+\)|(\\w+)((.\\w)+|(.\\w+))){1}" },
        _reg: { value: null, writable: true }
    });
    this._reset();
}
Object.defineProperties(ExpressionFormatter, {
    MAX_RECURSION: { value: 200, enumerable: true }
});
ExpressionFormatter.prototype = Object.create(Formattable.prototype, {
    constructor: { value: ExpressionFormatter },
    beginSeparator: {
        get: function get() {
            return this._beginSeparator;
        },
        set: function set(str) {
            this._beginSeparator = str || "{";
            this._reset();
        }
    },
    endSeparator: {
        get: function get() {
            return this._endSeparator;
        },
        set: function set(str) {
            this._endSeparator = str || "}";
            this._reset();
        }
    },
    length: {
        get: function get() {
            return this.expressions.length;
        }
    },
    clear: {
        value: function value() {
            this.expressions.clear();
        }
    },
    format: {
        value: function value(_value) {
            return this._format(String(_value), 0);
        }
    },
    set: {
        value: function value(key, _value2) {
            if (key === '' || !(key instanceof String || typeof key === 'string')) {
                return false;
            }
            if (_value2 === '' || !(_value2 instanceof String || typeof _value2 === 'string')) {
                return false;
            }
            this.expressions.set(key, _value2);
            return true;
        }
    },
    toString: { value: function value() {
            return '[ExpressionFormatter]';
        } },
    _reset: {
        value: function value() {
            this._reg = new RegExp(fastformat(this._pattern, this.beginSeparator, this.endSeparator), "g");
        }
    },
    _format: { value: function value(str) {
            var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
            if (depth >= ExpressionFormatter.MAX_RECURSION) {
                return str;
            }
            var m = str.match(this._reg);
            if (m === null) {
                return str;
            }
            var l = m.length;
            if (l > 0) {
                var exp = void 0;
                var key = void 0;
                for (var i = 0; i < l; i++) {
                    key = m[i].substr(1);
                    key = key.substr(0, key.length - 1);
                    if (this.expressions.has(key)) {
                        exp = this._format(this.expressions.get(key), depth + 1);
                        this.expressions.set(key, exp);
                        str = str.replace(m[i], exp) || exp;
                    }
                }
            }
            return str;
        } }
});

/**
 * The {@link system.formatters} library contains classes to format objects to a specific string expression.
 * @summary The {@link system.formatters} library contains classes to format objects to a specific string expression.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.formatters
 * @memberof system
 */
var formatters = Object.assign({
  ExpressionFormatter: ExpressionFormatter
});

function indexOfAny( source , anyOf , startIndex = 0 , count = -1 )
{
    if( !(source instanceof String || typeof(source) === 'string' ) || source === "" )
    {
        return -1 ;
    }
    if( !(anyOf instanceof Array) )
    {
        return -1 ;
    }
    startIndex = startIndex > 0 ? 0 : startIndex ;
    count      = count < 0 ? -1 : count ;
    let l = source.length;
    let endIndex;
    if( ( count < 0 ) || ( count > l - startIndex ) )
    {
        endIndex = l - 1 ;
    }
    else
    {
        endIndex = startIndex + count - 1;
    }
    for( let i = startIndex ; i <= endIndex ; i++ )
    {
        if( anyOf.indexOf( source[i] ) > - 1 )
        {
            return i ;
        }
    }
    return -1 ;
}

function Receiver() {}
Receiver.prototype = Object.create( Object.prototype ,
{
    constructor : { writable : true , value : Receiver } ,
    receive : { writable : true , value : function() {} } ,
    toString : { writable : true , value : function ()
    {
        return '[' + this.constructor.name + ']' ;
    }}
});

function Signaler() {}
Signaler.prototype = Object.create( Object.prototype,
{
    constructor : { writable : true , value : Signaler } ,
    length : { get : function () { return 0 ; }} ,
    connect : { writable : true , value : function( receiver , priority = 0 , autoDisconnect = false ) {} },
    connected : { writable : true , value : function() {} },
    disconnect: { writable : true , value : function( receiver ) {} },
    emit: { writable : true , value : function() {} },
    hasReceiver : { writable : true , value : function( receiver ) {} }
});

function SignalEntry( receiver , priority = 0 , auto = false )
{
    this.auto = auto ;
    this.receiver = receiver ;
    this.priority = priority ;
}
SignalEntry.prototype = Object.create( Object.prototype ,
{
    constructor : { value: SignalEntry, writable: false } ,
    toString : { value : function () { return '[SignalEntry]' ; }}
});

function Signal()
{
    Object.defineProperties( this ,
    {
        proxy : { value : null, configurable : true , writable : true } ,
        receivers : { writable : true , value : [] }
    }) ;
}
Signal.prototype = Object.create( Signaler.prototype ,
{
    constructor : { value : Signal , writable : true },
    length : { get : function(){ return this.receivers.length ; } },
    connect : { value : function ( receiver , priority = 0 , autoDisconnect = false )
    {
        if ( receiver === null )
        {
            return false ;
        }
        autoDisconnect = autoDisconnect === true ;
        priority = priority > 0 ? (priority - (priority % 1)) : 0 ;
        if ( ( typeof(receiver) === "function" ) || ( receiver instanceof Function ) || ( receiver instanceof Receiver ) || ( "receive" in receiver ) )
        {
            if ( this.hasReceiver( receiver ) )
            {
                return false ;
            }
            this.receivers.push( new SignalEntry( receiver , priority , autoDisconnect ) ) ;
            let i;
            let j;
            let a = this.receivers;
            let swap = function( j , k )
            {
                let temp = a[j];
                a[j]     = a[k] ;
                a[k]     = temp ;
                return true ;
            };
            let swapped = false;
            let l = a.length;
            for( i = 1 ; i < l ; i++ )
            {
                for( j = 0 ; j < ( l - i ) ; j++ )
                {
                    if ( a[j+1].priority > a[j].priority )
                    {
                        swapped = swap(j, j+1) ;
                    }
                }
                if ( !swapped )
                {
                    break;
                }
            }
            return true ;
        }
        return false ;
    }},
    connected : { value : function ()
    {
        return this.receivers.length > 0 ;
    }},
    disconnect : { value : function ( receiver = null )
    {
        if ( receiver === null )
        {
            if ( this.receivers.length > 0 )
            {
                this.receivers = [] ;
                return true ;
            }
            else
            {
                return false ;
            }
        }
        if ( this.receivers.length > 0 )
        {
            let l = this.receivers.length;
            while( --l > -1 )
            {
                if ( this.receivers[l].receiver === receiver )
                {
                    this.receivers.splice( l , 1 ) ;
                    return true ;
                }
            }
        }
        return false ;
    }},
    emit : { value : function( ...values )
    {
        let l = this.receivers.length;
        if ( l === 0 )
        {
            return ;
        }
        let i;
        let r = [];
        let a = this.receivers.slice();
        let e;
        let slot;
        for ( i = 0 ; i < l ; i++ )
        {
            e = a[i] ;
            if ( e.auto )
            {
                r.push( e )  ;
            }
        }
        if ( r.length > 0 )
        {
            l = r.length ;
            while( --l > -1 )
            {
                i = this.receivers.indexOf( r[l] ) ;
                if ( i > -1 )
                {
                    this.receivers.splice( i , 1 ) ;
                }
            }
        }
        l = a.length ;
        for ( i = 0 ; i<l ; i++ )
        {
            slot = a[i].receiver ;
            if( slot instanceof Function || typeof(slot) === "function" )
            {
                slot.apply( this.proxy || this , values ) ;
            }
            else if ( slot instanceof Receiver || ( "receive" in slot && (slot.receive instanceof Function) ) )
            {
                slot.receive.apply( this.proxy || slot , values ) ;
            }
        }
    }},
    hasReceiver : { value : function ( receiver )
    {
        if ( receiver === null )
        {
            return false ;
        }
        if ( this.receivers.length > 0 )
        {
            let l = this.receivers.length;
            while( --l > -1 )
            {
                if ( this.receivers[l].receiver === receiver )
                {
                    return true ;
                }
            }
        }
        return false ;
    }},
    toArray : { value : function()
    {
        let r = [];
        let l = this.receivers.length;
        if ( l > 0 )
        {
            for( let i=0 ; i<l ; i++ )
            {
                r.push( this.receivers[i].receiver ) ;
            }
        }
        return r ;
    }},
    toString : { value : function () { return '[Signal]' ; }}
});

function LoggerLevel(value, name) {
  Enum.call(this, value, name);
}
LoggerLevel.prototype = Object.create(Enum.prototype);
LoggerLevel.prototype.constructor = LoggerLevel;
Object.defineProperties(LoggerLevel, {
  ALL: { value: new LoggerLevel(1, 'ALL'), enumerable: true },
  CRITICAL: { value: new LoggerLevel(16, 'CRITICAL'), enumerable: true },
  DEBUG: { value: new LoggerLevel(2, 'DEBUG'), enumerable: true },
  DEFAULT_LEVEL_STRING: { value: 'UNKNOWN', enumerable: true },
  ERROR: { value: new LoggerLevel(8, 'ERROR'), enumerable: true },
  INFO: { value: new LoggerLevel(4, 'INFO'), enumerable: true },
  NONE: { value: new LoggerLevel(0, 'NONE'), enumerable: true },
  WARNING: { value: new LoggerLevel(6, 'WARNING'), enumerable: true },
  WTF: { value: new LoggerLevel(32, 'WTF'), enumerable: true },
  get: { value: function value(_value) {
      var levels = [LoggerLevel.ALL, LoggerLevel.CRITICAL, LoggerLevel.DEBUG, LoggerLevel.ERROR, LoggerLevel.INFO, LoggerLevel.NONE, LoggerLevel.WARNING, LoggerLevel.WTF];
      var l = levels.length;
      while (--l > -1) {
        if (levels[l]._value === _value) {
          return levels[l];
        }
      }
      return null;
    } },
  getLevelString: { value: function value(_value2) {
      if (LoggerLevel.validate(_value2)) {
        return _value2.toString();
      } else {
        return LoggerLevel.DEFAULT_LEVEL_STRING;
      }
    } },
  validate: { value: function value(level) {
      var levels = [LoggerLevel.ALL, LoggerLevel.CRITICAL, LoggerLevel.DEBUG, LoggerLevel.ERROR, LoggerLevel.INFO, LoggerLevel.NONE, LoggerLevel.WARNING, LoggerLevel.WTF];
      return levels.indexOf(level) > -1;
    } }
});

function LoggerEntry(message, level, channel) {
  this.channel = channel;
  this.level = level instanceof LoggerLevel ? level : LoggerLevel.ALL;
  this.message = message;
}
LoggerEntry.prototype = Object.create(Object.prototype);
LoggerEntry.prototype.constructor = LoggerEntry;

function Logger(channel) {
    Signal.call(this);
    Object.defineProperties(this, {
        _entry: { value: new LoggerEntry(null, null, channel), writable: true }
    });
}
Logger.prototype = Object.create(Signal.prototype, {
    constructor: { writable: true, value: Logger },
    channel: { get: function get() {
            return this._entry.channel;
        } },
    critical: { value: function value(context) {
            for (var _len = arguments.length, options = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                options[_key - 1] = arguments[_key];
            }
            this._log(LoggerLevel.CRITICAL, context, options);
        } },
    debug: { value: function value(context) {
            for (var _len2 = arguments.length, options = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
                options[_key2 - 1] = arguments[_key2];
            }
            this._log(LoggerLevel.DEBUG, context, options);
        } },
    error: { value: function value(context) {
            for (var _len3 = arguments.length, options = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
                options[_key3 - 1] = arguments[_key3];
            }
            this._log(LoggerLevel.ERROR, context, options);
        } },
    info: { value: function value(context) {
            for (var _len4 = arguments.length, options = Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {
                options[_key4 - 1] = arguments[_key4];
            }
            this._log(LoggerLevel.INFO, context, options);
        } },
    log: { value: function value(context) {
            for (var _len5 = arguments.length, options = Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
                options[_key5 - 1] = arguments[_key5];
            }
            this._log(LoggerLevel.ALL, context, options);
        } },
    warning: { value: function value(context) {
            for (var _len6 = arguments.length, options = Array(_len6 > 1 ? _len6 - 1 : 0), _key6 = 1; _key6 < _len6; _key6++) {
                options[_key6 - 1] = arguments[_key6];
            }
            this._log(LoggerLevel.WARNING, context, options);
        } },
    wtf: { value: function value(context) {
            for (var _len7 = arguments.length, options = Array(_len7 > 1 ? _len7 - 1 : 0), _key7 = 1; _key7 < _len7; _key7++) {
                options[_key7 - 1] = arguments[_key7];
            }
            this._log(LoggerLevel.WTF, context, options);
        } },
    toString: { value: function value() {
            return '[Logger]';
        } },
    _log: { value: function value(level, context, options) {
            if (this.connected()) {
                if ((typeof context === "string" || context instanceof String) && options instanceof Array) {
                    var len = options.length;
                    for (var i = 0; i < len; i++) {
                        context = String(context).replace(new RegExp("\\{" + i + "\\}", "g"), options[i]);
                    }
                }
                this._entry.message = context;
                this._entry.level = level;
                this.emit(this._entry);
            }
        } }
});

var strings = Object.defineProperties({}, {
  CHARS_INVALID: { value: "The following characters are not valid\: []~$^&\/(){}<>+\=_-`!@#%?,\:;'\\", enumerable: true },
  CHAR_PLACEMENT: { value: "'*' must be the right most character.", enumerable: true },
  EMPTY_FILTER: { value: "filter must not be null or empty.", enumerable: true },
  ERROR_FILTER: { value: "Error for filter '{0}'.", enumerable: true },
  DEFAULT_CHANNEL: { value: "", enumerable: true },
  ILLEGALCHARACTERS: { value: "[]~$^&/\\(){}<>+=`!#%?,:;'\"@", enumerable: true },
  INVALID_CHARS: { value: "Channels can not contain any of the following characters : []~$^&/\\(){}<>+=`!#%?,:;'\"@", enumerable: true },
  INVALID_LENGTH: { value: "Channels must be at least one character in length.", enumerable: true },
  INVALID_TARGET: { value: "Log, Invalid target specified.", enumerable: true }
});

function LoggerTarget() {
    Object.defineProperties(this, {
        _count: { value: 0, writable: true },
        _factory: { value: null, writable: true },
        _filters: { value: ["*"], writable: true },
        _level: { value: LoggerLevel.ALL, writable: true }
    });
    this.factory = Log;
}
LoggerTarget.prototype = Object.create(Receiver.prototype, {
    constructor: { value: LoggerTarget, writable: true },
    factory: {
        get: function get() {
            return this._factory;
        },
        set: function set(factory) {
            if (this._factory) {
                this._factory.removeTarget(this);
            }
            this._factory = factory instanceof LoggerFactory ? factory : Log;
            this._factory.addTarget(this);
        }
    },
    filters: {
        get: function get() {
            return [].concat(this._filters);
        },
        set: function set(value) {
            var filters = [];
            if (value && value instanceof Array && value.length > 0) {
                var filter = void 0;
                var length = value.length;
                for (var i = 0; i < length; i++) {
                    filter = value[i];
                    if (filters.indexOf(filter) === -1) {
                        this._checkFilter(filter);
                        filters.push(filter);
                    }
                }
            } else {
                filters.push('*');
            }
            if (this._count > 0) {
                this._factory.removeTarget(this);
            }
            this._filters = filters;
            if (this._count > 0) {
                this._factory.addTarget(this);
            }
        }
    },
    level: {
        get: function get() {
            return this._level;
        },
        set: function set(value) {
            this._factory.removeTarget(this);
            this._level = value || LoggerLevel.ALL;
            this._factory.addTarget(this);
        }
    },
    addFilter: { value: function value(channel) {
            this._checkFilter(channel);
            var index = this._filters.indexOf(channel);
            if (index === -1) {
                this._filters.push(channel);
                return true;
            }
            return false;
        } },
    addLogger: { value: function value(logger) {
            if (logger && logger instanceof Logger) {
                this._count++;
                logger.connect(this);
            }
        } },
    logEntry: { value: function value(entry)
        {
        } },
    receive: { value: function value(entry) {
            if (entry instanceof LoggerEntry) {
                if (this._level === LoggerLevel.NONE) {
                    return;
                }
                if (entry.level.valueOf() >= this._level.valueOf()) {
                    this.logEntry(entry);
                }
            }
        } },
    removeFilter: { value: function value(channel) {
            if (channel && (typeof channel === "string" || channel instanceof String) && channel !== "") {
                var index = this._filters.indexOf(channel);
                if (index > -1) {
                    this._filters.splice(index, 1);
                    return true;
                }
            }
            return false;
        } },
    removeLogger: { value: function value(logger) {
            if (logger instanceof Logger) {
                this._count--;
                logger.disconnect(this);
            }
        } },
    _checkFilter: { value: function value(filter) {
            if (filter === null) {
                throw new InvalidFilterError(strings.EMPTY_FILTER);
            }
            if (this._factory.hasIllegalCharacters(filter)) {
                throw new InvalidFilterError(fastformat(strings.ERROR_FILTER, filter) + strings.CHARS_INVALID);
            }
            var index = filter.indexOf("*");
            if (index >= 0 && index !== filter.length - 1) {
                throw new InvalidFilterError(fastformat(strings.ERROR_FILTER, filter) + strings.CHAR_PLACEMENT);
            }
        } },
    toString: { value: function value() {
            return '[LoggerTarget]';
        } }
});

function LoggerFactory() {
    Object.defineProperties(this, {
        _loggers: { value: new ArrayMap(), writable: true },
        _targetLevel: { value: LoggerLevel.NONE, writable: true },
        _targets: { value: [], writable: true }
    });
}
LoggerFactory.prototype = Object.create(Receiver.prototype, {
    constructor: { value: LoggerFactory },
    addTarget: { value: function value(target                 )
        {
            if (target && target instanceof LoggerTarget) {
                var channel = void 0;
                var log = void 0;
                var filters = target.filters;
                var it = this._loggers.iterator();
                while (it.hasNext()) {
                    log = it.next();
                    channel = it.key();
                    if (this._channelMatchInFilterList(channel, filters)) {
                        target.addLogger(log);
                    }
                }
                this._targets.push(target);
                if (this._targetLevel === LoggerLevel.NONE || target.level.valueOf() < this._targetLevel.valueOf()) {
                    this._targetLevel = target.level;
                }
            } else {
                throw new Error(strings.INVALID_TARGET);
            }
        } },
    flush: { value: function value()
        {
            this._loggers.clear();
            this._targets = [];
            this._targetLevel = LoggerLevel.NONE;
        } },
    getLogger: { value: function value(channel) {
            this._checkChannel(channel);
            var logger = this._loggers.get(channel);
            if (!logger) {
                logger = new Logger(channel);
                this._loggers.set(channel, logger);
            }
            var target = void 0;
            var len = this._targets.length;
            for (var i = 0; i < len; i++) {
                target = this._targets[i];
                if (this._channelMatchInFilterList(channel, target.filters)) {
                    target.addLogger(logger);
                }
            }
            return logger;
        } },
    hasIllegalCharacters: { value: function value(_value) {
            return indexOfAny(_value, strings.ILLEGALCHARACTERS.split("")) !== -1;
        } },
    isAll: { value: function value() {
            return this._targetLevel === LoggerLevel.ALL;
        } },
    isCritical: { value: function value() {
            return this._targetLevel === LoggerLevel.CRITICAL;
        } },
    isDebug: { value: function value() {
            return this._targetLevel === LoggerLevel.DEBUG;
        } },
    isError: { value: function value() {
            return this._targetLevel === LoggerLevel.ERROR;
        } },
    isInfo: { value: function value() {
            return this._targetLevel === LoggerLevel.INFO;
        } },
    isWarning: { value: function value() {
            return this._targetLevel === LoggerLevel.WARNING;
        } },
    isWtf: { value: function value() {
            return this._targetLevel === LoggerLevel.WTF;
        } },
    removeTarget: { value: function value(target) {
            if (target && target instanceof LoggerTarget) {
                var log = void 0;
                var filters = target.filters;
                var it = this._loggers.iterator();
                while (it.hasNext()) {
                    log = it.next();
                    var c = it.key();
                    if (this._channelMatchInFilterList(c, filters)) {
                        target.removeLogger(log);
                    }
                }
                var len = this._targets.length;
                for (var i = 0; i < len; i++) {
                    if (target === this._targets[i]) {
                        this._targets.splice(i, 1);
                        i--;
                    }
                }
                this._resetTargetLevel();
            } else {
                throw new Error(strings.INVALID_TARGET);
            }
        } },
    toString: { value: function value() {
            return '[LoggerFactory]';
        } },
    _channelMatchInFilterList: { value: function value(channel, filters) {
            var filter = void 0;
            var index = -1;
            var len = filters.length;
            for (var i = 0; i < len; i++) {
                filter = filters[i];
                index = filter.indexOf("*");
                if (index === 0) {
                    return true;
                }
                index = index < 0 ? index = channel.length : index - 1;
                if (channel.substring(0, index) === filter.substring(0, index)) {
                    return true;
                }
            }
            return false;
        } },
    _checkChannel: { value: function value(channel)
        {
            if (channel === null || channel.length === 0) {
                throw new InvalidChannelError(strings.INVALID_LENGTH);
            }
            if (this.hasIllegalCharacters(channel) || channel.indexOf("*") !== -1) {
                throw new InvalidChannelError(strings.INVALID_CHARS);
            }
        } },
    _resetTargetLevel: { value: function value() {
            var t = void 0;
            var min = LoggerLevel.NONE;
            var len = this._targets.length;
            for (var i = 0; i < len; i++) {
                t = this._targets[i];
                if (min === LoggerLevel.NONE || t.level.valueOf() < min.valueOf()) {
                    min = t.level;
                }
            }
            this._targetLevel = min;
        } }
});

var Log = new LoggerFactory();

var logger = Log.getLogger("system.ioc.logger");

var MagicReference = Object.defineProperties({}, {
  CONFIG: { value: "#config", enumerable: true },
  INIT: { value: "#init", enumerable: true },
  LOCALE: { value: "#locale", enumerable: true },
  PARAMS: { value: "#params", enumerable: true },
  ROOT: { value: "#root", enumerable: true },
  STAGE: { value: "#stage", enumerable: true },
  THIS: { value: "#this", enumerable: true }
});

var ObjectAttribute = Object.defineProperties({}, {
  ARGUMENTS: { value: 'args', enumerable: true },
  CALLBACK: { value: 'callback', enumerable: true },
  CONFIG: { value: 'config', enumerable: true },
  DEPENDS_ON: { value: 'dependsOn', enumerable: true },
  DESTROY_METHOD_NAME: { value: 'destroy', enumerable: true },
  EVALUATORS: { value: 'evaluators', enumerable: true },
  FACTORY: { value: 'factory', enumerable: true },
  GENERATES: { value: 'generates', enumerable: true },
  ID: { value: 'id', enumerable: true },
  IDENTIFY: { value: 'identify', enumerable: true },
  INIT_METHOD_NAME: { value: 'init', enumerable: true },
  LAZY_INIT: { value: 'lazyInit', enumerable: true },
  LAZY_TYPE: { value: 'lazyType', enumerable: true },
  LISTENERS: { value: 'listeners', enumerable: true },
  LOCALE: { value: 'locale', enumerable: true },
  LOCK: { value: 'lock', enumerable: true },
  NAME: { value: 'name', enumerable: true },
  PROPERTIES: { value: 'properties', enumerable: true },
  RECEIVERS: { value: 'receivers', enumerable: true },
  REFERENCE: { value: 'ref', enumerable: true },
  SCOPE: { value: 'scope', enumerable: true },
  SINGLETON: { value: 'singleton', enumerable: true },
  TYPE: { value: 'type', enumerable: true },
  VALUE: { value: 'value', enumerable: true }
});

function ObjectArgument(value) {
    var policy = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "value";
    var evaluators = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    Object.defineProperties(this, {
        args: { value: null, writable: true },
        evaluators: { value: evaluators instanceof Array ? evaluators : null, writable: true },
        scope: { value: null, writable: true },
        value: { value: value, writable: true },
        _policy: { value: null, writable: true }
    });
    this.policy = policy;
}
ObjectArgument.prototype = Object.create(Object.prototype, {
    constructor: { value: ObjectArgument },
    policy: {
        get: function policy() {
            return this._policy;
        },
        set: function set(str) {
            switch (str) {
                case ObjectAttribute.CALLBACK:
                case ObjectAttribute.REFERENCE:
                case ObjectAttribute.CONFIG:
                case ObjectAttribute.LOCALE:
                    {
                        this._policy = str;
                        break;
                    }
                default:
                    {
                        this._policy = ObjectAttribute.VALUE;
                    }
            }
        }
    },
    toString: { value: function value() {
            return '[ObjectArgument]';
        } }
});

function ConfigEvaluator(config) {
    PropertyEvaluator.call(this);
    this.config = config instanceof ObjectConfig ? config : null;
    Object.defineProperties(this, {
        target: { get: function get() {
                return this.config !== null ? this.config.config : null;
            } }
    });
}
ConfigEvaluator.prototype = Object.create(PropertyEvaluator.prototype, {
    constructor: { value: ConfigEvaluator }
});

function LocaleEvaluator(config) {
    PropertyEvaluator.call(this);
    this.config = config instanceof ObjectConfig ? config : null;
    Object.defineProperties(this, {
        target: {
            get: function get() {
                return this.config !== null ? this.config.locale : null;
            }
        }
    });
}
LocaleEvaluator.prototype = Object.create(PropertyEvaluator.prototype, {
    constructor: { value: LocaleEvaluator }
});

function invoke( c , a = null )
{
    if( !(c instanceof Function) )
    {
        return null ;
    }
    if( a === null || !(a instanceof Array) || (a.length === 0)  )
    {
        return new c();
    }
    switch( a.length )
    {
        case 0:
        return new c();
        case 1:
        return new c( a[0] );
        case 2:
        return new c( a[0],a[1] );
        case 3:
        return new c( a[0],a[1],a[2] );
        case 4:
        return new c( a[0],a[1],a[2],a[3] );
        case 5:
        return new c( a[0],a[1],a[2],a[3],a[4] );
        case 6:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5] );
        case 7:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6] );
        case 8:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7] );
        case 9:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8] );
        case 10:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9] );
        case 11:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10] );
        case 12:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11] );
        case 13:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12] );
        case 14:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13] );
        case 15:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14] );
        case 16:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15] );
        case 17:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16] );
        case 18:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17] );
        case 19:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18] );
        case 20:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19] );
        case 21:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20] );
        case 22:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21] );
        case 23:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22] );
        case 24:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23] );
        case 25:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24] );
        case 26:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25] );
        case 27:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26] );
        case 28:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27] );
        case 29:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28] );
        case 30:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28],a[29] );
        case 31:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28],a[29],a[30] );
        case 32:
        return new c( a[0],a[1],a[2],a[3],a[4],a[5],a[6],a[7],a[8],a[9],a[10],a[11],a[12],a[13],a[14],a[15],a[16],
                      a[17],a[18],a[19],a[20],a[21],a[22],a[23],a[24],a[25],a[26],a[27],a[28],a[29],a[30],a[31] );
        default:
        return null;
    }
}

function isLockable(target) {
    if (target) {
        if (target instanceof Lockable) {
            return true;
        } else {
            return Boolean(target['isLocked']) && target.isLocked instanceof Function && Boolean(target['lock']) && target.lock instanceof Function && Boolean(target['unlock']) && target.unlock instanceof Function;
        }
    }
    return false;
}
function Lockable() {
    Object.defineProperties(this, {
        __lock__: { writable: true, value: false }
    });
}
Lockable.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Lockable },
    isLocked: { writable: true, value: function value() {
            return this.__lock__;
        } },
    lock: { writable: true, value: function value() {
            this.__lock__ = true;
        } },
    unlock: { writable: true, value: function value() {
            this.__lock__ = false;
        } },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function createArguments(a) {
    if (!(a instanceof Array) || a.length === 0) {
        return null;
    } else {
        var args = [];
        var l = a.length;
        for (var i = 0; i < l; i++) {
            var o = a[i];
            if (o !== null) {
                var call = ObjectAttribute.CALLBACK in o ? o[ObjectAttribute.CALLBACK] : null;
                var conf = ObjectAttribute.CONFIG in o ? String(o[ObjectAttribute.CONFIG]) : null;
                var i18n = ObjectAttribute.LOCALE in o ? String(o[ObjectAttribute.LOCALE]) : null;
                var ref = ObjectAttribute.REFERENCE in o ? String(o[ObjectAttribute.REFERENCE]) : null;
                var value = ObjectAttribute.VALUE in o ? o[ObjectAttribute.VALUE] : null;
                var evaluators = ObjectAttribute.EVALUATORS in o ? o[ObjectAttribute.EVALUATORS] : null;
                if (ref !== null && ref.length > 0) {
                    args.push(new ObjectArgument(ref, ObjectAttribute.REFERENCE, evaluators));
                } else if (conf !== null && conf.length > 0) {
                    args.push(new ObjectArgument(conf, ObjectAttribute.CONFIG, evaluators));
                } else if (i18n !== null && i18n.length > 0) {
                    args.push(new ObjectArgument(i18n, ObjectAttribute.LOCALE, evaluators));
                } else if (call instanceof Function || (call instanceof String || typeof call === 'string') && call !== '') {
                    var def = new ObjectArgument(call, ObjectAttribute.CALLBACK, evaluators);
                    if (ObjectAttribute.SCOPE in o) {
                        def.scope = o[ObjectAttribute.SCOPE];
                    }
                    if (ObjectAttribute.ARGUMENTS in o && o[ObjectAttribute.ARGUMENTS] instanceof Array) {
                        def.args = createArguments(o[ObjectAttribute.ARGUMENTS]);
                    }
                    args.push(def);
                } else {
                    args.push(new ObjectArgument(value, ObjectAttribute.VALUE, evaluators));
                }
            }
        }
        return args.length > 0 ? args : null;
    }
}

function dumpArray( value , prettyprint = false , indent = 0 , indentor = "    " )
{
    indent = isNaN(indent) ? 0 : indent ;
    prettyprint = Boolean( prettyprint ) ;
    if( !indentor )
    {
        indentor = "    " ;
    }
    let source = [];
    let i;
    let l = value.length;
    for( i = 0 ; i < l ; i++ )
    {
        if( value[i] === undefined )
        {
            source.push( "undefined" );
            continue;
        }
        if( value[i] === null )
        {
            source.push( "null" );
            continue;
        }
        if( prettyprint )
        {
            indent++ ;
        }
        source.push( dump( value[i], prettyprint, indent, indentor ) ) ;
        if( prettyprint )
        {
            indent-- ;
        }
    }
    if( prettyprint )
    {
        let spaces  = [];
        for( i=0 ; i < indent ; i++ )
        {
            spaces.push( indentor );
        }
        let decal  = '\n' + spaces.join( '' );
        return decal + "[" + decal + indentor + source.join( "," + decal + indentor ) + decal + "]" ;
    }
    else
    {
        return "[" + source.join( "," ) + "]" ;
    }
}

function dumpDate( date          , timestamp = false  )
{
    timestamp = Boolean( timestamp ) ;
    if ( timestamp )
    {
        return "new Date(" + String( date.valueOf() ) + ")";
    }
    else
    {
        let y    = date.getFullYear();
        let m    = date.getMonth();
        let d    = date.getDate();
        let h    = date.getHours();
        let mn   = date.getMinutes();
        let s    = date.getSeconds();
        let ms   = date.getMilliseconds();
        let data = [ y, m, d, h, mn, s, ms ];
        data.reverse();
        while( data[0] === 0 )
        {
            data.splice( 0, 1 );
        }
        data.reverse() ;
        return "new Date(" + data.join( "," ) + ")";
    }
}

function dumpObject( value , prettyprint = false , indent = 0 , indentor = "    ")
{
    indent = isNaN(indent) ? 0 : indent ;
    prettyprint = Boolean( prettyprint ) ;
    if( !indentor )
    {
        indentor = "    " ;
    }
    let source  = [];
    for( let member in value )
    {
        if( value.hasOwnProperty( member ) )
        {
            if( value[member] === undefined )
            {
                source.push( member + ":" + "undefined" );
                continue;
            }
            if( value[member] === null )
            {
                source.push( member + ":" + "null" );
                continue;
            }
            if( prettyprint )
            {
                indent++ ;
            }
            source.push( member + ":" + dump( value[ member ], prettyprint, indent, indentor ) );
            if( prettyprint )
            {
                indent-- ;
            }
        }
    }
    source = source.sort();
    if( prettyprint )
    {
        let spaces = [];
        for( let i = 0 ; i < indent ; i++ )
        {
            spaces.push( indentor );
        }
        let decal = '\n' + spaces.join( '' );
        return decal + '{' + decal + indentor + source.join( ',' + decal + indentor ) + decal + '}' ;
    }
    else
    {
        return( '{' + source.join( ',' ) + '}' ) ;
    }
}

function toUnicodeNotation( num )
{
    let hex = num.toString( 16 );
    while( hex.length < 4 )
    {
        hex = "0" + hex ;
    }
    return hex ;
}

function dumpString( value )
{
    let code;
    let quote  = "\"";
    let str    = "";
    let ch     = "";
    let pos    = 0;
    let len    = value.length;
    while( pos < len )
    {
        ch  = value.charAt( pos );
        code = value.charCodeAt( pos );
        if( code > 0xFF )
        {
            str += "\\u" + toUnicodeNotation( code );
            pos++;
            continue;
        }
        switch( ch )
        {
            case "\u0008" :
            {
                str += "\\b" ;
                break;
            }
            case "\u0009" :
            {
                str += "\\t" ;
                break;
            }
            case "\u000A" :
            {
                str += "\\n" ;
                break;
            }
            case "\u000B" :
            {
                str += "\\v" ;
                break;
            }
            case "\u000C" :
            {
                str += "\\f" ;
                break;
            }
            case "\u000D" :
            {
                str += "\\r" ;
                break;
            }
            case "\u0022" :
            {
                str += "\\\"" ;
                break;
            }
            case "\u0027" :
            {
                str += "\\\'";
                break;
            }
            case "\u005c" :
            {
                str += "\\\\";
                break;
            }
            default :
            {
                str += ch;
            }
        }
        pos++;
    }
    return quote + str + quote;
}

function dump( value , prettyprint=false , indent=0  , indentor="    " )
{
    indent = isNaN(indent) ? 0 : indent ;
    prettyprint = Boolean( prettyprint ) ;
    if( !indentor )
    {
        indentor = "    " ;
    }
    if( value === undefined )
    {
        return "undefined";
    }
    else if( value === null )
    {
        return "null";
    }
    else if( typeof(value) === "string" || value instanceof String )
    {
        return dumpString( value );
    }
    else if ( typeof(value) === "boolean" || value instanceof Boolean  )
    {
        return value ? "true" : "false";
    }
    else if( typeof(value) === "number" || value instanceof Number )
    {
        return value.toString() ;
    }
    else if( value instanceof Date )
    {
        return dumpDate( value );
    }
    else if( value instanceof Array )
    {
        return dumpArray( value , prettyprint, indent, indentor );
    }
    else if( value.constructor && value.constructor === Object )
    {
        return dumpObject( value , prettyprint, indent, indentor );
    }
    else if( "toSource" in value )
    {
        return value.toSource( indent );
    }
    else
    {
        return '<unknown>';
    }
}

var ObjectOrder = Object.defineProperties({}, {
  AFTER: { value: "after", enumerable: true },
  BEFORE: { value: "before", enumerable: true },
  NONE: { value: "none", enumerable: true },
  NOW: { value: "now", enumerable: true }
});

function ObjectListener(dispatcher, type) {
  var method = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var useCapture = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var order = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "after";
  var priority = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;
  Object.defineProperties(this, {
    dispatcher: { value: dispatcher, writable: true },
    method: { value: method, writable: true },
    priority: { value: priority, writable: true },
    type: { value: type, writable: true },
    useCapture: { value: useCapture === true, writable: true },
    _order: { value: order === ObjectOrder.BEFORE ? ObjectOrder.BEFORE : ObjectOrder.AFTER, writable: true }
  });
}
ObjectListener.prototype = Object.create(Object.prototype, {
  constructor: { value: ObjectListener },
  order: {
    get: function get() {
      return this._order;
    },
    set: function set(value) {
      this._order = value === ObjectOrder.BEFORE ? ObjectOrder.BEFORE : ObjectOrder.AFTER;
    }
  },
  toString: { value: function value() {
      return '[ObjectListener]';
    } }
});
Object.defineProperties(ObjectListener, {
  DISPATCHER: { value: "dispatcher", enumerable: true },
  METHOD: { value: "method", enumerable: true },
  ORDER: { value: "order", enumerable: true },
  PRIORITY: { value: "priority", enumerable: true },
  USE_CAPTURE: { value: "useCapture", enumerable: true },
  TYPE: { value: "type", enumerable: true }
});

function createListeners(factory) {
    if (!factory) {
        return null;
    }
    var a = null;
    if (factory instanceof Array) {
        a = factory;
    } else if (ObjectAttribute.LISTENERS in factory && factory[ObjectAttribute.LISTENERS] instanceof Array) {
        a = factory[ObjectAttribute.LISTENERS];
    }
    if (a === null || a.length === 0) {
        return null;
    }
    var def = void 0;
    var dispatcher = void 0;
    var type = void 0;
    var listeners = [];
    var id = String(factory[ObjectAttribute.ID]);
    var len = a.length;
    for (var i = 0; i < len; i++) {
        def = a[i];
        if (def !== null && ObjectListener.DISPATCHER in def && ObjectListener.TYPE in def) {
            dispatcher = def[ObjectListener.DISPATCHER];
            if (!(dispatcher instanceof String || typeof dispatcher === 'string') || dispatcher.length === 0) {
                continue;
            }
            type = def[ObjectListener.TYPE];
            if (!(type instanceof String || typeof type === 'string') || type.length === 0) {
                continue;
            }
            listeners.push(new ObjectListener(dispatcher, type, def[ObjectListener.METHOD], def[ObjectListener.USE_CAPTURE] === true, def[ObjectListener.ORDER] === ObjectOrder.BEFORE ? ObjectOrder.BEFORE : ObjectOrder.AFTER, isNaN(def[ObjectListener.PRIORITY]) ? 0 : def[ObjectListener.PRIORITY]));
        } else {
            if (logger) {
                logger.warning("ObjectBuilder.createListeners failed, a listener definition is invalid in the object definition \"{0}\" at \"{1}\" with the value : {2}", id, i, dump(def));
            }
        }
    }
    return listeners.length > 0 ? listeners : null;
}

function ObjectStrategy() {}
ObjectStrategy.prototype = Object.create(Object.prototype, {
  constructor: { writable: true, value: ObjectStrategy },
  toString: { writable: true, value: function value() {
      return '[' + this.constructor.name + ']';
    } }
});

function ObjectProperty(name, value) {
  var policy = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "value";
  var evaluators = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  Object.defineProperties(this, {
    args: { value: null, writable: true },
    evaluators: { value: evaluators instanceof Array ? evaluators : null, writable: true },
    name: { value: name, writable: true },
    scope: { value: null, writable: true },
    value: { value: value, writable: true },
    _policy: { value: null, writable: true }
  });
  this.policy = policy;
}
ObjectProperty.prototype = Object.create(ObjectStrategy.prototype, {
  constructor: { writable: true, value: ObjectProperty },
  policy: {
    get: function get() {
      return this._policy;
    },
    set: function set(str) {
      switch (str) {
        case ObjectAttribute.ARGUMENTS:
        case ObjectAttribute.CALLBACK:
        case ObjectAttribute.CONFIG:
        case ObjectAttribute.LOCALE:
        case ObjectAttribute.REFERENCE:
          {
            this._policy = str;
            break;
          }
        default:
          {
            this._policy = ObjectAttribute.VALUE;
          }
      }
    }
  }
});

function createProperties(factory) {
    if (!factory) {
        return null;
    }
    var a = null;
    if (factory instanceof Array) {
        a = factory;
    } else if (ObjectAttribute.PROPERTIES in factory && factory[ObjectAttribute.PROPERTIES] instanceof Array) {
        a = factory[ObjectAttribute.PROPERTIES];
    }
    if (!(a instanceof Array) || a.length === 0) {
        return null;
    }
    var properties = [];
    var id = String(factory[ObjectAttribute.ID]);
    var len = a.length;
    var prop = null;
    for (var i = 0; i < len; i++) {
        prop = a[i];
        var args = null;
        var call = null;
        var conf = null;
        var i18n = null;
        var name = null;
        var ref = null;
        var value = null;
        var evaluators = null;
        if (prop && ObjectAttribute.NAME in prop) {
            name = prop[ObjectAttribute.NAME];
            if (!(name instanceof String || typeof name === 'string') || name.length === '') {
                continue;
            }
            if (ObjectAttribute.EVALUATORS in prop) {
                evaluators = prop[ObjectAttribute.EVALUATORS] instanceof Array ? prop[ObjectAttribute.EVALUATORS] : null;
            }
            if (ObjectAttribute.ARGUMENTS in prop) {
                args = prop[ObjectAttribute.ARGUMENTS] || null;
            }
            if (ObjectAttribute.CONFIG in prop) {
                conf = prop[ObjectAttribute.CONFIG] || null;
            }
            if (ObjectAttribute.LOCALE in prop) {
                i18n = prop[ObjectAttribute.LOCALE] || null;
            }
            if (ObjectAttribute.CALLBACK in prop) {
                call = prop[ObjectAttribute.CALLBACK];
            }
            if (ObjectAttribute.REFERENCE in prop) {
                ref = prop[ObjectAttribute.REFERENCE] || null;
            }
            if (ObjectAttribute.VALUE in prop) {
                value = prop[ObjectAttribute.VALUE];
            }
            var property = null;
            if ((ref instanceof String || typeof ref === 'string') && ref !== '') {
                property = new ObjectProperty(name, ref, ObjectAttribute.REFERENCE, evaluators);
            } else if ((conf instanceof String || typeof conf === 'string') && conf !== '') {
                property = new ObjectProperty(name, conf, ObjectAttribute.CONFIG, evaluators);
            } else if ((i18n instanceof String || typeof i18n === 'string') && i18n !== '') {
                property = new ObjectProperty(name, i18n, ObjectAttribute.LOCALE, evaluators);
            } else if (call instanceof Function || (call instanceof String || typeof call === 'string') && call !== '') {
                property = new ObjectProperty(name, call, ObjectAttribute.CALLBACK, evaluators);
                if (args && args instanceof Array) {
                    property.args = createArguments(args);
                }
                if (ObjectAttribute.SCOPE in prop) {
                    property.scope = prop[ObjectAttribute.SCOPE] || null;
                }
            } else if (args && args instanceof Array) {
                property = new ObjectProperty(name, createArguments(args), ObjectAttribute.ARGUMENTS);
            } else {
                property = new ObjectProperty(name, value, ObjectAttribute.VALUE, evaluators);
            }
            if (property) {
                properties.push(property);
            }
        } else if (logger) {
            logger.warning("createProperties failed, a property definition is invalid in the object definition \"{0}\" at \"{1}\" with the value : {2}", id, i, dump(prop));
        }
    }
    return properties.length > 0 ? properties : null;
}

function ObjectReceiver(signal) {
  var slot = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var priority = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var autoDisconnect = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var order = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "after";
  Object.defineProperties(this, {
    autoDisconnect: { value: autoDisconnect, writable: true },
    order: {
      get: function get() {
        return this._order;
      },
      set: function set(value) {
        this._order = value === ObjectOrder.BEFORE ? ObjectOrder.BEFORE : ObjectOrder.AFTER;
      }
    },
    priority: { value: priority, writable: true },
    signal: { value: signal, writable: true },
    slot: { value: slot, writable: true },
    _order: { value: order === ObjectOrder.BEFORE ? ObjectOrder.BEFORE : ObjectOrder.AFTER, writable: true }
  });
}
ObjectReceiver.prototype = Object.create(Object.prototype, {
  constructor: { value: ObjectReceiver },
  toString: { value: function value() {
      return '[ObjectReceiver]';
    } }
});
Object.defineProperties(ObjectReceiver, {
  AUTO_DISCONNECT: { value: "autoDisconnect", enumerable: true },
  ORDER: { value: "order", enumerable: true },
  PRIORITY: { value: "priority", enumerable: true },
  SIGNAL: { value: "signal", enumerable: true },
  SLOT: { value: "slot", enumerable: true }
});

function createReceivers(factory) {
    if (!factory) {
        return null;
    }
    var a = null;
    if (factory instanceof Array) {
        a = factory;
    } else if (ObjectAttribute.RECEIVERS in factory && factory[ObjectAttribute.RECEIVERS] instanceof Array) {
        a = factory[ObjectAttribute.RECEIVERS];
    }
    if (a === null || a.length === 0) {
        return null;
    }
    var def = void 0;
    var receivers = [];
    var signal = void 0;
    var id = String(factory[ObjectAttribute.ID]);
    var len = a.length;
    for (var i = 0; i < len; i++) {
        def = a[i];
        if (def !== null && ObjectReceiver.SIGNAL in def) {
            signal = def[ObjectReceiver.SIGNAL];
            if (!(signal instanceof String || typeof signal === 'string') || signal.length === 0) {
                continue;
            }
            receivers.push(new ObjectReceiver(signal, def[ObjectReceiver.SLOT], isNaN(def[ObjectReceiver.PRIORITY]) ? 0 : def[ObjectReceiver.PRIORITY], def[ObjectReceiver.AUTO_DISCONNECT] === true, def[ObjectReceiver.ORDER] === ObjectOrder.BEFORE ? ObjectOrder.BEFORE : ObjectOrder.AFTER));
        } else {
            logger.warning("ObjectBuilder.createReceivers failed, a receiver definition is invalid in the object definition \"{0}\" at \"{1}\" with the value : {2}", id, i, dump(def));
        }
    }
    return receivers.length > 0 ? receivers : null;
}

var ObjectStrategies = Object.defineProperties({}, {
  FACTORY_METHOD: { value: 'factoryMethod', enumerable: true },
  FACTORY_PROPERTY: { value: 'factoryProperty', enumerable: true },
  FACTORY_REFERENCE: { value: 'factoryReference', enumerable: true },
  FACTORY_VALUE: { value: 'factoryValue', enumerable: true },
  STATIC_FACTORY_METHOD: { value: 'staticFactoryMethod', enumerable: true },
  STATIC_FACTORY_PROPERTY: { value: 'staticFactoryProperty', enumerable: true }
});

function ObjectMethod(name, args) {
  Object.defineProperties(this, {
    args: { value: args, writable: true },
    name: { value: name, writable: true }
  });
}
ObjectMethod.prototype = Object.create(ObjectStrategy.prototype, {
  constructor: { writable: true, value: ObjectMethod }
});

function ObjectFactoryMethod(factory, name, args) {
    ObjectMethod.call(this, name, args);
    Object.defineProperties(this, {
        factory: { value: factory, writable: true }
    });
}
ObjectFactoryMethod.prototype = Object.create(ObjectMethod.prototype, {
    constructor: { writable: true, value: ObjectFactoryMethod }
});
Object.defineProperties(ObjectFactoryMethod, {
    build: { value: function value(o) {
            if (o === null) {
                return null;
            }
            if (ObjectAttribute.FACTORY in o && ObjectAttribute.NAME in o) {
                return new ObjectFactoryMethod(o[ObjectAttribute.FACTORY] || null, o[ObjectAttribute.NAME] || null, createArguments(o[ObjectAttribute.ARGUMENTS] || null));
            } else {
                return null;
            }
        } }
});

function ObjectFactoryProperty(factory, name) {
    var evaluators = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    ObjectProperty.call(this, name, null, null, evaluators);
    Object.defineProperties(this, {
        factory: { value: factory, writable: true }
    });
}
ObjectFactoryProperty.prototype = Object.create(ObjectProperty.prototype, {
    constructor: { writable: true, value: ObjectFactoryProperty }
});
Object.defineProperties(ObjectFactoryProperty, {
    build: {
        value: function value(o) {
            if (o === null) {
                return null;
            }
            if (ObjectAttribute.FACTORY in o && ObjectAttribute.NAME in o) {
                return new ObjectFactoryProperty(o[ObjectAttribute.FACTORY] || null, o[ObjectAttribute.NAME] || null, o[ObjectAttribute.EVALUATORS] || null);
            } else {
                return null;
            }
        }
    }
});

function ObjectReference(ref) {
    Object.defineProperties(this, {
        ref: { value: ref instanceof String || typeof ref === 'string' ? ref : null, writable: true }
    });
}
ObjectReference.prototype = Object.create(ObjectStrategy.prototype, {
    constructor: { value: ObjectReference }
});

function ObjectStaticFactoryMethod(type, name, args) {
    ObjectMethod.call(this, name, args);
    Object.defineProperties(this, {
        type: { value: type, writable: true }
    });
}
ObjectStaticFactoryMethod.prototype = Object.create(ObjectMethod.prototype, {
    constructor: { value: ObjectStaticFactoryMethod, writable: true }
});
Object.defineProperties(ObjectStaticFactoryMethod, {
    build: { value: function value(o) {
            if (o === null) {
                return null;
            }
            if (ObjectAttribute.TYPE in o && ObjectAttribute.NAME in o) {
                var strategy = new ObjectStaticFactoryMethod(o[ObjectAttribute.TYPE] || null, o[ObjectAttribute.NAME] || null, createArguments(o[ObjectAttribute.ARGUMENTS] || null));
                return strategy;
            } else {
                return null;
            }
        } }
});

function ObjectStaticFactoryProperty(name, type) {
    var evaluators = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    ObjectProperty.call(this, name, null, null, evaluators);
    Object.defineProperties(this, {
        type: { value: type, writable: true }
    });
}
ObjectStaticFactoryProperty.prototype = Object.create(ObjectProperty.prototype, {
    constructor: { value: ObjectStaticFactoryProperty, writable: true }
});
Object.defineProperties(ObjectStaticFactoryProperty, {
    build: { value: function value(o) {
            if (o === null) {
                return null;
            }
            if (ObjectAttribute.TYPE in o && ObjectAttribute.NAME in o) {
                return new ObjectStaticFactoryProperty(o[ObjectAttribute.NAME] || null, o[ObjectAttribute.TYPE] || null, o[ObjectAttribute.EVALUATORS] || null);
            } else {
                return null;
            }
        } }
});

function ObjectValue(value) {
    Object.defineProperties(this, {
        value: { writable: true, value: value }
    });
}
ObjectValue.prototype = Object.create(ObjectStrategy.prototype, {
    constructor: { writable: true, value: ObjectValue }
});

function createStrategy(o) {
    if (ObjectStrategies.FACTORY_METHOD in o) {
        return ObjectFactoryMethod.build(o[ObjectStrategies.FACTORY_METHOD]);
    } else if (ObjectStrategies.FACTORY_PROPERTY in o) {
        return ObjectFactoryProperty.build(o[ObjectStrategies.FACTORY_PROPERTY]);
    } else if (ObjectStrategies.FACTORY_REFERENCE in o) {
        return new ObjectReference(o[ObjectStrategies.FACTORY_REFERENCE]);
    } else if (ObjectStrategies.FACTORY_VALUE in o) {
        return new ObjectValue(o[ObjectStrategies.FACTORY_VALUE]);
    } else if (ObjectStrategies.STATIC_FACTORY_METHOD in o) {
        return ObjectStaticFactoryMethod.build(o[ObjectStrategies.STATIC_FACTORY_METHOD]);
    } else if (ObjectStrategies.STATIC_FACTORY_PROPERTY in o) {
        return ObjectStaticFactoryProperty.build(o[ObjectStrategies.STATIC_FACTORY_PROPERTY]);
    } else {
        return null;
    }
}

var ObjectScope = Object.defineProperties({}, {
  PROTOTYPE: { value: "prototype", enumerable: true },
  SINGLETON: { value: "singleton", enumerable: true },
  SCOPES: { value: ["prototype", "singleton"] },
  validate: { value: function value(scope) {
      return ObjectScope.SCOPES.indexOf(scope) > -1;
    } }
});

function ObjectDefinition(id, type) {
    var singleton = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var lazyInit = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    var lazyType = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
    if (id === null || id === undefined) {
        throw new ReferenceError(this + " constructor failed, the 'id' value passed in argument not must be empty or 'null' or 'undefined'.");
    }
    if (type === null || type === undefined) {
        throw new ReferenceError(this + " constructor failed, the 'type' passed in argument not must be empty or 'null' or 'undefined'.");
    }
    Object.defineProperties(this, {
        constructorArguments: { value: null, enumerable: true, writable: true },
        destroyMethodName: { value: null, enumerable: true, writable: true },
        id: { value: id, enumerable: true, writable: true },
        identify: { value: false, enumerable: true, writable: true },
        initMethodName: { value: null, enumerable: true, writable: true },
        lock: { value: false, enumerable: true, writable: true },
        properties: { value: null, enumerable: true, writable: true },
        type: { value: type, enumerable: true, writable: true },
        _afterListeners: { value: null, writable: true },
        _beforeListeners: { value: null, writable: true },
        _dependsOn: { value: null, writable: true },
        _generates: { value: null, writable: true },
        _lazyInit: { value: lazyInit && singleton, writable: true },
        _lazyType: { value: lazyType === true, writable: true },
        _singleton: { value: singleton === true, writable: true },
        _scope: { value: singleton === true ? ObjectScope.SINGLETON : ObjectScope.PROTOTYPE, writable: true },
        _strategy: { value: null, writable: true }
    });
}
ObjectDefinition.prototype = Object.create(Identifiable.prototype, {
    constructor: { writable: true, value: Identifiable },
    afterListeners: { get: function get() {
            return this._afterListeners;
        } },
    afterReceivers: { get: function get() {
            return this._afterReceivers;
        } },
    beforeListeners: { get: function get() {
            return this._beforeListeners;
        } },
    beforeReceivers: { get: function get() {
            return this._beforeReceivers;
        } },
    dependsOn: {
        get: function get() {
            return this._dependsOn;
        },
        set: function set(ar) {
            this._dependsOn = ar instanceof Array && ar.length > 0 ? ar.filter(this._filterStrings) : null;
        }
    },
    generates: {
        get: function get() {
            return this._generates;
        },
        set: function set(ar) {
            this._generates = ar instanceof Array && ar.length > 0 ? ar.filter(this._filterStrings) : null;
        }
    },
    lazyInit: {
        get: function get() {
            return this._lazyInit;
        },
        set: function set(flag) {
            this._lazyInit = flag instanceof Boolean || typeof flag === 'boolean' ? flag : false;
        }
    },
    lazyType: {
        get: function get() {
            return this._lazyType;
        },
        set: function set(value) {
            this._lazyType = value === true;
        }
    },
    listeners: { set: function set(ar) {
            this._afterListeners = [];
            this._beforeListeners = [];
            if (ar === null || !(ar instanceof Array)) {
                return;
            }
            var l = ar.length;
            if (l > 0) {
                for (var i = 0; i < l; i++) {
                    var r = ar[i];
                    if (r instanceof ObjectListener) {
                        if (r.order === ObjectOrder.AFTER) {
                            this._afterListeners.push(r);
                        } else {
                            this._beforeListeners.push(r);
                        }
                    }
                }
            }
        } },
    receivers: { set: function set(ar) {
            this._afterReceivers = [];
            this._beforeReceivers = [];
            if (ar === null || !(ar instanceof Array)) {
                return;
            }
            var l = ar.length;
            if (l > 0) {
                for (var i = 0; i < l; i++) {
                    var r = ar[i];
                    if (r instanceof ObjectReceiver) {
                        if (r.order === ObjectOrder.AFTER) {
                            this._afterReceivers.push(r);
                        } else {
                            this._beforeReceivers.push(r);
                        }
                    }
                }
            }
        } },
    singleton: { get: function get() {
            return this._singleton;
        } },
    scope: {
        get: function get() {
            return this._scope;
        },
        set: function set(scope) {
            this._scope = ObjectScope.validate(scope) ? scope : ObjectScope.PROTOTYPE;
            this._singleton = Boolean(this._scope === ObjectScope.SINGLETON);
        }
    },
    strategy: {
        get: function get() {
            return this._strategy;
        },
        set: function set(strategy) {
            this._strategy = strategy instanceof ObjectStrategy ? strategy : null;
        }
    },
    toString: { value: function value() {
            return "[ObjectDefinition]";
        } },
    _filterStrings: { value: function value(item) {
            return (typeof item === 'string' || item instanceof String) && item.length > 0;
        } }
});

function createObjectDefinition(o) {
    var definition = new ObjectDefinition(o[ObjectAttribute.ID] || null, o[ObjectAttribute.TYPE] || null, o[ObjectAttribute.SINGLETON] || false, o[ObjectAttribute.LAZY_INIT] || false, o[ObjectAttribute.LAZY_TYPE] || false);
    if (ObjectAttribute.IDENTIFY in o && (o[ObjectAttribute.IDENTIFY] instanceof Boolean || typeof o[ObjectAttribute.IDENTIFY] === 'boolean')) {
        definition.identify = o[ObjectAttribute.IDENTIFY];
    }
    if (ObjectAttribute.LOCK in o && (o[ObjectAttribute.LOCK] instanceof Boolean || typeof o[ObjectAttribute.LOCK] === 'boolean')) {
        definition.lock = o[ObjectAttribute.LOCK];
    }
    if (ObjectAttribute.ARGUMENTS in o && o[ObjectAttribute.ARGUMENTS] instanceof Array) {
        definition.constructorArguments = createArguments(o[ObjectAttribute.ARGUMENTS]);
    }
    if (ObjectAttribute.DESTROY_METHOD_NAME in o) {
        definition.destroyMethodName = o[ObjectAttribute.DESTROY_METHOD_NAME];
    }
    if (ObjectAttribute.INIT_METHOD_NAME in o) {
        definition.initMethodName = o[ObjectAttribute.INIT_METHOD_NAME];
    }
    if (ObjectAttribute.SCOPE in o) {
        definition.scope = o[ObjectAttribute.SCOPE];
    }
    if (ObjectAttribute.DEPENDS_ON in o && o[ObjectAttribute.DEPENDS_ON] instanceof Array) {
        definition.dependsOn = o[ObjectAttribute.DEPENDS_ON];
    }
    if (ObjectAttribute.GENERATES in o && o[ObjectAttribute.GENERATES] instanceof Array) {
        definition.generates = o[ObjectAttribute.GENERATES];
    }
    var listeners = createListeners(o);
    if (listeners) {
        definition.listeners = listeners;
    }
    var properties = createProperties(o);
    if (properties) {
        definition.properties = properties;
    }
    var receivers = createReceivers(o);
    if (receivers) {
        definition.receivers = receivers;
    }
    var strategy = createStrategy(o);
    if (strategy) {
        definition.strategy = strategy;
    }
    return definition;
}

function isRunnable(target) {
    if (target) {
        if (target instanceof Runnable) {
            return true;
        }
        return Boolean(target['run']) && target.run instanceof Function;
    }
    return false;
}
function Runnable() {}
Runnable.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Runnable },
    run: { writable: true, value: function value() {
        } },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

var TaskPhase = Object.defineProperties({}, {
  ERROR: { value: 'error', enumerable: true },
  DELAYED: { value: 'delayed', enumerable: true },
  FINISHED: { value: 'finished', enumerable: true },
  INACTIVE: { value: 'inactive', enumerable: true },
  RUNNING: { value: 'running', enumerable: true },
  STOPPED: { value: 'stopped', enumerable: true },
  TIMEOUT: { value: 'timeout', enumerable: true }
});

function Action() {
  Object.defineProperties(this, {
    finishIt: { value: new Signal() },
    startIt: { value: new Signal() },
    __lock__: { writable: true, value: false },
    _phase: { writable: true, value: TaskPhase.INACTIVE },
    _running: { writable: true, value: false }
  });
}
Action.prototype = Object.create(Runnable.prototype, {
  constructor: { writable: true, value: Action },
  phase: { get: function get() {
      return this._phase;
    } },
  running: { get: function get() {
      return this._running;
    } },
  clone: { writable: true, value: function value() {
      return new Action();
    } },
  isLocked: { writable: true, value: function value() {
      return this.__lock__;
    } },
  lock: { writable: true, value: function value() {
      this.__lock__ = true;
    } },
  notifyFinished: { writable: true, value: function value() {
      this._running = false;
      this._phase = TaskPhase.FINISHED;
      this.finishIt.emit(this);
      this._phase = TaskPhase.INACTIVE;
    } },
  notifyStarted: { writable: true, value: function value() {
      this._running = true;
      this._phase = TaskPhase.RUNNING;
      this.startIt.emit(this);
    } },
  unlock: { writable: true, value: function value() {
      this.__lock__ = false;
    } }
});

function Task() {
  Action.call(this);
  Object.defineProperties(this, {
    changeIt: { value: new Signal() },
    clearIt: { value: new Signal() },
    errorIt: { value: new Signal() },
    infoIt: { value: new Signal() },
    looping: { value: false, writable: true },
    loopIt: { value: new Signal() },
    pauseIt: { value: new Signal() },
    progressIt: { value: new Signal() },
    resumeIt: { value: new Signal() },
    stopIt: { value: new Signal() },
    throwError: { value: false, writable: true },
    timeoutIt: { value: new Signal() }
  });
}
Task.prototype = Object.create(Action.prototype, {
  constructor: { writable: true, value: Task },
  clone: { writable: true, value: function value() {
      return new Task();
    } },
  notifyChanged: { writable: true, value: function value() {
      if (!this.__lock__) {
        this.changeIt.emit(this);
      }
    } },
  notifyCleared: { writable: true, value: function value() {
      if (!this.__lock__) {
        this.clearIt.emit(this);
      }
    } },
  notifyError: { writable: true, value: function value() {
      var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this._running = false;
      this._phase = TaskPhase.ERROR;
      if (!this.__lock__) {
        this.errorIt.emit(this, message);
      }
      if (this.throwError) {
        throw new Error(message);
      }
    } },
  notifyInfo: { writable: true, value: function value(info) {
      if (!this.__lock__) {
        this.infoIt.emit(this, info);
      }
    } },
  notifyLooped: { writable: true, value: function value() {
      this._running = true;
      this._phase = TaskPhase.RUNNING;
      if (!this.__lock__) {
        this.loopIt.emit(this);
      }
    } },
  notifyPaused: { writable: true, value: function value() {
      this._running = false;
      this._phase = TaskPhase.STOPPED;
      if (!this.__lock__) {
        this.pauseIt.emit(this);
      }
    } },
  notifyProgress: { writable: true, value: function value() {
      this._running = true;
      this._phase = TaskPhase.RUNNING;
      if (!this.__lock__) {
        this.progressIt.emit(this);
      }
    } },
  notifyResumed: { writable: true, value: function value() {
      this._running = true;
      this._phase = TaskPhase.RUNNING;
      if (!this.__lock__) {
        this.resumeIt.emit(this);
      }
    } },
  notifyStopped: { writable: true, value: function value() {
      this._running = false;
      this._phase = TaskPhase.STOPPED;
      if (!this.__lock__) {
        this.stopIt.emit(this);
      }
    } },
  notifyTimeout: { writable: true, value: function value() {
      this._running = false;
      this._phase = TaskPhase.TIMEOUT;
      if (!this.__lock__) {
        this.timeoutIt.emit(this);
      }
    } },
  resume: { writable: true, value: function value() {} },
  reset: { writable: true, value: function value() {} },
  start: { writable: true, value: function value() {
      this.run();
    } },
  stop: { writable: true, value: function value() {} }
});

function ObjectDefinitionContainer() {
    Task.call(this);
    Object.defineProperties(this, {
        _map: { writable: true, value: new ArrayMap() }
    });
}
ObjectDefinitionContainer.prototype = Object.create(Task.prototype, {
    constructor: { configurable: true, writable: true, value: ObjectDefinitionContainer },
    numObjectDefinition: { get: function get() {
            return this._map.length;
        } },
    addObjectDefinition: { value: function value(definition) {
            if (definition instanceof ObjectDefinition) {
                this._map.set(definition.id, definition);
            } else {
                throw new ReferenceError(this + " addObjectDefinition failed, the specified object definition must be an ObjectDefinition object.");
            }
        } },
    clearObjectDefinition: { value: function value() {
            this._map.clear();
        } },
    clone: { value: function value() {
            return new ObjectDefinitionContainer();
        } },
    getObjectDefinition: { value: function value(id) {
            if (this._map.has(id)) {
                return this._map.get(id);
            } else {
                throw new ReferenceError(this + " getObjectDefinition failed, the specified object definition don't exist : " + id);
            }
        } },
    hasObjectDefinition: { value: function value(id) {
            return this._map.has(id);
        } },
    removeObjectDefinition: { value: function value(id) {
            if (this._map.has(id)) {
                this._map.delete(id);
            } else {
                throw new ReferenceError(this + " removeObjectDefinition failed, the specified object definition don't exist : " + id);
            }
        } }
});

function ObjectFactory() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var objects = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    ObjectDefinitionContainer.call(this);
    Object.defineProperties(this, {
        objects: { value: objects instanceof Array ? objects : null, writable: true },
        bufferSingletons: { value: [], writable: true },
        _config: { value: null, writable: true },
        _evaluator: { value: new MultiEvaluator() },
        _singletons: { value: new ArrayMap() }
    });
    this.config = config;
}
ObjectFactory.prototype = Object.create(ObjectDefinitionContainer.prototype, {
    constructor: { value: ObjectFactory },
    config: {
        get: function get() {
            return this._config;
        },
        set: function set(config) {
            if (this._config) {
                this._config.referenceEvaluator.factory = null;
            }
            this._config = config instanceof ObjectConfig ? config : new ObjectConfig();
            this._config.referenceEvaluator.factory = this;
        }
    },
    singletons: { get: function get() {
            return this._singletons;
        } },
    clone: { value: function value() {
            return new ObjectFactory(this.config, [].concat(this.objects));
        } },
    hasSingleton: { value: function value(id) {
            return this._singletons.has(id);
        } },
    getObject: { value: function value(id) {
            if (!(id instanceof String || typeof id === 'string')) {
                return null;
            }
            var instance = null;
            try {
                var definition = void 0;
                try {
                    definition = this.getObjectDefinition(id);
                } catch (e) {
                }
                if (!(definition instanceof ObjectDefinition)) {
                    throw new Error("the definition is not register in the factory");
                }
                if (definition.singleton) {
                    instance = this._singletons.get(id) || null;
                }
                if (!instance) {
                    if (!(definition.type instanceof Function)) {
                        if (definition.type instanceof String || typeof definition.type === 'string') {
                            definition.type = this.config.typeEvaluator.eval(definition.type);
                        }
                    }
                    if (definition.type instanceof Function) {
                        if (definition.strategy) {
                            instance = this.createObjectWithStrategy(definition.strategy);
                        } else {
                            try {
                                instance = invoke(definition.type, this.createArguments(definition.constructorArguments, definition.id));
                            } catch (e) {
                                throw new Error("can't create the instance with the specified definition type " + definition.type + ". The arguments limit exceeded, you can pass a maximum of 32 arguments");
                            }
                        }
                        if (instance) {
                            if (!definition.lazyType) {
                                var check = false;
                                if (instance instanceof definition.type) {
                                    check = true;
                                } else if (definition.type === String) {
                                    check = instance instanceof String || typeof instance === 'string';
                                } else if (definition.type === Number) {
                                    check = instance instanceof Number || typeof instance === 'number';
                                } else if (definition.type === Boolean) {
                                    check = instance instanceof Boolean || typeof instance === 'boolean';
                                }
                                if (!check) {
                                    instance = null;
                                    throw new Error("the new object is not an instance of the [" + definition.type.name + "] constructor");
                                }
                            }
                            if (definition.singleton) {
                                this._singletons.set(id, instance);
                            }
                            this.dependsOn(definition);
                            this.populateIdentifiable(instance, definition);
                            var flag = isLockable(instance) && (definition.lock === true || this.config.lock === true && definition.lock !== false);
                            if (flag) {
                                instance.lock();
                            }
                            if (definition.beforeListeners instanceof Array && definition.beforeListeners.length > 0) {
                                this.registerListeners(instance, definition.beforeListeners);
                            }
                            if (definition.beforeReceivers instanceof Array && definition.beforeReceivers.length > 0) {
                                this.registerReceivers(instance, definition.beforeReceivers);
                            }
                            this.populateProperties(instance, definition);
                            if (definition.afterListeners instanceof Array && definition.afterListeners.length > 0) {
                                this.registerListeners(instance, definition.afterListeners);
                            }
                            if (definition.afterReceivers instanceof Array && definition.afterReceivers.length > 0) {
                                this.registerReceivers(instance, definition.afterReceivers);
                            }
                            if (flag) {
                                instance.unlock();
                            }
                            this.invokeInitMethod(instance, definition);
                            this.generates(definition);
                        }
                    } else {
                        throw new Error("the definition.type property is not a valid constructor");
                    }
                }
            } catch (er) {
                this.warn(this + " getObject('" + id + "') failed, " + er.message + ".");
            }
            return instance || null;
        } },
    isDirty: { value: function value() {
            return this.bufferSingletons && this.bufferSingletons instanceof Array && this.bufferSingletons.length > 0;
        } },
    isLazyInit: { value: function value(id) {
            if (this.hasObjectDefinition(id)) {
                return this.getObjectDefinition(id).lazyInit;
            } else {
                return false;
            }
        } },
    isSingleton: { value: function value(id) {
            if (this.hasObjectDefinition(id)) {
                return this.getObjectDefinition(id).singleton;
            } else {
                return false;
            }
        } },
    removeSingleton: { value: function value(id) {
            if (this.isSingleton(id) && this._singletons.has(id)) {
                this.invokeDestroyMethod(id);
                this._singletons.delete(id);
            }
        } },
    run: { value: function value() {
            if (this.running) {
                return;
            }
            this.notifyStarted();
            if (arguments.length > 0 && (arguments.length <= 0 ? undefined : arguments[0]) instanceof Array) {
                this.objects = arguments.length <= 0 ? undefined : arguments[0];
            }
            if (this.bufferSingletons === null) {
                this.bufferSingletons = [];
            }
            if (this.objects instanceof Array && this.objects.length > 0) {
                while (this.objects.length > 0) {
                    var init = this.objects.shift();
                    if (init !== null) {
                        var definition = createObjectDefinition(init);
                        this.addObjectDefinition(definition);
                        if (definition.singleton && !definition.lazyInit) {
                            if (this.hasObjectDefinition(definition.id)) {
                                this.bufferSingletons.push(String(definition.id));
                            }
                        }
                    } else {
                        this.warn(this + " create new object definition failed with a 'null' or 'undefined' object.");
                    }
                }
            }
            if (this.bufferSingletons instanceof Array && this.bufferSingletons.length > 0 && !this._config.lazyInit && !this.isLocked()) {
                var len = this.bufferSingletons.length;
                for (var i = 0; i < len; i++) {
                    this.getObject(this.bufferSingletons[i]);
                }
                this.bufferSingletons = null;
            }
            this.notifyFinished();
        } },
    warn: { value: function value() {
            if (this.config.useLogger) {
                for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                    args[_key] = arguments[_key];
                }
                logger.warning.apply(logger, args);
            }
        } },
    createArguments: { value: function value() {
            var args = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (args === null || !(args instanceof Array) || args.length === 0) {
                return null;
            }
            var stack = [];
            var len = args.length;
            for (var i = 0; i < len; i++) {
                var item = args[i];
                if (item instanceof ObjectArgument) {
                    var value = item.value;
                    try {
                        var alert = null;
                        if (item.policy === ObjectAttribute.CALLBACK) {
                            var callback = value;
                            if (value instanceof String || typeof value === 'string') {
                                callback = this._config.referenceEvaluator.eval(value);
                            }
                            if (callback instanceof Function) {
                                if (item.scope) {
                                    if (item.args instanceof Array) {
                                        callback = value.bind.apply(item.scope, [item.scope].concat(this.createArguments(item.args, id)));
                                    } else {
                                        callback = value.bind(item.scope);
                                    }
                                }
                                value = callback;
                            } else {
                                alert = ObjectAttribute.CALLBACK;
                                value = null;
                            }
                        }
                        if (item.policy === ObjectAttribute.REFERENCE) {
                            value = this._config.referenceEvaluator.eval(value);
                            if (value === null) {
                                alert = ObjectAttribute.FACTORY;
                            }
                        } else if (item.policy === ObjectAttribute.CONFIG) {
                            value = this._config.configEvaluator.eval(value);
                            if (value === null) {
                                alert = ObjectAttribute.LOCALE;
                            }
                        } else if (item.policy === ObjectAttribute.LOCALE) {
                            value = this._config.localeEvaluator.eval(value);
                            if (value === null) {
                                alert = ObjectAttribute.LOCALE;
                            }
                        }
                        if (alert !== null) {
                            this.warn(this + " createArguments failed at the index '" + i + "' and return a 'null' " + alert + " reference, see the object definition with the id : " + id);
                        }
                        if (item.evaluators !== null && item.evaluators.length > 0) {
                            value = this.eval(value, item.evaluators);
                        }
                        stack.push(value);
                    } catch (er) {
                        this.warn(this + " createArguments failed in the object definition with the id : " + id + ", " + er.toString());
                    }
                }
            }
            return stack;
        } },
    createObjectWithStrategy: { value: function value(strategy) {
            var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (!(strategy instanceof ObjectStrategy)) {
                return null;
            }
            var name = strategy.name;
            var instance = null;
            var object = void 0;
            var ref = void 0;
            if (strategy instanceof ObjectMethod) {
                if (strategy instanceof ObjectStaticFactoryMethod) {
                    object = strategy.type;
                    if (object instanceof String || typeof object === 'string') {
                        object = this.config.typeEvaluator.eval(object);
                    }
                    if (object && name && name in object && object[name] instanceof Function) {
                        instance = object[name].apply(object, this.createArguments(strategy.args, id));
                    }
                } else if (strategy instanceof ObjectFactoryMethod) {
                    ref = this.getObject(strategy.factory);
                    if (ref && name && name in ref && ref[name] instanceof Function) {
                        instance = ref[name].apply(ref, this.createArguments(strategy.args, id));
                    }
                }
            } else if (strategy instanceof ObjectProperty) {
                if (strategy instanceof ObjectStaticFactoryProperty) {
                    object = strategy.type;
                    if (object instanceof String || typeof object === 'string') {
                        object = this.config.typeEvaluator.eval(object);
                    }
                    if (object && name && name in object) {
                        instance = object[name];
                    }
                } else if (strategy instanceof ObjectFactoryProperty) {
                    ref = this.getObject(strategy.factory);
                    if (ref && name && name in ref) {
                        instance = ref[name];
                    }
                }
            } else if (strategy instanceof ObjectValue) {
                instance = strategy.value;
            } else if (strategy instanceof ObjectReference) {
                instance = this._config.referenceEvaluator.eval(strategy.ref);
            }
            return instance;
        } },
    dependsOn: { value: function value(definition)
        {
            if (definition instanceof ObjectDefinition && definition.dependsOn instanceof Array && definition.dependsOn.length > 0) {
                var id = void 0;
                var len = definition.dependsOn.length;
                for (var i = 0; i < len; i++) {
                    id = definition.dependsOn[i];
                    if (this.hasObjectDefinition(id)) {
                        this.getObject(id);
                    }
                }
            }
        } },
    eval: { value: function value(_value) {
            var evaluators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (!(evaluators instanceof Array) || evaluators.length === 0) {
                return _value;
            }
            this._evaluator.clear();
            var o = void 0;
            var s = evaluators.length;
            var a = [];
            for (var i = 0; i < s; i++) {
                o = evaluators[i];
                if (o === null) {
                    continue;
                }
                if (o instanceof String || typeof o === 'string') {
                    o = this.getObject(o);
                }
                if (o instanceof Evaluable) {
                    a.push(o);
                }
            }
            if (a.length > 0) {
                this._evaluator.add(a);
                _value = this._evaluator.eval(_value);
                this._evaluator.clear();
            }
            return _value;
        } },
    generates: { value: function value(definition)
        {
            if (definition instanceof ObjectDefinition && definition.generates instanceof Array) {
                var ar = definition.generates;
                var len = ar.length;
                if (len > 0) {
                    for (var i = 0; i < len; i++) {
                        var id = ar[i];
                        if (this.hasObjectDefinition(id)) {
                            this.getObject(id);
                        }
                    }
                }
            }
        } },
    invokeDestroyMethod: { value: function value(id) {
            if (this.hasObjectDefinition(id) && this._singletons.has(id)) {
                var definition = this.getObjectDefinition(id);
                var o = this._singletons.get(id);
                var name = definition.destroyMethodName || null;
                if (name === null && this.config !== null) {
                    name = this.config.defaultDestroyMethod;
                }
                if (name && name in o && o[name] instanceof Function) {
                    o[name].call(o);
                }
            }
        } },
    invokeInitMethod: { value: function value(o) {
            var definition = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (definition && definition instanceof ObjectDefinition) {
                var name = definition.initMethodName || null;
                if (name === null && this.config) {
                    name = this.config.defaultInitMethod || null;
                }
                if (name && name in o && o[name] instanceof Function) {
                    o[name].call(o);
                }
            }
        } },
    populateIdentifiable: { value: function value(o) {
            var definition = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (definition && definition instanceof ObjectDefinition) {
                if (definition.singleton && isIdentifiable(o)) {
                    if (definition.identify === true || this.config.identify === true && definition.identify !== false) {
                        o.id = definition.id;
                    }
                }
            }
        } },
    populateProperties: { value: function value(o) {
            var definition                      = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (definition && definition instanceof ObjectDefinition) {
                var properties = definition.properties;
                if (properties && properties instanceof Array && properties.length > 0) {
                    var id = definition.id;
                    var len = properties.length;
                    for (var i = 0; i < len; i++) {
                        this.populateProperty(o, properties[i], id);
                    }
                }
            }
        } },
    populateProperty: { value: function value(o, prop, id) {
            if (o === null) {
                this.warn(this + " populate a new property failed, the object not must be 'null' or 'undefined', see the factory with the object definition '" + id + "'.");
                return;
            }
            var name = prop.name;
            var value = prop.value;
            if (name === MagicReference.INIT) {
                if (prop.policy === ObjectAttribute.REFERENCE && (value instanceof String || typeof value === 'string')) {
                    value = this._config.referenceEvaluator.eval(value);
                } else if (prop.policy === ObjectAttribute.CONFIG) {
                    value = this.config.configEvaluator.eval(value);
                } else if (prop.policy === ObjectAttribute.LOCALE) {
                    value = this.config.localeEvaluator.eval(value);
                }
                if (prop.evaluators && prop.evaluators.length > 0) {
                    value = this.eval(value, prop.evaluators);
                }
                if (value) {
                    for (var member in value) {
                        if (member in o) {
                            o[member] = value[member];
                        } else {
                            this.warn(this + " populateProperty failed with the magic #init name, the " + member + " attribute is not declared on the object with the object definition '" + id + "'.");
                        }
                    }
                } else {
                    this.warn(this + " populate a new property failed with the magic #init name, the object to enumerate not must be null, see the factory with the object definition '" + id + "'.");
                }
                return;
            }
            if (!(name in o)) {
                this.warn(this + " populate a new property failed with the " + name + " attribute, this property is not registered in the object, see the object definition '" + id + "'.");
                return;
            }
            try {
                if (prop.policy === ObjectAttribute.CALLBACK) {
                    if (value instanceof String || typeof value === 'string') {
                        value = this._config.referenceEvaluator.eval(value);
                        if (value === null) {
                            this.warn(this + " populateProperty with the name '" + name + "' return a null callback reference, see the object definition with the id : " + id);
                        }
                    }
                    if (value instanceof Function) {
                        if (prop.scope) {
                            if (prop.args instanceof Array) {
                                value = value.bind.apply(prop.scope, [prop.scope].concat(this.createArguments(prop.args, id)));
                            } else {
                                value = value.bind(prop.scope);
                            }
                        }
                    } else {
                        value = null;
                    }
                } else if (prop.policy === ObjectAttribute.REFERENCE) {
                    value = this._config.referenceEvaluator.eval(value);
                    if (value === null) {
                        this.warn(this + " populateProperty with the name '" + name + "' return a 'null' factory reference, see the object definition with the id : " + id);
                    }
                } else if (prop.policy === ObjectAttribute.CONFIG) {
                    value = this.config.configEvaluator.eval(value);
                    if (value === null) {
                        this.warn(this + " populateProperty with the name '" + name + "' return a 'null' config reference, see the object definition with the id : " + id);
                    }
                } else if (prop.policy === ObjectAttribute.LOCALE) {
                    value = this.config.localeEvaluator.eval(value);
                    if (value === null) {
                        this.warn(this + " populateProperty with the name '" + name + "' return a null locale reference, see the object definition with the id : " + id);
                    }
                } else if (o[name] instanceof Function) {
                    if (prop.policy === ObjectAttribute.ARGUMENTS) {
                        o[name].apply(o, this.createArguments(value, id));
                        return;
                    } else {
                        o[name]();
                        return;
                    }
                }
                if (prop.evaluators && prop.evaluators.length > 0) {
                    value = this.eval(value, prop.evaluators);
                }
                o[name] = value;
            } catch (e) {
                this.warn(this + " populateProperty failed with the name '" + name + ", see the object definition '" + id + "', error: " + e.toString());
            }
        } },
    registerListeners: { value: function value(o, listeners) {
            if (o === null || listeners === null || !(listeners instanceof Array)) {
                return;
            }
            var len = listeners.length;
            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    try {
                        var entry = listeners[i];
                        var dispatcher = this._config.referenceEvaluator.eval(entry.dispatcher);
                        if (dispatcher && entry.type !== null) {
                            var listener = void 0;
                            if (dispatcher instanceof IEventDispatcher) {
                                if (entry.method && entry.method in o && o[entry.method] instanceof Function) {
                                    listener = o[entry.method].bind(o);
                                } else if (o instanceof EventListener) {
                                    listener = o;
                                }
                                dispatcher.addEventListener(entry.type, listener, entry.useCapture, entry.priority);
                            } else if ("addEventListener" in dispatcher && dispatcher.addEventListener instanceof Function)
                                {
                                    if (entry.method && entry.method in o && o[entry.method] instanceof Function) {
                                        listener = o[entry.method].bind(o);
                                    } else if (o instanceof EventListener) {
                                        listener = o.handleEvent.bind(o);
                                    }
                                    dispatcher.addEventListener(entry.type, listener, entry.useCapture);
                                }
                        }
                    } catch (e) {
                        this.warn(this + " registerListeners failed with the target '" + o + "' , in the collection of this listeners at {" + i + "} : " + e.toString());
                    }
                }
            }
        } },
    registerReceivers: { value: function value(o) {
            var receivers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            if (!(receivers instanceof Array) || receivers.length === 0) {
                return;
            }
            var len = receivers.length;
            for (var i = 0; i < len; i++) {
                try {
                    var receiver = receivers[i];
                    var signaler = this._config.referenceEvaluator.eval(receiver.signal);
                    var slot = null;
                    if (signaler instanceof Signaler) {
                        if ((receiver.slot instanceof String || typeof receiver.slot === 'string') && receiver.slot in o && o[receiver.slot] instanceof Function) {
                            slot = o[receiver.slot];
                        } else if (o instanceof Receiver) {
                            slot = o;
                        }
                        if (slot instanceof Receiver || slot instanceof Function) {
                            signaler.connect(slot, receiver.priority, receiver.autoDisconnect);
                        }
                    }
                } catch (e) {
                    this.warn(this + " registerReceivers failed with the target '" + o + "' , in the collection of this receivers at {" + i + "} : " + e.toString());
                }
            }
        } }
});

function ReferenceEvaluator(factory) {
    Object.defineProperties(this, {
        factory: { value: factory instanceof ObjectFactory ? factory : null, writable: true },
        separator: { value: ".", writable: true },
        undefineable: { value: null, writable: true },
        throwError: {
            get: function get() {
                return this._propEvaluator.throwError;
            },
            set: function set(flag) {
                this._propEvaluator.throwError = flag;
            }
        },
        _propEvaluator: { value: new PropertyEvaluator(), writable: true }
    });
}
ReferenceEvaluator.prototype = Object.create(Evaluable.prototype, {
    constructor: { value: ReferenceEvaluator },
    eval: { value: function value(o) {
            if (this.factory instanceof ObjectFactory && (o instanceof String || typeof o === 'string')) {
                var exp = String(o);
                if (exp.length > 0) {
                    var root;
                    try {
                        root = this.factory.config.root;
                    } catch (e) {
                    }
                    switch (exp) {
                        case MagicReference.CONFIG:
                            {
                                return this.factory.config.config;
                            }
                        case MagicReference.LOCALE:
                            {
                                return this.factory.config.locale;
                            }
                        case MagicReference.PARAMS:
                            {
                                return this.factory.config.parameters;
                            }
                        case MagicReference.THIS:
                            {
                                return this.factory;
                            }
                        case MagicReference.ROOT:
                            {
                                return root;
                            }
                        case MagicReference.STAGE:
                            {
                                var stage = this.factory.config.stage;
                                if (stage !== null) {
                                    return stage;
                                } else if (root && "stage" in root && root.stage !== null) {
                                    return root.stage;
                                } else {
                                    return this.undefineable;
                                }
                                break;
                            }
                        default:
                            {
                                var members = exp.split(this.separator);
                                if (members.length > 0) {
                                    var ref = members.shift();
                                    var value = this.factory.getObject(ref);
                                    if (value && members.length > 0) {
                                        this._propEvaluator.target = value;
                                        value = this._propEvaluator.eval(members.join("."));
                                        this._propEvaluator.target = null;
                                    }
                                    return value;
                                }
                            }
                    }
                }
            }
            return this.undefineable;
        } }
});

function getDefinitionByName( name , domain = null )
{
    if( ( name instanceof String ) || typeof(name) === 'string' )
    {
        let elements = name.split('.');
        if( elements.length > 0 )
        {
            try
            {
                let o = domain;
                elements.forEach( ( element ) =>
                {
                    if(o.hasOwnProperty(element) )
                    {
                        o = o[element] ;
                    }
                    else
                    {
                        return undefined ;
                    }
                });
                return o ;
            }
            catch( ignored ) {}
        }
    }
    return undefined ;
}

var TypePolicy = Object.defineProperties({}, {
  ALIAS: { value: "alias", enumerable: true },
  ALL: { value: "all", enumerable: true },
  EXPRESSION: { value: "expression", enumerable: true },
  NONE: { value: "none", enumerable: true }
});

function TypeEvaluator() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    Object.defineProperties(this, {
        config: { value: config instanceof ObjectConfig ? config : null, writable: true },
        throwError: { value: false, writable: true }
    });
}
TypeEvaluator.prototype = Object.create(Evaluable.prototype, {
    constructor: { value: TypeEvaluator },
    eval: { value: function value(o) {
            if (o instanceof Function) {
                return o;
            } else if (o instanceof String || typeof o === 'string') {
                var type = String(o);
                var config = this.config;
                if (config && config instanceof ObjectConfig) {
                    var policy = config.typePolicy;
                    if (policy !== TypePolicy.NONE) {
                        if (policy === TypePolicy.ALL || policy === TypePolicy.ALIAS) {
                            var aliases = config.typeAliases;
                            if (aliases instanceof ArrayMap && aliases.has(type)) {
                                type = aliases.get(type);
                            }
                        }
                        if (policy === TypePolicy.ALL || policy === TypePolicy.EXPRESSION) {
                            if (config.typeExpression instanceof ExpressionFormatter) {
                                type = config.typeExpression.format(type);
                            }
                        }
                    }
                }
                try {
                    var func = getDefinitionByName(type, config.domain);
                    if (func instanceof Function) {
                        return func;
                    }
                } catch (e) {
                    if (this.throwError) {
                        throw new EvalError(this + " eval failed : " + e.toString());
                    }
                }
            }
            return null;
        } }
});

function ObjectConfig() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    Object.defineProperties(this, {
        defaultDestroyMethod: { value: null, writable: true, enumerable: true },
        defaultInitMethod: { value: null, writable: true, enumerable: true },
        domain: { value: null, writable: true, enumerable: true },
        identify: { value: false, writable: true, enumerable: true },
        lazyInit: { value: false, writable: true, enumerable: true },
        lock: { value: false, writable: true, enumerable: true },
        parameters: { value: null, writable: true, enumerable: true },
        root: { value: null, writable: true, enumerable: true },
        stage: { value: null, writable: true, enumerable: true },
        useLogger: { value: false, writable: true, enumerable: true },
        _config: { value: {}, writable: true },
        _configEvaluator: { value: new ConfigEvaluator(this), writable: true },
        _locale: { value: {}, writable: true },
        _localeEvaluator: { value: new LocaleEvaluator(this), writable: true },
        _referenceEvaluator: { value: new ReferenceEvaluator(), writable: true },
        _typeAliases: { value: new ArrayMap(), writable: true },
        _typeEvaluator: { value: new TypeEvaluator(this), writable: true },
        _typeExpression: { value: new ExpressionFormatter(), writable: true },
        _typePolicy: { value: TypePolicy.NONE, writable: true }
    });
    this.throwError = false;
    if (init) {
        this.initialize(init);
    }
}
Object.defineProperties(ObjectConfig, {
    TYPE_ALIAS: { value: 'alias', enumerable: true }
});
ObjectConfig.prototype = Object.create(Object.prototype, {
    constructor: { value: ObjectConfig },
    config: {
        get: function get() {
            return this._config;
        },
        set: function set(init) {
            for (var prop in init) {
                this._config[prop] = init[prop];
            }
        }
    },
    configEvaluator: {
        get: function get() {
            return this._configEvaluator;
        }
    },
    locale: {
        get: function get() {
            return this._locale;
        },
        set: function set(init) {
            for (var prop in init) {
                this._locale[prop] = init[prop];
            }
        }
    },
    localeEvaluator: {
        get: function get() {
            return this._localeEvaluator;
        }
    },
    referenceEvaluator: {
        get: function get() {
            return this._referenceEvaluator;
        }
    },
    throwError: {
        get: function get() {
            return this._configEvaluator.throwError && this._localeEvaluator.throwError && this._typeEvaluator.throwError && this._referenceEvaluator.throwError;
        },
        set: function set(flag) {
            this._configEvaluator.throwError = flag;
            this._localeEvaluator.throwError = flag;
            this._referenceEvaluator.throwError = flag;
            this._typeEvaluator.throwError = flag;
        }
    },
    typeAliases: {
        get: function get() {
            return this._typeAliases;
        },
        set: function set(aliases) {
            if (aliases instanceof ArrayMap) {
                var it = aliases.iterator();
                while (it.hasNext()) {
                    var next = it.next();
                    var key = it.key();
                    this._typeAliases.set(key, next);
                }
            } else if (aliases instanceof Array) {
                var len = aliases.length;
                if (len > 0) {
                    while (--len > -1) {
                        var item = aliases[len];
                        if (item !== null && ObjectConfig.TYPE_ALIAS in item && ObjectAttribute.TYPE in item) {
                            this._typeAliases.set(String(item[ObjectConfig.TYPE_ALIAS]), String(item[ObjectAttribute.TYPE]));
                        }
                    }
                }
            }
        }
    },
    typeEvaluator: {
        get: function get() {
            return this._typeEvaluator;
        }
    },
    typeExpression: {
        get: function get() {
            return this._typeExpression;
        },
        set: function set(expressions                              ) {
            if (expressions instanceof ExpressionFormatter) {
                this._typeExpression = expressions;
            } else if (expressions instanceof Array) {
                if (this._typeExpression === null) {
                    this._typeExpression = new ExpressionFormatter();
                }
                var len = expressions.length;
                if (len > 0) {
                    while (--len > -1) {
                        var item = expressions[len];
                        if (item !== null && ObjectAttribute.NAME in item && ObjectAttribute.VALUE in item) {
                            this._typeExpression.set(String(item[ObjectAttribute.NAME]), String(item[ObjectAttribute.VALUE]));
                        }
                    }
                }
            } else {
                this._typeExpression = new ExpressionFormatter();
            }
        }
    },
    typePolicy: {
        get: function get() {
            return this._typePolicy;
        },
        set: function set(policy) {
            switch (policy) {
                case TypePolicy.ALIAS:
                case TypePolicy.EXPRESSION:
                case TypePolicy.ALL:
                    {
                        this._typePolicy = policy;
                        break;
                    }
                default:
                    {
                        this._typePolicy = TypePolicy.NONE;
                    }
            }
        }
    },
    initialize: { value: function value(init) {
            if (init === null) {
                return;
            }
            for (var prop in init) {
                if (prop in this) {
                    this[prop] = init[prop];
                }
            }
        } },
    setConfigTarget: { value: function value() {
            var o = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            this._config = o || {};
        } },
    setLocaleTarget: { value: function value() {
            var o = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            this._locale = o || {};
        } },
    toString: { value: function value() {
            return '[ObjectConfig]';
        } }
});

function Parameters(parameters) {
    Object.defineProperties(this, {
        parameters: { value: parameters, writable: true },
        _evaluators: { value: new MultiEvaluator(), writable: true }
    });
    this._evaluators.autoClear = true;
}
Parameters.prototype = Object.create(Object.prototype, {
    constructor: { value: Parameters },
    contains: { value: function value(name) {
            return this.parameters && name && name in this.parameters && this.parameters[name] !== null;
        } },
    get: { value: function value(name) {
            if (this.parameters && this.contains(name)) {
                for (var _len = arguments.length, rest = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                    rest[_key - 1] = arguments[_key];
                }
                if (rest.length === 0) {
                    return this.parameters[name];
                } else {
                    this._evaluators.add(rest);
                    return this._evaluators.eval(this.parameters[name]);
                }
            } else {
                return null;
            }
        } },
    toString: { value: function value() {
            return '[Parameters]';
        } }
});

/**
 * The {@link system.ioc} library provides a simple and strong implementation of the <strong>Inversion of Control</strong> (<b>{@link https://en.wikipedia.org/wiki/Inversion_of_control|IoC}</b>) principle.
 * <p><b>IoC</b> is also known as <b>dependency injection</b> (DI). It is a process whereby objects define their dependencies, that is, the other objects they work with, only through constructor arguments, arguments to a factory method, or properties that are set on the object instance after it is constructed or returned from a factory method.</p>
 * <p> The container then injects those dependencies when it creates the <b>object definitions</b>. This process is fundamentally the inverse, hence the name Inversion of Control (IoC), of the <b>object definition</b> itself controlling the instantiation or location of its dependencies by using direct construction of classes, or a more complex mechanism.</p>
 * @summary The {@link system.ioc} library provides a simple et strong implementation of the <strong>Inversion of Control</strong> (<b>{@link https://en.wikipedia.org/wiki/Inversion_of_control|IoC}</b>) principle.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.ioc
 * @memberof system
 * @example
 * var Point = function( x , y )
 * {
 *     this.x = x ;
 *     this.y = y ;
 *     console.log("constructor:" + this.toString() ) ;
 * };
 *
 * Point.prototype.test = function( message = null )
 * {
 *     console.log( 'test:' + this.toString() + " message:" + message ) ;
 * }
 *
 * Point.prototype.toString = function()
 * {
 *     return "[Point x:" + this.x + " y:" + this.y + "]" ;
 * } ;
 *
 * var ObjectFactory = system.ioc.ObjectFactory ;
 *
 * var factory = new ObjectFactory();
 * var config  = factory.config ;
 *
 * config.setConfigTarget
 * ({
 *     origin : { x : 10 , y : 20 }
 * })
 *
 * config.setLocaleTarget
 * ({
 *     messages :
 *     {
 *         test : 'test'
 *     }
 * })
 *
 * var objects =
 * [
 *     {
 *         id   : "position" ,
 *         type : "Point" ,
 *         args : [ { value : 2 } , { ref : 'origin.y' }],
 *         properties :
 *         [
 *             { name : "x" , ref   :'origin.x' } ,
 *             { name : "y" , value : 100       }
 *         ]
 *     },
 *     {
 *         id         : "origin" ,
 *         type       : "Point" ,
 *         singleton  : true ,
 *         args       : [ { config : 'origin.x' } , { value : 20 }] ,
 *         properties :
 *         [
 *             { name : 'test' , args : [ { locale : 'messages.test' } ] }
 *         ]
 *     }
 * ];
 *
 * factory.run( objects );
 *
 * trace( factory.getObject('position') ) ;
 */
var ioc = Object.assign({
  logger: logger,
  MagicReference: MagicReference,
  ObjectArgument: ObjectArgument,
  ObjectAttribute: ObjectAttribute,
  ObjectConfig: ObjectConfig,
  ObjectDefinition: ObjectDefinition,
  ObjectDefinitionContainer: ObjectDefinitionContainer,
  ObjectFactory: ObjectFactory,
  ObjectListener: ObjectListener,
  ObjectMethod: ObjectMethod,
  ObjectOrder: ObjectOrder,
  ObjectProperty: ObjectProperty,
  ObjectReceiver: ObjectReceiver,
  ObjectScope: ObjectScope,
  Parameters: Parameters,
  TypePolicy: TypePolicy
});

function isLoggable(target) {
    if (target) {
        return target instanceof Loggable || 'logger' in target && (target.logger === null || target.logger instanceof Logger);
    }
    return false;
}
function Loggable() {
    Object.defineProperties(this, {
        _logger: { value: null, writable: true }
    });
}
Loggable.prototype = Object.create(Object.prototype, {
    constructor: { value: Loggable },
    logger: {
        get: function get() {
            return this._logger;
        },
        set: function set(logger) {
            this._logger = logger instanceof Logger ? logger : null;
        }
    }
});

function LineFormattedTarget() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    LoggerTarget.call(this);
    Object.defineProperties(this, {
        _lineNumber: { value: 1, writable: true }
    });
    this.includeChannel = false;
    this.includeDate = false;
    this.includeLevel = false;
    this.includeLines = false;
    this.includeMilliseconds = false;
    this.includeTime = false;
    this.separator = " ";
    if (init) {
        for (var prop in init) {
            if (this.hasOwnProperty(prop)) {
                this[prop] = init[prop];
            }
        }
    }
}
LineFormattedTarget.prototype = Object.create(LoggerTarget.prototype, {
    constructor: { value: LineFormattedTarget, writable: true },
    internalLog: { value: function value(message, level)
        {
        } },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } },
    logEntry: { value: function value(entry) {
            var message = this.formatMessage(entry.message, LoggerLevel.getLevelString(entry.level), entry.channel, new Date());
            this.internalLog(message, entry.level);
        } },
    resetLineNumber: { value: function value() {
            this._lineNumber = 1;
        } },
    formatDate: { value: function value(d) {
            var date = "";
            date += this.getDigit(d.getDate());
            date += "/" + this.getDigit(d.getMonth() + 1);
            date += "/" + d.getFullYear();
            return date;
        } },
    formatLevel: { value: function value(level) {
            return '[' + level + ']';
        } },
    formatLines: { value: function value() {
            return "[" + this._lineNumber++ + "]";
        } },
    formatMessage: { value: function value(message, level, channel, date         ) {
            var msg = "";
            if (this.includeLines) {
                msg += this.formatLines() + this.separator;
            }
            if (this.includeDate || this.includeTime) {
                date = date || new Date();
                if (this.includeDate) {
                    msg += this.formatDate(date) + this.separator;
                }
                if (this.includeTime) {
                    msg += this.formatTime(date) + this.separator;
                }
            }
            if (this.includeLevel) {
                msg += this.formatLevel(level || "") + this.separator;
            }
            if (this.includeChannel) {
                msg += (channel || "") + this.separator;
            }
            msg += message;
            return msg;
        } },
    formatTime: { value: function value(d) {
            var time = "";
            time += this.getDigit(d.getHours());
            time += ":" + this.getDigit(d.getMinutes());
            time += ":" + this.getDigit(d.getSeconds());
            if (this.includeMilliseconds) {
                time += ":" + this.getDigit(d.getMilliseconds());
            }
            return time;
        } },
    getDigit: { value: function value(n) {
            if (isNaN(n)) {
                return "00";
            }
            return (n < 10 ? "0" : "") + n;
        } }
});

function ConsoleTarget() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    LineFormattedTarget.call(this, init);
}
ConsoleTarget.prototype = Object.create(LineFormattedTarget.prototype, {
    constructor: { value: ConsoleTarget },
    internalLog: { value: function value(message, level)
        {
            if (console) {
                switch (level) {
                    case LoggerLevel.CRITICAL:
                        {
                            console.trace(message);
                            break;
                        }
                    case LoggerLevel.DEBUG:
                        {
                            console.log(message);
                            break;
                        }
                    case LoggerLevel.ERROR:
                        {
                            console.error(message);
                            break;
                        }
                    case LoggerLevel.INFO:
                        {
                            console.info(message);
                            break;
                        }
                    case LoggerLevel.WARNING:
                        {
                            console.warn(message);
                            break;
                        }
                    default:
                    case LoggerLevel.ALL:
                        {
                            console.log(message);
                            break;
                        }
                }
            } else {
                throw new new ReferenceError('The console reference is unsupported.')();
            }
        } }
});

function trace( context )
{
    if( console )
    {
        console.log( context ) ;
    }
}

function TraceTarget() {
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    LineFormattedTarget.call(this, init);
}
TraceTarget.prototype = Object.create(LineFormattedTarget.prototype, {
    constructor: { value: TraceTarget },
    internalLog: { value: function value(message, level)
        {
            trace(message);
        } }
});

/**
 * The {@link system.logging} library defines functions and classes which implement a flexible event logging system for applications and libraries.
 * @summary The {@link system.logging} library defines functions and classes which implement a flexible event logging system for applications and libraries.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.logging
 * @memberof system
 * @example
 * var logger = Log.getLogger('channel') ;
 *
 * var target = new ConsoleTarget
 * ({
 *     includeChannel      : true  ,
 *     includeDate         : false ,
 *     includeLevel        : true  ,
 *     includeLines        : true  ,
 *     includeMilliseconds : true  ,
 *     includeTime         : true
 * }) ;
 *
 * target.filters = ['*'] ;
 * target.level   = LoggerLevel.ALL ;
 *
 * logger.debug( 'hello {0}, love it.' , 'VEGAS' ) ;
 * logger.critical( 'hello {0}, it\'s critical.' , 'VEGAS' ) ;
 * logger.info( 'hello, my name is {0}' , 'VEGAS' ) ;
 * logger.error( 'hello {0}, an error is invoked.' , 'VEGAS' ) ;
 * logger.warning( 'hello {0}, don\'t forget me.' , 'VEGAS' ) ;
 * logger.wtf( 'hello {0} ! WHAT ??' , 'VEGAS' ) ;
 */
var logging = Object.assign({
  isLoggable: isLoggable,
  Log: Log,
  Loggable: Loggable,
  Logger: Logger,
  LoggerEntry: LoggerEntry,
  LoggerFactory: LoggerFactory,
  LoggerLevel: LoggerLevel,
  LoggerTarget: LoggerTarget,
  targets: Object.assign({
    ConsoleTarget: ConsoleTarget,
    LineFormattedTarget: LineFormattedTarget,
    TraceTarget: TraceTarget
  })
});

function isRule(target) {
    if (target) {
        return target instanceof Rule || 'eval' in target && target['eval'] instanceof Function;
    }
    return false;
}
function Rule() {}
Rule.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Rule },
    eval: { writable: true, value: function value() {
        } },
    toString: { value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function BooleanRule(condition) {
  Object.defineProperties(this, {
    condition: { value: condition, enumerable: true, writable: true }
  });
}
BooleanRule.prototype = Object.create(Rule.prototype);
BooleanRule.prototype.constructor = BooleanRule;
BooleanRule.prototype.eval = function () {
  return this.condition instanceof Rule ? this.condition.eval() : Boolean(this.condition);
};

function ElseIf() {
    var rule = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var then = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    this.rule = rule instanceof Rule ? rule : new BooleanRule(rule);
    this.then = then;
}
ElseIf.prototype = Object.create(Rule.prototype, {
    constructor: { value: ElseIf, writable: true },
    eval: { writable: true, value: function value() {
            if (this.rule && this.rule instanceof Rule) {
                return this.rule.eval();
            } else {
                return false;
            }
        } }
});

function EmptyString() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  this.value = value;
}
EmptyString.prototype = Object.create(Rule.prototype);
EmptyString.prototype.constructor = EmptyString;
EmptyString.prototype.eval = function () {
  return this.value === "";
};

function ElseIfEmptyString() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var then = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    ElseIf.call(this, new EmptyString(value), then);
}
ElseIfEmptyString.prototype = Object.create(ElseIf.prototype, {
    constructor: { writable: true, value: ElseIfEmptyString }
});

function Equals() {
    var value1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var value2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    this.value1 = value1;
    this.value2 = value2;
}
Equals.prototype = Object.create(Rule.prototype);
Equals.prototype.constructor = Equals;
Equals.prototype.eval = function () {
    if (this.value1 === this.value2) {
        return true;
    } else if (this.value1 instanceof Rule && this.value2 instanceof Rule) {
        return this.value1.eval() === this.value2.eval();
    } else if (isEquatable(this.value1)) {
        return this.value1.equals(this.value2);
    } else {
        return false;
    }
};

function ElseIfEquals(value1, value2) {
    var then = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    ElseIf.call(this, new Equals(value1, value2), then);
}
ElseIfEquals.prototype = Object.create(ElseIf.prototype, {
    constructor: { writable: true, value: ElseIfEquals }
});

function False() {
  var condition = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  this.condition = condition;
}
False.prototype = Object.create(Rule.prototype);
False.prototype.constructor = False;
False.prototype.eval = function () {
  return (this.condition instanceof Rule ? this.condition.eval() : Boolean(this.condition)) === false;
};

function ElseIfFalse(value) {
    var then = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    ElseIf.call(this, new False(value), then);
}
ElseIfFalse.prototype = Object.create(ElseIf.prototype, {
    constructor: { writable: true, value: ElseIfFalse }
});

function Null() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
  var strict = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  this.value = value;
  this.strict = Boolean(strict);
}
Null.prototype = Object.create(Rule.prototype);
Null.prototype.constructor = Null;
Null.prototype.eval = function () {
  if (this.strict) {
    return this.value === null;
  } else {
    return this.value == null;
  }
};

function ElseIfNull(value) {
    var then = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var strict = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    ElseIf.call(this, new Null(value, strict), then);
}
ElseIfNull.prototype = Object.create(ElseIf.prototype, {
    constructor: { writable: true, value: ElseIfNull }
});

function True() {
  var condition = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  this.condition = condition;
}
True.prototype = Object.create(Rule.prototype);
True.prototype.constructor = True;
True.prototype.eval = function () {
  return (this.condition instanceof Rule ? this.condition.eval() : Boolean(this.condition)) === true;
};

function ElseIfTrue(condition) {
    var then = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    ElseIf.call(this, new True(condition), then);
}
ElseIfTrue.prototype = Object.create(ElseIf.prototype, {
    constructor: { writable: true, value: ElseIfTrue }
});

function Undefined() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
  this.value = value;
}
Undefined.prototype = Object.create(Rule.prototype);
Undefined.prototype.constructor = Undefined;
Undefined.prototype.eval = function () {
  return this.value === undefined;
};

function ElseIfUndefined(value) {
    var then = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    ElseIf.call(this, new Undefined(value), then);
}
ElseIfUndefined.prototype = Object.create(ElseIf.prototype, {
    constructor: { writable: true, value: ElseIfUndefined }
});

function Zero() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  this.value = value;
}
Zero.prototype = Object.create(Rule.prototype);
Zero.prototype.constructor = Zero;
Zero.prototype.eval = function () {
  return this.value === 0;
};

function ElseIfZero(value) {
    var then = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    ElseIf.call(this, new Zero(value), then);
}
ElseIfZero.prototype = Object.create(ElseIf.prototype, {
    constructor: { writable: true, value: ElseIfZero }
});

function isBoolean( object )
{
    return (typeof(object) === 'boolean') || (object instanceof Boolean ) ;
}

function IfTask()
{
    var rule = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var thenTask = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var elseTask = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    Action.call(this);
    Object.defineProperties(this, {
        errorIt: { value: new Signal() },
        throwError: { value: false, writable: true, enumerable: true },
        _done: { value: false, writable: true },
        _elseIfTasks: { value: [] },
        _elseTask: {
            value: elseTask instanceof Action ? elseTask : null,
            writable: true
        },
        _rule: {
            value: rule instanceof Rule ? rule : new BooleanRule(rule),
            writable: true
        },
        _thenTask: {
            value: thenTask instanceof Action ? thenTask : null,
            writable: true
        }
    });
    for (var _len = arguments.length, elseIfTasks = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
        elseIfTasks[_key - 3] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfTask.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: IfTask },
    elseIfTasks: { get: function get() {
            return this._elseIfTasks;
        } },
    elseTask: { get: function get() {
            return this._elseTask;
        } },
    rule: {
        get: function get() {
            return this._rule;
        },
        set: function set(rule) {
            this._rule = rule instanceof Rule ? rule : new BooleanRule(rule);
        }
    },
    thenTask: { get: function get() {
            return this._thenTask;
        } },
    addElse: { value: function value(action) {
            if (this._elseTask) {
                throw new Error(this + " addElse failed, you must not nest more than one <else> into <if>");
            } else if (action instanceof Action) {
                this._elseTask = action;
            }
            return this;
        } },
    addElseIf: { value: function value()
        {
            for (var _len2 = arguments.length, elseIfTask = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                elseIfTask[_key2] = arguments[_key2];
            }
            if (elseIfTask && elseIfTask.length > 0) {
                var ei = void 0;
                var len = elseIfTask.length;
                for (var i = 0; i < len; i++) {
                    ei = null;
                    if (elseIfTask[i] instanceof ElseIf) {
                        ei = elseIfTask[i];
                    } else if ((elseIfTask[i] instanceof Rule || isBoolean(elseIfTask[i])) && elseIfTask[i + 1] instanceof Action) {
                        ei = new ElseIf(elseIfTask[i], elseIfTask[i + 1]);
                        i++;
                    }
                    if (ei) {
                        this._elseIfTasks.push(ei);
                    }
                }
            }
            return this;
        } },
    addRule: { value: function value(rule)
        {
            if (this._rule) {
                throw new Error(this + " addRule failed, you must not nest more than one <condition> into <if>");
            } else {
                this._rule = rule instanceof Rule ? rule : new BooleanRule(rule);
            }
            return this;
        } },
    addThen: { value: function value(action) {
            if (this._thenTask) {
                throw new Error(this + " addThen failed, you must not nest more than one <then> into <if>");
            } else if (action instanceof Action) {
                this._thenTask = action;
            }
            return this;
        } },
    clear: { value: function value() {
            this._rule = null;
            this._elseIfTasks.length = 0;
            this._elseTask = null;
            this._thenTask = null;
            return this;
        } },
    deleteElseIf: { value: function value()
        {
            this._elseIfTasks.length = 0;
            return this;
        } },
    deleteElse: { value: function value()
        {
            this._elseTask = null;
            return this;
        } },
    deleteRule: { value: function value() {
            this._rule = null;
            return this;
        } },
    deleteThen: { value: function value()
        {
            this._thenTask = null;
            return this;
        } },
    notifyError: { value: function value(message)
        {
            this._running = false;
            this._phase = TaskPhase.ERROR;
            this.errorIt.emit(message, this);
            if (this.throwError) {
                throw new Error(message);
            }
        } },
    run: { value: function value() {
            if (this.running) {
                return;
            }
            this._done = false;
            this.notifyStarted();
            if (!this._rule || !(this._rule instanceof Rule)) {
                this.notifyError(this + " run failed, the 'conditional rule' of the task not must be null.");
                this.notifyFinished();
                return;
            }
            if (this._rule.eval()) {
                if (this._thenTask instanceof Action) {
                    this._execute(this._thenTask);
                } else if (this.throwError) {
                    this.notifyError(this + " run failed, the 'then' action not must be null.");
                }
            } else {
                if (this._elseIfTasks.length > 0) {
                    var ei = void 0;
                    var len = this._elseIfTasks.length;
                    for (var i = 0; i < len && !this._done; i++) {
                        ei = this._elseIfTasks[i];
                        if (ei instanceof ElseIf && ei.eval()) {
                            this._execute(ei.then);
                        }
                    }
                }
                if (!this._done && this._elseTask) {
                    this._execute(this._elseTask);
                }
            }
            if (!this._done) {
                if (this.throwError) {
                    this.notifyError(this + " run failed, the 'then' action not must be null.");
                } else {
                    this.notifyFinished();
                }
            }
        } },
    _execute: { value: function value(action) {
            if (action instanceof Action) {
                this._done = true;
                action.finishIt.connect(this._finishTask.bind(this), 1, true);
                action.run();
            }
        } },
    _finishTask: {
        value: function value() {
            this.notifyFinished();
        }
    }
});

function IfEmptyString(value)
{
    var thenTask = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var elseTask = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    IfTask.call(this, new EmptyString(value), thenTask, elseTask);
    for (var _len = arguments.length, elseIfTasks = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
        elseIfTasks[_key - 3] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfEmptyString.prototype = Object.create(IfTask.prototype, {
    constructor: { writable: true, value: IfEmptyString }
});

function IfEquals(value1, value2)
{
    var thenTask = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var elseTask = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    IfTask.call(this, new Equals(value1, value2), thenTask, elseTask);
    for (var _len = arguments.length, elseIfTasks = Array(_len > 4 ? _len - 4 : 0), _key = 4; _key < _len; _key++) {
        elseIfTasks[_key - 4] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfEquals.prototype = Object.create(IfTask.prototype, {
    constructor: { writable: true, value: IfEquals }
});

function IfFalse(condition)
{
    var thenTask = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var elseTask = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    IfTask.call(this, new False(condition), thenTask, elseTask);
    for (var _len = arguments.length, elseIfTasks = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
        elseIfTasks[_key - 3] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfFalse.prototype = Object.create(IfTask.prototype, {
    constructor: { writable: true, value: IfFalse }
});

function IfNull(value)
{
    var strict = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var thenTask            = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    var elseTask            = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    IfTask.call(this, new Null(value, strict), thenTask, elseTask);
    for (var _len = arguments.length, elseIfTasks = Array(_len > 4 ? _len - 4 : 0), _key = 4; _key < _len; _key++) {
        elseIfTasks[_key - 4] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfNull.prototype = Object.create(IfTask.prototype, {
    constructor: { writable: true, value: IfNull }
});

function IfTrue(condition)
{
    var thenTask = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var elseTask = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    IfTask.call(this, new True(condition), thenTask, elseTask);
    for (var _len = arguments.length, elseIfTasks = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
        elseIfTasks[_key - 3] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfTrue.prototype = Object.create(IfTask.prototype, {
    constructor: { writable: true, value: IfTrue }
});

function IfUndefined(value)
{
    var thenTask = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var elseTask = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    IfTask.call(this, new Undefined(value), thenTask, elseTask);
    for (var _len = arguments.length, elseIfTasks = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
        elseIfTasks[_key - 3] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfUndefined.prototype = Object.create(IfTask.prototype, {
    constructor: { writable: true, value: IfUndefined }
});

function IfZero(value)
{
    var thenTask            = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var elseTask            = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    IfTask.call(this, new Zero(value), thenTask, elseTask);
    for (var _len = arguments.length, elseIfTasks = Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
        elseIfTasks[_key - 3] = arguments[_key];
    }
    if (elseIfTasks.length > 0) {
        this.addElseIf.apply(this, elseIfTasks);
    }
}
IfZero.prototype = Object.create(IfTask.prototype, {
    constructor: { writable: true, value: IfZero }
});

/**
 * The {@link system.logics} library perform some tasks based on whether a given condition holds <code>true</code> or not.
 * <p>This task is heavily based on the Condition framework that can be found in the {@link system.rules} library.</p>
 * <p>In addition to the {@link system.rules.Rule|Rule} condition, you can specify three different child actions based on the {@link system.process.Action|Action} :  <code>elseif</code>, <code>then</code> and <code>else</code>. All three subelements are optional. Both <code>then</code> and <code>else</code> must not be used more than once inside the if task. Both are containers for tasks, just like {@link system.process.BatchTask|BatchTask} and {@link system.process.Chain|Chain} tasks.</p>
 * @summary The {@link system.logics} library perform some tasks based on whether a given condition holds <code>true</code> or not.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.logics
 * @memberof system
 * @example
 * // -------- Imports
 *
 * var IfTask      = system.logics.IfTask ;
 * var Do          = system.process.Do ;
 * var ElseIf      = system.logics.ElseIf ;
 * var EmptyString = system.rules.EmptyString ;
 * var Equals      = system.rules.Equals ;
 *
 * // -------- init
 *
 * var task ;
 *
 * var do1 = new Do() ;
 * var do2 = new Do() ;
 * var do3 = new Do() ;
 * var do4 = new Do() ;
 *
 * do1.something = function() { trace("do1 ###") } ;
 * do2.something = function() { trace("do2 ###") } ;
 * do3.something = function() { trace("do3 ###") } ;
 * do4.something = function() { trace("do4 ###") } ;
 *
 * // -------- behaviors
 *
 * var error = function( message , action  )
 * {
 *     trace( "error:" + action + " message:" + message ) ;
 * };
 *
 * var finish = function( action )
 * {
 *     trace( "finish: " + action ) ;
 * };
 *
 * var start = function( action )
 * {
 *     trace( "start: " + action ) ;
 * };
 *
 * trace(' -------- test 1');
 *
 * task = new IfTask( new EmptyString('') , do1 , do2 ) ;
 *
 * task.finishIt.connect(finish) ;
 * task.errorIt.connect(error) ;
 * task.startIt.connect(start) ;
 *
 * task.run() ;
 *
 * task.clear() ;
 *
 * trace(' -------- test 2');
 *
 * task.clear() ;
 *
 * task.rule = new Equals(1,2) ;
 *
 * task.addThen( do1 )
 *     .addElse( do2 )
 *     .run() ;
 *
 * trace(' -------- test 3 : <elseIf>');
 *
 * task.clear() ;
 *
 * task.addRule( new Equals(1,2) )
 *     .addThen( do1 )
 *     .addElseIf
 *     (
 *         new ElseIf( new Equals(2,1) , do3 ) ,
 *         new ElseIf( new Equals(2,2) , do4 )
 *     )
 *     .addElse( do2 )
 *     .run() ;
 *
 * trace(' -------- test 4 : <then> is already register');
 *
 * task.clear() ;
 * task.throwError = true ;
 *
 * try
 * {
 *     task.addThen( do1 )
 *         .addElse( do2 )
 *         .addThen( do3 )
 * }
 * catch (e)
 * {
 *     trace( e ) ;
 * }
 *
 * trace(' -------- test 5 : <rule> is not defined');
 *
 * try
 * {
 *     task.run() ;
 * }
 * catch (e)
 * {
 *     trace( e ) ;
 * }
 *
 * trace(' -------- test 6 : <rule> is not defined and throwError = false');
 *
 * task.throwError = false ;
 *
 * task.run() ;
 */
var logics = Object.assign({
  ElseIf: ElseIf,
  ElseIfEmptyString: ElseIfEmptyString,
  ElseIfEquals: ElseIfEquals,
  ElseIfFalse: ElseIfFalse,
  ElseIfNull: ElseIfNull,
  ElseIfTrue: ElseIfTrue,
  ElseIfUndefined: ElseIfUndefined,
  ElseIfZero: ElseIfZero,
  IfEmptyString: IfEmptyString,
  IfEquals: IfEquals,
  IfFalse: IfFalse,
  IfNull: IfNull,
  IfTask: IfTask,
  IfTrue: IfTrue,
  IfUndefined: IfUndefined,
  IfZero: IfZero
});

function Model() {
    Lockable.call(this);
}
Model.prototype = Object.create(Lockable.prototype, {
    constructor: { writable: true, value: Model },
    supports: { writable: true, value: function value(_value) {
            return _value === _value;
        } },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } },
    validate: { writable: true, value: function value(_value2)
        {
            if (!this.supports(_value2)) {
                throw new Error(this + " validate(" + _value2 + ") is mismatch.");
            }
        } }
});

function ChangeModel() {
    Model.call(this);
    Object.defineProperties(this, {
        beforeChanged: { value: new Signal() },
        changed: { value: new Signal() },
        cleared: { value: new Signal() },
        security: { value: true, writable: true },
        _current: { value: null, writable: true }
    });
}
ChangeModel.prototype = Object.create(Model.prototype, {
    constructor: { writable: true, value: ChangeModel },
    current: {
        get: function get() {
            return this._current;
        },
        set: function set(o) {
            if (o === this._current && this.security) {
                return;
            }
            if (o) {
                this.validate(o);
            }
            if (this._current) {
                this.notifyBeforeChange(this._current);
            }
            this._current = o;
            if (this._current) {
                this.notifyChange(this._current);
            }
        }
    },
    clear: { writable: true, value: function value() {
            this._current = null;
            this.notifyClear();
        } },
    notifyBeforeChange: { value: function value(_value) {
            if (!this.isLocked()) {
                this.beforeChanged.emit(_value, this);
            }
        } },
    notifyChange: { value: function value(_value2) {
            if (!this.isLocked()) {
                this.changed.emit(_value2, this);
            }
        } },
    notifyClear: { value: function value() {
            if (!this.isLocked()) {
                this.cleared.emit(this);
            }
        } }
});

function MemoryModel() {
    ChangeModel.call(this);
    Object.defineProperties(this, {
        enableErrorChecking: { writable: true, value: false },
        header: { value: new MemoryEntry(), writable: true },
        _reduced: { value: false, writable: true },
        size: { value: 0, writable: true }
    });
    this.header.next = this.header.previous = this.header;
}
MemoryModel.prototype = Object.create(ChangeModel.prototype, {
    constructor: { writable: true, value: MemoryModel },
    current: {
        get: function get() {
            return this._current;
        },
        set: function set(o) {
            if (o === this._current && this.security) {
                return;
            }
            if (o) {
                this.validate(o);
            }
            if (this._current) {
                this.notifyBeforeChange(this._current);
            }
            this._current = o;
            if (this._current) {
                this.add(this._current);
                this.notifyChange(this._current);
            }
        }
    },
    length: { get: function get() {
            return this.size;
        } },
    reduced: { get: function get() {
            return this._reduced;
        } },
    back: { value: function value() {
            var old = this.last();
            if (old) {
                this._reduced = true;
                this.notifyBeforeChange(old);
                this._reduced = false;
            }
            this.removeLast();
            this._current = this.last();
            if (this._current) {
                this.notifyChange(this._current);
            }
            return old;
        } },
    backTo: { value: function value() {
            var pos = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
            if (pos < 1) {
                pos = 1;
            }
            if (this.size > 1) {
                if (pos < this.size) {
                    this._reduced = true;
                    var old = this.last();
                    if (old) {
                        this.notifyBeforeChange(old);
                    }
                    while (pos !== this.size) {
                        this.removeLast();
                    }
                    this._reduced = false;
                    this._current = this.last();
                    if (this._current) {
                        this.notifyChange(this._current);
                    }
                    return old;
                } else {
                    if (this.enableErrorChecking) {
                        throw new RangeError(this + " backTo failed, the passed-in index '" + pos + "' is out of bounds (" + this.size + ".");
                    } else {
                        return null;
                    }
                }
            } else {
                if (this.enableErrorChecking) {
                    throw new NoSuchElementError(this + " backTo failed, the length of the memory model must be greater than 1 element.");
                } else {
                    return null;
                }
            }
        } },
    clear: { value: function value() {
            if (this.size > 0) {
                var e = this.header.next;
                var next = void 0;
                while (e !== this.header) {
                    next = e.next;
                    e.next = e.previous = null;
                    e.element = null;
                    e = next;
                }
                this.header.next = this.header.previous = this.header;
                this.size = 0;
            }
            ChangeModel.prototype.clear.call(this);
        } },
    first: { value: function value() {
            if (this.size > 0) {
                return this.header.next.element;
            } else {
                if (this.enableErrorChecking) {
                    throw new NoSuchElementError(this + " first method failed, the memory is empty.");
                } else {
                    return null;
                }
            }
        } },
    home: { value: function value() {
            if (this.size > 1) {
                var old = this.header.previous.element;
                if (old) {
                    this.notifyBeforeChange(old);
                }
                var top = this.header.next;
                while (this.header.previous !== top) {
                    this.removeEntry(this.header.previous);
                }
                this._current = this.last();
                if (this._current) {
                    this.notifyChange(this._current);
                }
                return old;
            } else {
                if (this.enableErrorChecking) {
                    throw new NoSuchElementError(this + " home failed, the length of the memory model must be greater than 1 element.");
                } else {
                    return null;
                }
            }
        } },
    isEmpty: { value: function value() {
            return this.size === 0;
        } },
    last: { value: function value() {
            if (this.size > 0) {
                return this.header.previous.element;
            } else {
                if (this.enableErrorChecking) {
                    throw new NoSuchElementError(this + " last method failed, the memory is empty.");
                } else {
                    return null;
                }
            }
        } },
    add: { value: function value(element) {
            this.addBefore(element, this.header);
            return element;
        } },
    addBefore: { value: function value(element, entry) {
            var e = new MemoryEntry(element, entry, entry.previous);
            e.previous.next = e;
            e.next.previous = e;
            this.size++;
            return e;
        } },
    removeEntry: { value: function value(entry) {
            if (entry === this.header) {
                if (this.enableErrorChecking) {
                    throw new NoSuchElementError(this + " removeEntry failed.");
                } else {
                    return null;
                }
            }
            var result = entry.element;
            entry.previous.next = entry.next;
            entry.next.previous = entry.previous;
            entry.next = entry.previous = null;
            entry.element = null;
            this.size--;
            return result;
        } },
    removeLast: { value: function value() {
            return this.removeEntry(this.header.previous);
        } }
});
function MemoryEntry() {
    var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var next                 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var previous                 = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    this.element = element;
    this.next = next;
    this.previous = previous;
}

function ArrayModel() {
    var factory = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    ChangeModel.call(this);
    Object.defineProperties(this, {
        added: { value: new Signal() },
        removed: { value: new Signal() },
        updated: { value: new Signal() },
        _array: { writable: true, value: factory instanceof Array ? factory : [] }
    });
}
ArrayModel.prototype = Object.create(ChangeModel.prototype, {
    constructor: { writable: true, value: ArrayModel },
    length: { get: function get() {
            return this._array.length;
        } },
    add: { value: function value(entry) {
            if (entry === null || entry === undefined) {
                throw new ReferenceError(this + " add method failed, the passed-in argument not must be 'null'.");
            }
            this.validate(entry);
            this._array.push(entry);
            this.notifyAdd(this._array.length - 1, entry);
        } },
    addAt: { value: function value(index, entry) {
            if (entry === null || entry === undefined) {
                throw new ReferenceError(this + " add method failed, the passed-in argument not must be 'null'.");
            }
            this.validate(entry);
            this._array.splice(index, 0, entry);
            this.notifyAdd(index, entry);
        } },
    clear: { value: function value() {
            this._array.length = 0;
            ChangeModel.prototype.clear.call(this);
        } },
    get: { value: function value(index) {
            return this._array[index];
        } },
    has: { value: function value(entry) {
            return this._array.indexOf(entry) > -1;
        } },
    isEmpty: { value: function value() {
            return this._array.length === 0;
        } },
    notifyAdd: { value: function value(index, entry) {
            if (!this.isLocked()) {
                this.added.emit(index, entry, this);
            }
        } },
    notifyRemove: { value: function value(index, entry) {
            if (!this.isLocked()) {
                this.removed.emit(index, entry, this);
            }
        } },
    notifyUpdate: { value: function value(index, entry) {
            if (!this.isLocked()) {
                this.updated.emit(index, entry, this);
            }
        } },
    remove: { value: function value(entry) {
            if (entry === null || entry === undefined) {
                throw new ReferenceError(this + " remove method failed, the entry passed in argument not must be null.");
            }
            var index = this._array.indexOf(entry);
            if (index > -1) {
                this.removeAt(index);
            } else {
                throw new ReferenceError(this + " remove method failed, the entry is not register in the model.");
            }
        } },
    removeAt: { value: function value(index) {
            var count = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
            count = count > 1 ? count : 1;
            var old = this._array.splice(index, count);
            if (old) {
                this.notifyRemove(index, old);
            }
        } },
    removeRange: { value: function value(fromIndex, toIndex) {
            if (fromIndex === toIndex) {
                return null;
            }
            return this.removeAt(fromIndex, toIndex - fromIndex);
        } },
    setArray: { value: function value(ar) {
            this._array = ar instanceof Array ? ar : [];
        } },
    updateAt: { value: function value(index, entry) {
            this.validate(entry);
            var old = this._array[index];
            if (old) {
                this._array[index] = entry;
                this.notifyUpdate(index, old);
            }
        } },
    toArray: { value: function value() {
            return this._array;
        } }
});

function MapModel() {
    var factory = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "id";
    ChangeModel.call(this);
    Object.defineProperties(this, {
        added: { value: new Signal() },
        removed: { value: new Signal() },
        updated: { value: new Signal() },
        _map: { writable: true, value: factory instanceof KeyValuePair ? factory : new ArrayMap() },
        _primaryKey: {
            value: !(key instanceof String || typeof key === 'string') || key === "" ? MapModel.DEFAULT_PRIMARY_KEY : key,
            writable: true
        }
    });
}
Object.defineProperty(MapModel, 'DEFAULT_PRIMARY_KEY', { value: "id" });
MapModel.prototype = Object.create(ChangeModel.prototype, {
    constructor: { writable: true, value: MapModel },
    length: { get: function get() {
            return this._map.length;
        } },
    primaryKey: {
        get: function get() {
            return this._primaryKey;
        },
        set: function set(key) {
            if (key === this._primaryKey) {
                return;
            }
            this._primaryKey = !(key instanceof String || typeof key === 'string') || key === "" ? MapModel.DEFAULT_PRIMARY_KEY : key;
            if (this._map.length > 0) {
                this._map.clear();
            }
        }
    },
    add: { value: function value(entry) {
            if (entry === null || entry === undefined) {
                throw new ReferenceError(this + " add method failed, the passed-in argument not must be 'null'.");
            }
            this.validate(entry);
            if (this._primaryKey in entry) {
                if (!this._map.has(entry[this._primaryKey])) {
                    this._map.set(entry[this._primaryKey], entry);
                    this.notifyAdd(entry);
                } else {
                    throw new ReferenceError(this + " add method failed, the passed-in entry is already register in the model with the specified primary key, you must remove this entry before add a new entry.");
                }
            } else {
                throw new ReferenceError(this + " add method failed, the entry is not identifiable and don't contains a primary key with the name '" + this._primaryKey + "'.");
            }
        } },
    clear: { value: function value() {
            this._map.clear();
            ChangeModel.prototype.clear.call(this);
        } },
    get: { value: function value(key) {
            return this._map.get(key);
        } },
    getByProperty: { value: function value(propName, _value) {
            if (propName === null || !(propName instanceof String || typeof propName === 'string')) {
                return null;
            }
            var datas = this._map.values();
            var size = datas.length;
            try {
                if (size > 0) {
                    while (--size > -1) {
                        if (datas[size][propName] === _value) {
                            return datas[size];
                        }
                    }
                }
            } catch (er) {
            }
            return null;
        } },
    has: { value: function value(entry) {
            return this._map.hasValue(entry);
        } },
    hasByProperty: { value: function value(propName, _value2) {
            if (propName === null || !(propName instanceof String || typeof propName === 'string')) {
                return false;
            }
            var datas = this._map.values();
            var size = datas.length;
            if (size > 0) {
                while (--size > -1) {
                    if (datas[size][propName] === _value2) {
                        return true;
                    }
                }
            }
            return false;
        } },
    hasKey: { value: function value(key) {
            return this._map.has(key);
        } },
    isEmpty: { value: function value() {
            return this._map.isEmpty();
        } },
    iterator: { value: function value() {
            return this._map.iterator();
        } },
    keyIterator: { value: function value() {
            return this._map.keyIterator();
        } },
    notifyAdd: { value: function value(entry) {
            if (!this.isLocked()) {
                this.added.emit(entry, this);
            }
        } },
    notifyRemove: { value: function value(entry) {
            if (!this.isLocked()) {
                this.removed.emit(entry, this);
            }
        } },
    notifyUpdate: { value: function value(entry) {
            if (!this.isLocked()) {
                this.updated.emit(entry, this);
            }
        } },
    remove: { value: function value(entry) {
            if (entry === null || entry === undefined) {
                throw new ReferenceError(this + " remove method failed, the entry passed in argument not must be null.");
            }
            if (this._primaryKey in entry) {
                if (this._map.has(entry[this._primaryKey])) {
                    this._map.delete(entry[this._primaryKey]);
                    this.notifyRemove(entry);
                } else {
                    throw new ReferenceError(this + " remove method failed, no entry register in the model with the specified primary key.");
                }
            } else {
                throw new ReferenceError(this + " remove method failed, the entry is not identifiable and don't contains a primary key with the name '" + this._primaryKey + "'.");
            }
        } },
    setMap: { value: function value(map) {
            this._map = map instanceof KeyValuePair ? map : new ArrayMap();
        } },
    update: { value: function value(entry) {
            if (this._primaryKey in entry) {
                if (this._map.has(entry[this._primaryKey])) {
                    this._map.set(entry[this._primaryKey], entry);
                    this.notifyUpdate(entry);
                } else {
                    throw new ReferenceError(this + " update method failed, no entry register in the model with the specified primary key.");
                }
            } else {
                throw new ReferenceError(this + " update method failed, the entry is not identifiable and don't contains a primary key with the name '" + this._primaryKey + "'.");
            }
        } },
    toMap: { value: function value() {
            return this._map;
        } }
});

function InitMapModel() {
    var model = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var datas = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var autoClear = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var autoSelect = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    var autoDequeue = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
    var cleanFirst = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
    Action.call(this);
    Object.defineProperties(this, {
        autoClear: { value: autoClear === true, writable: true },
        autoDequeue: { value: autoDequeue === true, writable: true },
        autoSelect: { value: autoSelect === true, writable: true },
        cleanFirst: { value: cleanFirst === true, writable: true },
        datas: { value: datas instanceof Array ? datas : null, writable: true },
        first: { value: null, writable: true },
        model: { value: model instanceof MapModel ? model : null, writable: true }
    });
}
InitMapModel.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: InitMapModel },
    clone: { writable: true, value: function value() {
            return new InitMapModel(this.models, this.datas, this.autoClear, this.autoSelect, this.autoDequeue, this.cleanFirst);
        } },
    filterEntry: { writable: true, value: function value(_value) {
            return _value;
        } },
    reset: { writable: true, value: function value() {
            this.datas = null;
        } },
    run: { writable: true, value: function value() {
            this.notifyStarted();
            if (!(this.model instanceof MapModel)) {
                this.notifyFinished();
                return;
            }
            if (this.autoClear === true && !this.model.isEmpty()) {
                this.model.clear();
            }
            if (arguments.length > 0) {
                this.datas = (arguments.length <= 0 ? undefined : arguments[0]) instanceof Array ? arguments.length <= 0 ? undefined : arguments[0] : null;
            }
            if (!(this.datas instanceof Array) || this.datas.length === 0) {
                this.notifyFinished();
                return;
            }
            var entry = void 0;
            var size = this.datas.length;
            for (var i = 0; i < size; i++) {
                entry = this.filterEntry(this.datas[i]);
                this.model.add(entry);
                if (this.first === null && entry !== null) {
                    this.first = entry;
                }
            }
            if (this.datas && this.datas instanceof Array && this.autoDequeue === true) {
                this.datas.length = 0;
            }
            if (this.first !== null && this.autoSelect === true) {
                if (this.model.has(this.first)) {
                    this.model.current = this.model.get(this.first);
                } else {
                    this.model.current = this.first;
                }
                if (this.cleanFirst === true) {
                    this.first = null;
                }
            }
            this.notifyFinished();
        } }
});

/**
 * The {@link system.models} library provides a simple <b>MVC</b> implementation with a collection of <code>Model</code> classes to manage your applications.
 * @summary The {@link system.models} library provides a simple <b>MVC</b> implementation with a collection of <code>Model</code> classes to manage your applications.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.models
 * @memberof system
 * @example
 * var beforeChanged = function( value , model )
 * {
 *     trace( "before:" + value + " current:" + model.current ) ;
 * }
 *
 * var changed = function( value , model )
 * {
 *     trace( "change:" + value + " current:" + model.current ) ;
 * }
 *
 * var cleared = function( model )
 * {
 *     trace( "clear current:" + model.current ) ;
 * }
 *
 * var model = new ChangeModel() ;
 *
 * model.beforeChanged.connect( beforeChanged ) ;
 * model.changed.connect( changed ) ;
 * model.cleared.connect( cleared ) ;
 *
 * model.current = "hello" ;
 * model.current = "world" ;
 * model.current = null ;
 */
var models = Object.assign({
  ChangeModel: ChangeModel,
  MemoryModel: MemoryModel,
  Model: Model,
  arrays: Object.assign({
    ArrayModel: ArrayModel
  }),
  maps: Object.assign({
    InitMapModel: InitMapModel,
    MapModel: MapModel
  })
});

function PRNG() {
    var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
    Object.defineProperties(this, {
        _value: { value: 1, writable: true }
    });
    this.value = value > 0 ? value : Math.random() * 0X7FFFFFFE;
}
PRNG.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: PRNG },
    value: {
        get: function get() {
            return this._value;
        },
        set: function set(value) {
            value = value > 1 ? value : 1;
            value = value > 0X7FFFFFFE ? 0X7FFFFFFE : value;
            this._value = value;
        }
    },
    randomInt: { value: function value() {
            this._value = this._value * 16807 % 2147483647;
            return this._value;
        } },
    randomIntByMinMax: { value: function value() {
            var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
            if (isNaN(min)) {
                min = 0;
            }
            if (isNaN(max)) {
                max = 1;
            }
            min -= 0.4999;
            max += 0.4999;
            this._value = this._value * 16807 % 2147483647;
            return Math.round(min + (max - min) * this._value / 2147483647);
        } },
    randomIntByRange: { value: function value(r) {
            var min = r.min - 0.4999;
            var max = r.max + 0.4999;
            this._value = this._value * 16807 % 2147483647;
            return Math.round(min + (max - min) * this._value / 2147483647);
        } },
    randomNumber: { value: function value()
        {
            this._value = this._value * 16807 % 2147483647;
            return this._value / 2147483647;
        } },
    randomNumberByMinMax: { value: function value(min, max) {
            if (isNaN(min)) {
                min = 0;
            }
            if (isNaN(max)) {
                max = 1;
            }
            this._value = this._value * 16807 % 2147483647;
            return min + (max - min) * this._value / 2147483647;
        } },
    randomNumberByRange: { value: function value(r          )
        {
            this._value = this._value * 16807 % 2147483647;
            return r.min + (r.max - r.min) * this._value / 2147483647;
        } },
    toString: { value: function value() {
            return String(this._value);
        } },
    valueOf: { value: function value() {
            return this._value;
        } }
});

function Range() {
    var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
    var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NaN;
    var writable = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
    if (max < min) {
        throw new RangeError("The Range constructor failed, the 'max' argument is < of 'min' argument");
    }
    Object.defineProperties(this, {
        max: { writable: writable, value: isNaN(max) ? NaN : max },
        min: { writable: writable, value: isNaN(min) ? NaN : min }
    });
}
Range.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Range },
    clamp: { value: function value(_value) {
            if (isNaN(_value)) {
                return NaN;
            }
            var mi = this.min;
            var ma = this.max;
            if (isNaN(mi)) {
                mi = _value;
            }
            if (isNaN(ma)) {
                ma = _value;
            }
            return Math.max(Math.min(_value, ma), mi);
        } },
    clone: { writable: true, value: function value() {
            return new Range(this.min, this.max);
        } },
    combine: { value: function value() {
            var range = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            if (!(range instanceof Range)) {
                return this.clone();
            } else {
                return new Range(Math.min(this.min, range.min), Math.max(this.max, range.max));
            }
        } },
    contains: { value: function value(_value2) {
            return !(_value2 > this.max || _value2 < this.min);
        } },
    equals: { writable: true, value: function value(o) {
            if (o instanceof Range) {
                return o.min === this.min && o.max === this.max;
            } else {
                return false;
            }
        } },
    expand: { value: function value() {
            var lowerMargin = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            var upperMargin = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
            if (isNaN(lowerMargin)) {
                lowerMargin = 1;
            }
            if (isNaN(upperMargin)) {
                upperMargin = 1;
            }
            var delta = this.max - this.min;
            return new Range(this.min - delta * lowerMargin, this.max + delta * upperMargin);
        } },
    getCentralValue: { value: function value() {
            return (this.min + this.max) / 2;
        } },
    getRandomFloat: { value: function value() {
            return Math.random() * (this.max - this.min) + this.min;
        } },
    getRandomInteger: { value: function value() {
            return Math.floor(Math.random() * (this.max - this.min) + this.min);
        } },
    isOutOfRange: { value: function value(_value3) {
            return _value3 > this.max || _value3 < this.min;
        } },
    overlap: { value: function value(range) {
            return this.max >= range.min && range.max >= this.min;
        } },
    size: { value: function value() {
            return this.max - this.min;
        } },
    toString: { writable: true, value: function value() {
            return "[Range min:" + this.min + " max:" + this.max + "]";
        } }
});
Object.defineProperties(Range, {
    COLOR: { value: new Range(-255, 255, false), enumerable: true },
    DEGREE: { value: new Range(0, 360, false), enumerable: true },
    PERCENT: { value: new Range(0, 100, false), enumerable: true },
    RADIAN: { value: new Range(0, Math.PI * 2, false), enumerable: true },
    UNITY: { value: new Range(0, 1, false), enumerable: true }
});

/**
 * The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
 * @summary The {@link system.numeric} library contains classes and tools that provides extra <code>numeric</code> methods and implementations.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.numeric
 * @memberof system
 */
var numeric = Object.assign({
  PRNG: PRNG,
  Range: Range,
  RomanNumber: RomanNumber
});

function ActionEntry(action) {
  var priority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var auto = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  Object.defineProperties(this, {
    action: { writable: true, value: action },
    auto: { writable: true, value: auto === true },
    priority: { writable: true, value: priority > 0 ? Math.ceil(priority) : 0 }
  });
}
ActionEntry.prototype = Object.create(Object.prototype, {
  constructor: { value: ActionEntry },
  toString: { value: function value() {
      return "[ActionEntry action:" + this.action + " priority:" + this.priority + " auto:" + this.auto + "]";
    } }
});

function Apply() {
    var func = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var scope = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    Action.call(this);
    Object.defineProperties(this, {
        args: { value: args instanceof Array ? args : null, writable: true },
        func: { value: func instanceof Function ? func : null, writable: true },
        scope: { value: scope, writable: true }
    });
}
Apply.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: Apply },
    clone: { writable: true, value: function value() {
            return new Apply(this.func, this.args, this.scope);
        } },
    run: { writable: true, value: function value() {
            this.notifyStarted();
            if (this.func instanceof Function) {
                if (this.args && this.args.length > 0) {
                    this.func.apply(this.scope, this.args);
                } else {
                    this.func.apply(this.scope);
                }
            } else {
                throw new TypeError('[Apply] run failed, the \'func\' property must be a Function.');
            }
            this.notifyFinished();
        } }
});

function Batch() {
    var _this = this;
    var init = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    Object.defineProperties(this, {
        _entries: {
            value: [],
            enumerable: false,
            writable: true,
            configurable: false
        }
    });
    if (init && init instanceof Array && init.length > 0) {
        init.forEach(function (element) {
            if (element instanceof Runnable) {
                _this.add(element);
            }
        });
    }
}
Batch.prototype = Object.create(Runnable.prototype, {
    constructor: { writable: true, value: Batch },
    length: {
        get: function get() {
            return this._entries.length;
        }
    },
    add: { writable: true, value: function value(command) {
            if (command && command instanceof Runnable) {
                this._entries.push(command);
                return true;
            }
            return false;
        } },
    clear: { writable: true, value: function value() {
            this._entries.length = 0;
        } },
    clone: { writable: true, value: function value() {
            var b = new Batch();
            var l = this._entries.length;
            for (var i = 0; i < l; i++) {
                b.add(this._entries[i]);
            }
            return b;
        } },
    contains: { writable: true, value: function value(command) {
            if (command instanceof Runnable) {
                var l = this._entries.length;
                while (--l > -1) {
                    if (this._entries[l] === command) {
                        return true;
                    }
                }
            }
            return false;
        } },
    get: { writable: true, value: function value(key) {
            return this._entries[key];
        } },
    indexOf: { writable: true, value: function value(command, fromIndex         ) {
            if (isNaN(fromIndex)) {
                fromIndex = 0;
            }
            fromIndex = fromIndex > 0 ? Math.round(fromIndex) : 0;
            if (command instanceof Runnable) {
                var l = this._entries.length;
                var i = fromIndex;
                for (i; i < l; i++) {
                    if (this._entries[i] === command) {
                        return i;
                    }
                }
            }
            return -1;
        } },
    isEmpty: { writable: true, value: function value() {
            return this._entries.length === 0;
        } },
    remove: { writable: true, value: function value(command) {
            var index = this.indexOf(command);
            if (index > -1) {
                this._entries.splice(index, 1);
                return true;
            }
            return false;
        } },
    run: { writable: true, value: function value() {
            var l = this._entries.length;
            if (l > 0) {
                var i = -1;
                while (++i < l) {
                    this._entries[i].run();
                }
            }
        } },
    stop: { writable: true, value: function value() {
            var l = this._entries.length;
            if (l > 0) {
                this._entries.forEach(function (element) {
                    if (element instanceof Runnable && 'stop' in element && element.stop instanceof Function) {
                        element.stop();
                    }
                });
            }
        } },
    toArray: { writable: true, value: function value() {
            return this._entries.slice();
        } },
    toString: { writable: true, value: function value() {
            var r = "[Batch";
            var l = this._entries.length;
            if (l > 0) {
                r += '[';
                this._entries.forEach(function (element, index) {
                    r += element;
                    if (index < l - 1) {
                        r += ",";
                    }
                });
                r += ']';
            }
            r += "]";
            return r;
        } }
});

function TaskGroup() {
    var _this = this;
    var mode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'normal';
    var actions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    Task.call(this);
    Object.defineProperties(this, {
        verbose: { value: false, writable: true },
        _actions: { value: [], writable: true },
        _next: { value: null, writable: true, configurable: true },
        _stopped: { value: false, writable: true },
        _mode: { value: TaskGroup.NORMAL, writable: true }
    });
    if (typeof mode === "string" || mode instanceof String) {
        this.mode = mode;
    }
    if (actions && actions instanceof Array && actions.length > 0) {
        actions.forEach(function (action) {
            if (action instanceof Action) {
                _this.add(action);
            }
        });
    }
}
Object.defineProperties(TaskGroup, {
    EVERLASTING: { value: 'everlasting', enumerable: true },
    NORMAL: { value: 'normal', enumerable: true },
    TRANSIENT: { value: 'transient', enumerable: true }
});
TaskGroup.prototype = Object.create(Task.prototype, {
    constructor: { writable: true, value: TaskGroup },
    length: {
        get: function get() {
            return this._actions.length;
        },
        set: function set(value) {
            if (this._running) {
                throw new Error(this + " length property can't be changed, the batch process is in progress.");
            }
            this.dispose();
            var old          = this._actions.length;
            this._actions.length = value;
            var l = this._actions.length;
            if (l > 0) {
                while (--l > -1) {
                    var entry = this._actions[l];
                    if (entry && entry.action && this._next) {
                        entry.action.finishIt.connect(this._next);
                    }
                }
            } else if (old > 0) {
                this.notifyCleared();
            }
        }
    },
    mode: {
        get: function get() {
            return this._mode;
        },
        set: function set(value) {
            this._mode = value === TaskGroup.TRANSIENT || value === TaskGroup.EVERLASTING ? value : TaskGroup.NORMAL;
        }
    },
    stopped: { get: function get() {
            return this._stopped;
        } },
    add: { value: function value(action) {
            var priority = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
            var autoRemove = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
            if (this._running) {
                throw new Error(this + " add failed, the process is in progress.");
            }
            if (action && action instanceof Action) {
                autoRemove = autoRemove === true;
                priority = priority > 0 ? Math.round(priority) : 0;
                if (this._next) {
                    action.finishIt.connect(this._next);
                }
                this._actions.push(new ActionEntry(action, priority, autoRemove));
                var i = void 0;
                var j = void 0;
                var a = this._actions;
                var swap = function swap(j, k) {
                    var temp = a[j];
                    a[j] = a[k];
                    a[k] = temp;
                    return true;
                };
                var swapped = false;
                var l = a.length;
                for (i = 1; i < l; i++) {
                    for (j = 0; j < l - i; j++) {
                        if (a[j + 1].priority > a[j].priority) {
                            swapped = swap(j, j + 1);
                        }
                    }
                    if (!swapped) {
                        break;
                    }
                }
                return true;
            }
            return false;
        } },
    clone: { writable: true, value: function value() {
            return new TaskGroup(this._mode, this._actions.length > 0 ? this._actions : null);
        } },
    contains: { writable: true, value: function value(action) {
            if (action && action instanceof Action) {
                if (this._actions.length > 0) {
                    var e;
                    var l = this._actions.length;
                    while (--l > -1) {
                        e = this._actions[l];
                        if (e && e.action === action) {
                            return true;
                        }
                    }
                }
            }
            return false;
        } },
    dispose: { writable: true, value: function value() {
            var _this2 = this;
            if (this._actions.length > 0) {
                this._actions.forEach(function (entry) {
                    if (entry instanceof ActionEntry) {
                        entry.action.finishIt.disconnect(_this2._next);
                    }
                });
            }
        } },
    get: { writable: true, value: function value(index) {
            if (this._actions.length > 0 && index < this._actions.length) {
                var entry = this._actions[index];
                if (entry) {
                    return entry.action;
                }
            }
            return null;
        } },
    isEmpty: { writable: true, value: function value() {
            return this._actions.length === 0;
        } },
    next: { writable: true, value: function value(action           ) {
        } },
    remove: { writable: true, value: function value(action) {
            var _this3 = this;
            if (this._running) {
                throw new Error(this + " remove failed, the process is in progress.");
            }
            this.stop();
            if (this._actions.length > 0) {
                if (action && action instanceof Action) {
                    var e = void 0;
                    var l = this._actions.length;
                    this._actions.forEach(function (element) {
                        if (element && element instanceof ActionEntry && element.action === action) {
                            if (_this3._next) {
                                e.action.finishIt.disconnect(_this3._next);
                            }
                            _this3._actions.splice(l, 1);
                            return true;
                        }
                    });
                } else {
                    this.dispose();
                    this._actions.length = 0;
                    this.notifyCleared();
                    return true;
                }
            }
            return false;
        } },
    toArray: { writable: true, value: function value() {
            if (this._actions.length > 0) {
                var output = [];
                if (this._actions.length > 0) {
                    this._actions.forEach(function (element) {
                        if (element && element instanceof ActionEntry && element.action) {
                            output.push(element.action);
                        }
                    });
                }
                return output;
            } else {
                return [];
            }
        } },
    toString: { writable: true, value: function value() {
            var s = "[" + this.constructor.name;
            if (this.verbose === true) {
                if (this._actions.length > 0) {
                    s += "[";
                    var i = void 0;
                    var e = void 0;
                    var l = this._actions.length;
                    var r = [];
                    for (i = 0; i < l; i++) {
                        e = this._actions[i];
                        r.push(e && e.action ? e.action : null);
                    }
                    s += r.toString();
                    s += "]";
                }
            }
            s += "]";
            return s;
        } }
});

function BatchTaskNext(batch) {
    this.batch = batch;
}
BatchTaskNext.prototype = Object.create(Receiver.prototype, {
    constructor: { value: BatchTaskNext },
    receive: { value: function value(action) {
            var batch = this.batch;
            var mode = batch.mode;
            var actions = batch._actions;
            var currents = batch._currents;
            if (action && currents.has(action)) {
                var entry = currents.get(action);
                if (mode !== TaskGroup.EVERLASTING) {
                    if (mode === TaskGroup.TRANSIENT || entry.auto && mode === TaskGroup.NORMAL) {
                        var e;
                        var l = actions.length;
                        while (--l > -1) {
                            e = actions[l];
                            if (e && e.action === action) {
                                action.finishIt.disconnect(this);
                                actions.splice(l, 1);
                                break;
                            }
                        }
                    }
                }
                currents.delete(action);
            }
            if (batch._current !== null) {
                batch.notifyChanged();
            }
            batch._current = action;
            batch.notifyProgress();
            if (currents.length === 0) {
                batch._current = null;
                batch.notifyFinished();
            }
        } }
});

function BatchTask() {
    var mode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'normal';
    var actions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    TaskGroup.call(this, mode, actions);
    Object.defineProperties(this, {
        _current: { value: null, writable: true },
        _currents: { value: new ArrayMap(), writable: true },
        _next: { value: new BatchTaskNext(this) }
    });
}
BatchTask.prototype = Object.create(TaskGroup.prototype, {
    current: { get: function get() {
            return this._current;
        } }
});
BatchTask.prototype.constructor = BatchTask;
BatchTask.prototype.clone = function () {
    return new BatchTask(this._mode, this._actions.length > 0 ? this._actions : null);
};
BatchTask.prototype.resume = function ()
{
    if (this._stopped) {
        this._running = true;
        this._stopped = false;
        this.notifyResumed();
        if (this._actions.length > 0) {
            var a;
            var e;
            var l = this._actions.length;
            while (--l > -1) {
                e = this._actions[l];
                if (e) {
                    a = e.action;
                    if (a) {
                        if ("resume" in a) {
                            a.resume();
                        } else {
                            this.next(a);
                        }
                    }
                }
            }
        }
    } else {
        this.run();
    }
};
BatchTask.prototype.run = function ()
{
    var _this = this;
    if (!this._running) {
        this.notifyStarted();
        this._currents.clear();
        this._stopped = false;
        this._current = null;
        if (this._actions.length > 0) {
            var actions = [];
            this._actions.forEach(function (entry) {
                if (entry && entry.action) {
                    actions.push(entry.action);
                    _this._currents.set(entry.action, entry);
                }
            });
            actions.forEach(function (action) {
                action.run();
            });
        } else {
            this.notifyFinished();
        }
    }
};
BatchTask.prototype.stop = function ()
{
    if (this._running) {
        if (this._actions.length > 0) {
            var a = void 0;
            var e = void 0;
            var l = this._actions.length;
            while (--l > -1) {
                e = this._actions[l];
                if (e) {
                    a = e.action;
                    if (a) {
                        if ("stop" in a) {
                            a.stop();
                        }
                    }
                }
            }
        }
        this._running = false;
        this._stopped = true;
        this.notifyStopped();
    }
};

function Cache() {
    var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var init = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    Action.call(this);
    Object.defineProperties(this, {
        target: { value: target, writable: true },
        _queue: { value: [], writable: true }
    });
    if (init && init instanceof Array && init.length > 0) {
        init.forEach(function (prop) {
            if (prop instanceof Property) {
                this._queue.push(prop);
            }
        });
    }
}
Cache.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: Cache },
    length: {
        get: function get() {
            return this._queue.length;
        }
    }
});
Cache.prototype.add = function (property) {
    if (property instanceof Property) {
        this._queue.push(property);
    }
    return this;
};
Cache.prototype.addAttribute = function (name, value)
{
    if (name !== '' && (typeof name === 'string' || name instanceof String)) {
        this._queue.push(new Attribute(name, value));
    }
    return this;
};
Cache.prototype.addMethod = function (name)
{
    if (name !== '' && (typeof name === 'string' || name instanceof String)) {
        for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            args[_key - 1] = arguments[_key];
        }
        this._queue.push(new Method(name, args));
    }
    return this;
};
Cache.prototype.addMethodWithArguments = function (name, args) {
    if (name !== '' && (typeof name === 'string' || name instanceof String)) {
        this._queue.push(new Method(name, args));
    }
    return this;
};
Cache.prototype.clear = function () {
    this._queue.length = 0;
};
Cache.prototype.clone = function () {
    return new Cache(this.target, this._queue);
};
Cache.prototype.isEmpty = function () {
    return this._queue.length === 0;
};
Cache.prototype.run = function () {
    this.notifyStarted();
    if (this.target) {
        var l = this._queue.length;
        if (l > 0) {
            var item;
            var name;
            for (var i = 0; i < l; i++) {
                item = this._queue.shift();
                if (item instanceof Method) {
                    name = item.name;
                    if (name && name in this.target) {
                        if (this.target[name] instanceof Function) {
                            this.target[name].apply(this.target, item.args);
                        }
                    }
                } else if (item instanceof Attribute) {
                    name = item.name;
                    if (name in this.target) {
                        this.target[name] = item.value;
                    }
                }
            }
        }
    }
    this.notifyFinished();
};

function Call() {
    var func = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var scope = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    Action.call(this);
    Object.defineProperties(this, {
        func: { value: func instanceof Function ? func : null, writable: true },
        scope: { value: scope, writable: true }
    });
}
Call.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: Call },
    clone: { writable: true, value: function value() {
            return new Call(this.func, this.scope);
        } },
    run: { writable: true, value: function value() {
            this.notifyStarted();
            if (this.func instanceof Function) {
                for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                    args[_key] = arguments[_key];
                }
                if (args && args.length > 0) {
                    this.func.apply(this.scope, args);
                } else {
                    this.func.call(this.scope);
                }
            } else {
                throw new TypeError('[Call] run failed, the \'func\' property must be a Function.');
            }
            this.notifyFinished();
        } }
});

function ChainNext() {
    var chain = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    this.chain = chain;
}
ChainNext.prototype = Object.create(Receiver.prototype, {
    constructor: { value: ChainNext },
    receive: { value: function value() {
            if (this.chain === null) {
                return;
            }
            var chain = this.chain;
            var mode = chain._mode;
            if (chain._current) {
                if (mode !== TaskGroup.EVERLASTING) {
                    if (mode === TaskGroup.TRANSIENT || mode === TaskGroup.NORMAL && chain._current.auto) {
                        chain._current.action.finishIt.disconnect(this);
                        chain._position--;
                        chain._actions.splice(this._position, 1);
                    }
                }
                chain.notifyChanged();
                chain._current = null;
            }
            if (chain._actions.length > 0) {
                if (chain.hasNext()) {
                    chain._current = chain._actions[chain._position++];
                    chain.notifyProgress();
                    if (chain._current && chain._current.action) {
                        chain._current.action.run();
                    } else {
                        this.receive();
                    }
                } else if (this.looping) {
                    chain._position = 0;
                    if (chain.numLoop === 0) {
                        chain.notifyLooped();
                        chain._currentLoop = 0;
                        this.receive();
                    } else if (chain._currentLoop < chain.numLoop) {
                        chain._currentLoop++;
                        chain.notifyLooped();
                        this.receive();
                    } else {
                        chain._currentLoop = 0;
                        chain.notifyFinished();
                    }
                } else {
                    chain._currentLoop = 0;
                    chain._position = 0;
                    chain.notifyFinished();
                }
            } else {
                chain.notifyFinished();
            }
        } }
});

function Chain() {
    var looping = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    var numLoop = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var mode = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'normal';
    var actions = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    TaskGroup.call(this, mode, actions);
    Object.defineProperties(this, {
        looping: { value: Boolean(looping), writable: true },
        numLoop: {
            value: numLoop > 0 ? Math.round(numLoop) : 0,
            writable: true
        },
        _current: { value: null, writable: true },
        _currentLoop: { value: 0, writable: true },
        _position: { value: 0, writable: true },
        _next: { value: new ChainNext(this) }
    });
}
Chain.prototype = Object.create(TaskGroup.prototype, {
    constructor: { writable: true, value: Chain },
    current: { get: function get() {
            return this._current ? this._current.action : null;
        } },
    currentLoop: { get: function get() {
            return this._currentLoop;
        } },
    position: { get: function get() {
            return this._position;
        } },
    clone: { writable: true, value: function value() {
            return new Chain(this.looping, this.numLoop, this._mode, this._actions.length > 0 ? this._actions : null);
        } },
    element: { writable: true, value: function value() {
            return this.hasNext() ? this._actions[this._position].action : null;
        } },
    hasNext: { writable: true, value: function value() {
            return this._position < this._actions.length;
        } },
    resume: { writable: true, value: function value() {
            if (this._stopped) {
                this._running = true;
                this._stopped = false;
                this.notifyResumed();
                if (this._current && this._current.action) {
                    if ("resume" in this._current.action) {
                        this._current.action.resume();
                    }
                } else {
                    this._next.receive();
                }
            } else {
                this.run();
            }
        } },
    run: { writable: true, value: function value() {
            if (!this._running) {
                this.notifyStarted();
                this._current = null;
                this._stopped = false;
                this._position = 0;
                this._currentLoop = 0;
                this._next.receive();
            }
        } },
    stop: { writable: true, value: function value() {
            if (this._running) {
                if (this._current && this._current.action) {
                    if ('stop' in this._current.action && this._current.action instanceof Function) {
                        this._current.action.stop();
                        this._running = false;
                        this._stopped = true;
                        this.notifyStopped();
                    }
                }
            }
        } }
});

function Do() {
    Action.call(this);
}
Do.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: Do },
    clone: { writable: true, value: function value() {
            return new Do();
        } },
    something: { enumerable: true, writable: true, value: function value() {
        } },
    run: { writable: true, value: function value() {
            this.notifyStarted();
            if ('something' in this && this.something instanceof Function) {
                this.something();
            }
            this.notifyFinished();
        } }
});

let performance$1;
try
{
    if( window && window.performance )
    {
        performance$1 = window.performance ;
        performance$1.now = performance$1.now
            || performance$1.mozNow
            || performance$1.msNow
            || performance$1.oNow
            || performance$1.webkitNow ;
    }
}
catch( error )
{
    performance$1 = {} ;
}
if( !("now" in performance$1) )
{
    const startTime = Date.now();
    performance$1.now = () => Date.now() - startTime ;
}

let ca;
let ra;
if( window )
{
    const ONE_FRAME_TIME = 16;
    let lastTime = Date.now();
    const vendors = ['ms', 'moz', 'webkit', 'o'];
    let len = vendors.length;
    for ( let x = 0 ; x < len && !window.requestAnimationFrame ; ++x )
    {
        const p = vendors[x];
        window.requestAnimationFrame = window[`${p}RequestAnimationFrame`];
        window.cancelAnimationFrame  = window[`${p}CancelAnimationFrame`] || global[`${p}CancelRequestAnimationFrame`];
    }
    if (!window.requestAnimationFrame)
    {
        window.requestAnimationFrame = ( callback ) =>
        {
            if ( typeof callback !== 'function' )
            {
                throw new TypeError(`${callback}is not a function`) ;
            }
            const currentTime = Date.now();
            let delay = ONE_FRAME_TIME + lastTime - currentTime;
            if ( delay < 0 )
            {
                delay = 0;
            }
            lastTime = currentTime;
            return setTimeout(() =>
            {
                lastTime = Date.now();
                callback( performance$1.now() );
            }, delay);
        };
    }
    if (!window.cancelAnimationFrame)
    {
        window.cancelAnimationFrame = (id) => clearTimeout(id);
    }
    ca = window.cancelAnimationFrame ;
    ra = window.requestAnimationFrame ;
}
var cancelAnimationFrame  = ca;
var requestAnimationFrame = ra;

function FrameTimer() {
    Task.call(this);
    Object.defineProperties(this, {
        deltaTime: { value: 1, writable: true },
        elapsedMS: { value: 1 / FPMS, writable: true },
        fps: { get: function get() {
                return 1000 / this.elapsedMS;
            } },
        minFPS: {
            get: function get() {
                return 1000 / this._maxElapsedMS;
            },
            set: function set(fps) {
                this._maxElapsedMS = 1 / Math.min(Math.max(0, fps) / 1000, FPMS);
            }
        },
        lastTime: { value: 0, writable: true },
        speed: { value: 1, writable: true },
        _requestID: { value: null, writable: true },
        _maxElapsedMS: { value: 100, writable: true },
        _stopped: { value: false, writable: true }
    });
}
FrameTimer.prototype = Object.create(Task.prototype, {
    constructor: { value: FrameTimer, writable: true },
    stopped: { get: function get() {
            return this._stopped;
        } },
    clone: { value: function value() {
            return new FrameTimer();
        } },
    resume: { value: function value() {
            if (this._stopped) {
                this._running = true;
                this._stopped = false;
                this.notifyResumed();
                this._requestID = requestAnimationFrame(this._next.bind(this));
            }
        } },
    reset: { value: function value() {
            this.stop();
            this._stopped = false;
        } },
    run: { value: function value() {
            if (!this._running) {
                this._stopped = false;
                this.lastTime = performance.now();
                this.notifyStarted();
                this._requestID = requestAnimationFrame(this._next.bind(this));
            }
        } },
    stop: { value: function value() {
            if (this._running && !this._stopped) {
                this._running = false;
                this._stopped = true;
                cancelAnimationFrame(this._requestID);
                this._requestID = null;
                this.notifyStopped();
            }
        } },
    toString: { value: function value() {
            return '[FrameTimer]';
        } },
    _next: { value: function value() {
            var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : performance.now();
            if (this._requestID !== null && (this._stopped || !this._running)) {
                cancelAnimationFrame(this._requestID);
                this._requestID = null;
                return;
            }
            var elapsedMS = void 0;
            if (time > this.lastTime) {
                elapsedMS = this.elapsedMS = time - this.lastTime;
                if (elapsedMS > this._maxElapsedMS) {
                    elapsedMS = this._maxElapsedMS;
                }
                this.deltaTime = elapsedMS * FPMS * this.speed;
                this.notifyProgress();
            } else {
                this.deltaTime = this.elapsedMS = 0;
            }
            this.lastTime = time;
            this._requestID = requestAnimationFrame(this._next.bind(this));
        } }
});
var FPMS = 0.06;

function Lock(target) {
    Action.call(this);
    this.target = target;
}
Lock.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: Lock },
    clone: { writable: true, value: function value() {
            return new Lock(this.target);
        } },
    run: { writable: true, value: function value() {
            this.notifyStarted();
            if (isLockable(this.target) && !this.target.isLocked()) {
                this.target.lock();
            }
            this.notifyFinished();
        } }
});

function Priority() {
    Object.defineProperties(this, {
        priority: {
            get: function get() {
                return this._priority;
            },
            set: function set(value) {
                this._priority = value > 0 || value < 0 ? value : 0;
            }
        },
        _priority: { writable: true, value: 0 }
    });
}
Priority.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Priority },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function isResetable(target) {
    if (target) {
        if (target instanceof Resetable) {
            return true;
        }
        return Boolean(target['reset']) && target.reset instanceof Function;
    }
    return false;
}
function Resetable() {}
Resetable.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Resetable },
    reset: { writable: true, value: function value() {} },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function isResumable(target) {
    if (target) {
        if (target instanceof Resumable) {
            return true;
        }
        return Boolean(target['resume']) && target.resume instanceof Function;
    }
    return false;
}
function Resumable() {}
Resumable.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Resumable },
    resume: { writable: true, value: function value() {} },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function isStartable(target) {
    if (target) {
        if (target instanceof Startable) {
            return true;
        }
        return Boolean(target['start']) && target.start instanceof Function;
    }
    return false;
}
function Startable() {}
Startable.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Startable },
    start: { writable: true, value: function value() {} },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function isStoppable(target) {
    if (target) {
        if (target instanceof Stoppable) {
            return true;
        }
        return Boolean(target['stop']) && target.stop instanceof Function;
    }
    return false;
}
function Stoppable() {}
Stoppable.prototype = Object.create(Object.prototype, {
    constructor: { writable: true, value: Stoppable },
    stop: { writable: true, value: function value() {} },
    toString: { writable: true, value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function TimeoutPolicy(value, name) {
  Enum.call(this, value, name);
}
TimeoutPolicy.prototype = Object.create(Enum.prototype);
TimeoutPolicy.prototype.constructor = TimeoutPolicy;
Object.defineProperties(TimeoutPolicy, {
  INFINITY: { value: new TimeoutPolicy(0, 'infinity'), enumerable: true },
  LIMIT: { value: new TimeoutPolicy(1, 'limit'), enumerable: true }
});

function Timer() {
    var delay = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    var repeatCount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var useSeconds = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    Task.call(this);
    Object.defineProperties(this, {
        _count: { value: 0, writable: true },
        _delay: { value: delay > 0 ? delay : 0, writable: true },
        _itv: { value: 0, writable: true },
        _repeatCount: { value: repeatCount > 0 ? repeatCount : 0, writable: true },
        _stopped: { value: false, writable: true },
        _useSeconds: { value: Boolean(useSeconds), writable: true }
    });
}
Timer.prototype = Object.create(Task.prototype, {
    constructor: { value: Timer, writable: true },
    currentCount: { get: function get() {
            return this._count;
        } },
    delay: {
        get: function get() {
            return this._delay;
        },
        set: function set(value) {
            if (this._running) {
                throw new Error(this + " the 'delay' property can't be changed during the running phase.");
            }
            this._delay = value > 0 ? value : 0;
        }
    },
    repeatCount: {
        get: function get() {
            return this._repeatCount;
        },
        set: function set(value) {
            this._repeatCount = value > 0 ? value : 0;
        }
    },
    stopped: { get: function get() {
            return this._stopped;
        } },
    useSeconds: {
        get: function get() {
            return this._useSeconds;
        },
        set: function set(flag) {
            if (this._running) {
                throw new Error(this + " the 'useSeconds' property can't be changed during the running phase.");
            }
            this._useSeconds = Boolean(flag);
        }
    },
    clone: { value: function value() {
            return new Timer(this._delay, this._repeatCount);
        } },
    resume: { value: function value() {
            if (this._stopped) {
                this._running = true;
                this._stopped = false;
                this._itv = setInterval(this._next.bind(this), this._useSeconds ? this._delay * 1000 : this._delay);
                this.notifyResumed();
            }
        } },
    reset: { value: function value() {
            if (this.running) {
                this.stop();
            }
            this._count = 0;
        } },
    run: { value: function value() {
            if (!this._running) {
                this._count = 0;
                this._stopped = false;
                this.notifyStarted();
                this._itv = setInterval(this._next.bind(this), this._useSeconds ? this._delay * 1000 : this._delay);
            }
        } },
    stop: { value: function value() {
            if (this._running && !this._stopped) {
                this._running = false;
                this._stopped = true;
                clearInterval(this._itv);
                this.notifyStopped();
            }
        } },
    _next: { value: function value() {
            this._count++;
            this.notifyProgress();
            if (this._repeatCount > 0 && this._repeatCount === this._count) {
                clearInterval(this._itv);
                this.notifyFinished();
            }
        } }
});

function Unlock(target) {
    Action.call(this);
    this.target = target;
}
Unlock.prototype = Object.create(Action.prototype, {
    constructor: { writable: true, value: Unlock },
    clone: { writable: true, value: function value() {
            return new Unlock(this.target);
        } },
    run: { writable: true, value: function value() {
            this.notifyStarted();
            if (isLockable(this.target) && this.target.isLocked()) {
                this.target.unlock();
            }
            this.notifyFinished();
        } }
});

/**
 * The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
 * @summary The {@link system.process} library allow you to create and manage asynchronous operations in your applications.
 * @namespace system.process
 * @memberof system
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 */
var process = Object.assign({
    isLockable: isLockable,
    isResetable: isResetable,
    isResumable: isResumable,
    isRunnable: isRunnable,
    isStartable: isStartable,
    isStoppable: isStoppable,
    Action: Action,
    ActionEntry: ActionEntry,
    Apply: Apply,
    Batch: Batch,
    BatchTask: BatchTask,
    Cache: Cache,
    Call: Call,
    Chain: Chain,
    Do: Do,
    FrameTimer: FrameTimer,
    Lock: Lock,
    Lockable: Lockable,
    Priority: Priority,
    Resetable: Resetable,
    Resumable: Resumable,
    Runnable: Runnable,
    Startable: Startable,
    Stoppable: Stoppable,
    Task: Task,
    TaskGroup: TaskGroup,
    TaskPhase: TaskPhase,
    TimeoutPolicy: TimeoutPolicy,
    Timer: Timer,
    Unlock: Unlock
});

function And(rule1         , rule2         ) {
    Object.defineProperties(this, {
        rules: { value: [], enumerable: true },
        length: { get: function get() {
                return this.rules instanceof Array ? this.rules.length : 0;
            } }
    });
    if (!(rule1 instanceof Rule) || !(rule2 instanceof Rule)) {
        throw new ReferenceError(this + ' constructor failed, the two rules in argument must be defined.');
    }
    this.add(rule1);
    this.add(rule2);
    for (var _len = arguments.length, rules = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
        rules[_key - 2] = arguments[_key];
    }
    if (rules && rules.length > 0) {
        var len = rules.length;
        for (var i = 0; i < len; i++) {
            if (rules[i] instanceof Rule) {
                this.add(rules[i]);
            }
        }
    }
}
And.prototype = Object.create(Rule.prototype);
And.prototype.constructor = And;
And.prototype.add = function (rule) {
    if (rule instanceof Rule) {
        this.rules.push(rule);
    }
    return this;
};
And.prototype.clear = function () {
    this.rules.length = 0;
    return this;
};
And.prototype.eval = function () {
    if (this.rules.length > 0) {
        var b = this.rules[0].eval();
        var l = this.rules.length;
        for (var i = 1; i < l; i++) {
            b = b && this.rules[i].eval();
        }
        return b;
    } else {
        return false;
    }
};

function DivBy() {
  var value1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var value2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NaN;
  this.value1 = value1;
  this.value2 = value2;
}
DivBy.prototype = Object.create(Rule.prototype);
DivBy.prototype.constructor = DivBy;
DivBy.prototype.eval = function () {
  return this.value1 % this.value2 === 0;
};

function Even() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  this.value = value;
}
Even.prototype = Object.create(Rule.prototype);
Even.prototype.constructor = Even;
Even.prototype.eval = function () {
  return this.value % 2 === 0;
};

function GreaterOrEqualsThan() {
  var value1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var value2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NaN;
  this.value1 = value1;
  this.value2 = value2;
}
GreaterOrEqualsThan.prototype = Object.create(Rule.prototype);
GreaterOrEqualsThan.prototype.constructor = GreaterOrEqualsThan;
GreaterOrEqualsThan.prototype.eval = function () {
  return this.value1 >= this.value2;
};

function GreaterThan() {
  var value1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var value2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NaN;
  this.value1 = value1;
  this.value2 = value2;
}
GreaterThan.prototype = Object.create(Rule.prototype);
GreaterThan.prototype.constructor = GreaterThan;
GreaterThan.prototype.eval = function () {
  return this.value1 > this.value2;
};

function IsBoolean() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  this.value = value;
}
IsBoolean.prototype = Object.create(Rule.prototype);
IsBoolean.prototype.constructor = IsBoolean;
IsBoolean.prototype.eval = function () {
  return typeof this.value === 'boolean' || this.value instanceof Boolean;
};

function IsNaN() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var strict = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
  this.value = value;
  this.strict = Boolean(strict);
}
IsNaN.prototype = Object.create(Rule.prototype);
IsNaN.prototype.constructor = IsNaN;
IsNaN.prototype.eval = function () {
  if (this.strict) {
    return isNaN(this.value);
  } else {
    return !(this.value instanceof Number || typeof this.value === 'number') || isNaN(this.value);
  }
};

function IsNumber() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  this.value = value;
}
IsNumber.prototype = Object.create(Rule.prototype);
IsNumber.prototype.constructor = IsNumber;
IsNumber.prototype.eval = function () {
  return typeof this.value === 'number' || this.value instanceof Number;
};

function IsString() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  this.value = value;
}
IsString.prototype = Object.create(Rule.prototype);
IsString.prototype.constructor = IsString;
IsString.prototype.eval = function () {
  return typeof this.value === 'string' || this.value instanceof String;
};

function LessOrEqualsThan() {
  var value1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var value2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NaN;
  this.value1 = value1;
  this.value2 = value2;
}
LessOrEqualsThan.prototype = Object.create(Rule.prototype);
LessOrEqualsThan.prototype.constructor = LessOrEqualsThan;
LessOrEqualsThan.prototype.eval = function () {
  return this.value1 <= this.value2;
};

function LessThan() {
  var value1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  var value2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : NaN;
  this.value1 = value1;
  this.value2 = value2;
}
LessThan.prototype = Object.create(Rule.prototype);
LessThan.prototype.constructor = LessThan;
LessThan.prototype.eval = function () {
  return this.value1 < this.value2;
};

function Not() {
  var condition = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  this.condition = condition;
}
Not.prototype = Object.create(Rule.prototype);
Not.prototype.constructor = Not;
Not.prototype.eval = function () {
  return !(this.condition instanceof Rule ? this.condition.eval() : Boolean(this.condition));
};

function NotEquals() {
    var value1 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var value2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    this.value1 = value1;
    this.value2 = value2;
}
NotEquals.prototype = Object.create(Rule.prototype);
NotEquals.prototype.constructor = NotEquals;
NotEquals.prototype.eval = function () {
    if (this.value1 === this.value2) {
        return false;
    } else if (this.value1 instanceof Rule && this.value2 instanceof Rule) {
        return this.value1.eval() !== this.value2.eval();
    } else if (isEquatable(this.value1)) {
        return !this.value1.equals(this.value2);
    } else {
        return true;
    }
};

function Odd() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : NaN;
  this.value = value;
}
Odd.prototype = Object.create(Rule.prototype);
Odd.prototype.constructor = Odd;
Odd.prototype.eval = function () {
  return this.value % 2 !== 0;
};

function Or(rule1         , rule2         ) {
    Object.defineProperties(this, {
        rules: { value: [], enumerable: true },
        length: { get: function get() {
                return this.rules instanceof Array ? this.rules.length : 0;
            } }
    });
    if (!(rule1 instanceof Rule) || !(rule2 instanceof Rule)) {
        throw new ReferenceError(this + ' constructor failed, the two rules in argument must be defined.');
    }
    this.add(rule1);
    this.add(rule2);
    for (var _len = arguments.length, rules = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
        rules[_key - 2] = arguments[_key];
    }
    if (rules && rules.length > 0) {
        var len = rules.length;
        for (var i = 0; i < len; i++) {
            if (rules[i] instanceof Rule) {
                this.add(rules[i]);
            }
        }
    }
}
Or.prototype = Object.create(Rule.prototype);
Or.prototype.constructor = Or;
Or.prototype.add = function (rule) {
    if (rule instanceof Rule) {
        this.rules.push(rule);
    }
    return this;
};
Or.prototype.clear = function () {
    this.rules.length = 0;
    return this;
};
Or.prototype.eval = function () {
    if (this.rules.length > 0) {
        var b = this.rules[0].eval();
        var l = this.rules.length;
        for (var i = 1; i < l; i++) {
            b = b || this.rules[i].eval();
        }
        return b;
    } else {
        return false;
    }
};

/**
 * The {@link system.rules} library defines a set of functions and classes to evaluate some basic or complex conditions in your applications.
 * @summary The {@link system.rules} library defines a set of functions and classes to evaluate some basic or complex conditions in your applications.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.rules
 * @memberof system
 */
var rules = Object.assign({
    isRule: isRule,
    And: And,
    BooleanRule: BooleanRule,
    DivBy: DivBy,
    EmptyString: EmptyString,
    Equals: Equals,
    Even: Even,
    False: False,
    GreaterOrEqualsThan: GreaterOrEqualsThan,
    GreaterThan: GreaterThan,
    IsBoolean: IsBoolean,
    IsNaN: IsNaN,
    IsNumber: IsNumber,
    IsString: IsString,
    LessOrEqualsThan: LessOrEqualsThan,
    LessThan: LessThan,
    Odd: Odd,
    Not: Not,
    NotEquals: NotEquals,
    Null: Null,
    Or: Or,
    Rule: Rule,
    True: True,
    Undefined: Undefined,
    Zero: Zero
});

/**
 * The {@link system.signals} library is light-weight, strongly-typed messaging tools. Wire your application with better APIs and less boilerplate than W3C DOMEvents..
 * <p><b>Concept: </b>
 * <ul>
 * <li>A Signal is essentially a minimal emitter specific to one event, with its own <code>array</code> of receivers/slots ({@link system.signals.Receiver|Receiver} or <code>Function</code>).</li>
 * <li>A Signal gives an event a concrete membership in a class.</li>
 * <li>Receivers subscribe to real objects, not to string-based channels.</li>
 * <li>Event string constants are no longer needed.</li>
 * <li>Signals are inspired by {@link https://en.wikipedia.org/wiki/Signals_and_slots|signals/slots in Qt}.</li>
 * <ul>
 * @summary The {@link system.signals} library is light-weight, strongly-typed messaging tools.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.signals
 * @memberof system
 * @example
 * function Slot( name )
 * {
 *     this.name = name ;
 * }
 *
 * Slot.prototype = Object.create( system.signals.Receiver.prototype );
 * Slot.prototype.constructor = Slot;
 *
 * Slot.prototype.receive = function ( message )
 * {
 *     trace( this + " : " + message ) ;
 * }
 *
 * Slot.prototype.toString = function ()
 * {
 *     return "[Slot name:" + this.name + "]" ;
 * }
 *
 * var slot1 = new Slot("slot1") ;
 *
 * var slot2 = function( message )
 * {
 *     trace( this + " : " + message ) ;
 * }
 *
 * var signal = new system.signals.Signal() ;
 *
 * //signal.proxy = slot1 ;
 *
 * signal.connect( slot1 , 0 ) ;
 * signal.connect( slot2 , 2 ) ;
 *
 * signal.emit( "hello world" ) ;
 */
var signals = Object.assign({
  Receiver: Receiver,
  SignalEntry: SignalEntry,
  Signaler: Signaler,
  Signal: Signal
});

function MotionNextFrame(motion) {
    this.motion = motion instanceof Motion ? motion : null;
}
MotionNextFrame.prototype = Object.create(Receiver.prototype, {
    constructor: { value: MotionNextFrame },
    receive: { value: function value() {
            if (this.motion) {
                this.motion.setTime(this.motion.useSeconds ? (performance$1.now() - this.motion._startTime) / 1000 : this.motion._time + 1);
            }
        } }
});

function Transition() {
    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    Task.call(this);
    Object.defineProperties(this, {
        _id: { value: id, writable: true }
    });
}
Transition.prototype = Object.create(Task.prototype, {
    constructor: { value: Transition, writable: true },
    id: {
        get: function get() {
            return this._id;
        },
        set: function set(value) {
            this._id = value;
        }
    },
    clone: { writable: true, value: function value() {
            return new Transition(this.id);
        } },
    equals: { writable: true, value: function value(o) {
            if (o === this) {
                return true;
            } else if (o && o instanceof Transition) {
                return o.id === this.id;
            } else {
                return false;
            }
        } },
    toString: { value: function value() {
            return '[' + this.constructor.name + ']';
        } }
});

function Motion() {
    var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    Transition.call(this, id);
    Object.defineProperties(this, {
        useSeconds: { writable: true, value: false },
        _duration: { writable: true, value: 0 },
        _fps: { writable: true, value: NaN },
        _nextFrame: { value: new MotionNextFrame(this) },
        _prevTime: { writable: true, value: NaN },
        _startTime: { writable: true, value: NaN },
        _stopped: { writable: true, value: false },
        _target: { writable: true, value: null },
        _time: { writable: true, value: NaN },
        _timer: { writable: true, value: null }
    });
    this.setTimer(new FrameTimer());
}
Motion.prototype = Object.create(Transition.prototype, {
    constructor: { value: Motion, writable: true },
    duration: {
        get: function get() {
            return this._duration;
        },
        set: function set(value) {
            this._duration = isNaN(value) || value <= 0 ? 0 : value;
        }
    },
    fps: {
        get: function get() {
            return this._fps;
        },
        set: function set(value) {
            if (this._timer && this._timer._running) {
                this._timer.stop();
            }
            this._fps = value > 0 ? value : NaN;
            if (isNaN(this._fps)) {
                this.setTimer(new FrameTimer());
            } else {
                this.setTimer(new Timer(1000 / this._fps));
            }
        }
    },
    prevTime: {
        get: function get() {
            return this._prevTime;
        }
    },
    stopped: {
        get: function get() {
            return this._stopped;
        }
    },
    target: {
        get: function get() {
            return this._target;
        },
        set: function set(value) {
            this._target = value;
        }
    },
    clone: { writable: true, value: function value() {
            return new Motion(this.id);
        } },
    nextFrame: { value: function value() {
            this.setTime(this.useSeconds ? (performance$1.now() - this._startTime) / 1000 : this._time + 1);
        } },
    prevFrame: { value: function value() {
            if (!this.useSeconds) {
                this.setTime(this._time - 1);
            }
        } },
    resume: { writable: true, value: function value() {
            if (this._stopped && this._time !== this._duration) {
                this._stopped = false;
                this.fixTime();
                this.startInterval();
                this.notifyResumed();
            } else {
                this.run();
            }
        } },
    rewind: { value: function value() {
            var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            this._time = time > 0 ? time : 0;
            this.fixTime();
            this.update();
        } },
    run: { writable: true, value: function value() {
            this._stopped = false;
            this.notifyStarted();
            this.rewind();
            this.startInterval();
        } },
    setTime: { value: function value(time) {
            this._prevTime = this._time;
            if (time > this._duration) {
                time = this._duration;
                if (this.looping) {
                    this.rewind(time - this._duration);
                    this.notifyLooped();
                } else {
                    if (this.useSeconds) {
                        this._time = this._duration;
                        this.update();
                    }
                    this.stop();
                    this.notifyFinished();
                }
            } else if (time < 0) {
                this.rewind();
            } else {
                this._time = time;
                this.update();
            }
        } },
    startInterval: { value: function value() {
            this._timer.start();
            this._running = true;
        } },
    stop: { value: function value() {
            if (this._running) {
                this.stopInterval();
                this._stopped = true;
                this.notifyStopped();
            }
        } },
    stopInterval: { value: function value() {
            this._timer.stop();
            this._running = false;
        } },
    update: { writable: true, value: function value() {
        } },
    fixTime: { value: function value() {
            if (this.useSeconds) {
                this._startTime = performance$1.now() - this._time * 1000;
            }
        } },
    setTimer: { value: function value(_value) {
            if (this._timer) {
                if (this._timer instanceof Task) {
                    if (this._timer._running) {
                        this._timer.stop();
                    }
                    this._timer.progressIt.disconnect(this._nextFrame);
                }
                this._timer = null;
            }
            this._timer = _value instanceof Task ? _value : new Timer();
            if (this._timer) {
                this._timer.progressIt.connect(this._nextFrame);
            }
        } }
});

var linear = ( t , b , c , d ) => ( c * t / d ) + b;

function TweenUnit() {
    var easing = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
    var useSeconds = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var auto = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    var id = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
    Motion.call(this, id);
    Object.defineProperties(this, {
        position: { writable: true, value: 0 },
        _change: { writable: true, value: 1 },
        _easing: { writable: true, value: easing instanceof Function ? easing : linear }
    });
    this.duration = duration;
    this.useSeconds = useSeconds;
    if (auto) {
        this.run();
    }
}
TweenUnit.prototype = Object.create(Motion.prototype, {
    constructor: { value: TweenUnit, writable: true },
    easing: {
        get: function get() {
            return this._easing;
        },
        set: function set(value) {
            this._easing = value instanceof Function ? value : linear;
        }
    },
    clone: { writable: true, value: function value() {
            return new TweenUnit(this.easing, this.duration, this.useSeconds);
        } },
    set: { value: function value(easing) {
            var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
            var useSeconds = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
            this.duration = duration;
            this.useSeconds = useSeconds;
            this.easing = easing;
        } },
    update: { writable: true, value: function value() {
            if (this._easing) {
                this.position = this._easing(this._time, 0, this._change, this._duration);
                this.notifyChanged();
            } else {
                this.position = null;
            }
        } }
});

function Tween(init) {
    TweenUnit.call(this);
    this.position = null;
    Object.defineProperties(this, {
        _begin: { writable: true, value: null },
        _changed: { writable: true, value: false },
        _easings: { writable: true, value: null },
        _from: { writable: true, value: null },
        _target: { writable: true, value: null },
        _to: { writable: true, value: null }
    });
    if (init) {
        for (var prop in init) {
            if (prop in this) {
                this[prop] = init[prop];
            }
        }
        if ('auto' in init && init.auto === true) {
            this.run();
        }
    }
}
Tween.prototype = Object.create(TweenUnit.prototype, {
    constructor: { value: TweenUnit, writable: true },
    easings: {
        get: function get() {
            return this._easings;
        },
        set: function set(value) {
            this._easings = value;
        }
    },
    from: {
        get: function get() {
            return this._from;
        },
        set: function set(value) {
            this._from = value;
            this._changed = true;
        }
    },
    target: {
        get: function get() {
            return this._target;
        },
        set: function set(value) {
            this._target = value;
            this._changed = true;
        }
    },
    to: {
        get: function get() {
            return this._to;
        },
        set: function set(value) {
            this._to = value;
            this._changed = true;
        }
    },
    clone: { writable: true, value: function value() {
            return new Tween({
                duration: this.duration,
                easing: this.easing,
                easings: this.easings,
                from: this.from,
                target: this.target,
                to: this.to,
                useSeconds: this.useSeconds
            });
        } },
    notifyFinished: { value: function value() {
            this._changed = true;
            this._running = false;
            this._phase = TaskPhase.FINISHED;
            this.finishIt.emit(this);
            this._phase = TaskPhase.INACTIVE;
        } },
    run: { writable: true, value: function value() {
            var to = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
            if (to) {
                this.to = to;
            }
            this._changed = true;
            this._stopped = false;
            this.position = null;
            this.notifyStarted();
            this.rewind();
            this.startInterval();
        } },
    update: { writable: true, value: function value() {
            if (this._changed) {
                this._changed = false;
                if (!this._target) {
                    throw new Error(this + " update failed, the 'target' property not must be null.");
                }
                if (!this._to) {
                    throw new Error(this + " update failed, the 'to' property not must be null.");
                }
                if (this._from) {
                    this._begin = this._from;
                } else {
                    this._begin = {};
                    for (var prop in this._to) {
                        if (prop in this._target) {
                            this._begin[prop] = this._target[prop];
                        }
                    }
                }
            }
            this.position = {};
            for (var _prop in this._to) {
                if (_prop in this._target) {
                    var e = this._easings && _prop in this._easings && this.easings[_prop] instanceof Function ? this.easings[_prop] : this._easing;
                    this._target[_prop] = this.position[_prop] = e(this._time, this._begin[_prop], this._to[_prop] - this._begin[_prop], this._duration);
                }
            }
            this.notifyChanged();
        } }
});

/**
 * The {@link system.transitions} library is a simple animations toolkit to use in your projects, your games, your websites.
 * @summary The {@link system.transitions} library is a simple animations toolkit to use in your projects, your games, your websites.
 * @namespace system.transitions
 * @memberof system
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @see For more usage, read the {@tutorial system.transitions} tutorial.
 * @example <caption>Javascript script</caption>
 * "use strict" ;
 *
 * window.onload = function()
 * {
 *     if( !vegas )
 *     {
 *         throw new Error( "The VEGAS library is not found." ) ;
 *     }
 *
 *     // ----- imports
 *
 *     var global   = vegas.global ; // jshint ignore:line
 *     var trace    = vegas.trace  ; // jshint ignore:line
 *     var core     = vegas.core   ; // jshint ignore:line
 *     var system   = vegas.system ; // jshint ignore:line
 *
 *     var Tween = system.transitions.Tween ;
 *
 *     // ----- behaviors
 *
 *     var change = function( tween )
 *     {
 *         trace( 'progress ' + core.dump(tween.target) ) ;
 *         render() ;
 *     }
 *
 *     var finish = function()
 *     {
 *         trace( 'finish' ) ;
 *         // tween.duration = 120 ;
 *         // tween.from = null ;
 *         // tween.to   = tween.to === to ? from : to ;
 *         // tween.run() ;
 *     }
 *
 *     var start = function()
 *     {
 *         trace( 'start' ) ;
 *     }
 *
 *     // ----- initialize
 *
 *     var canvas  = document.getElementById('canvas') ;
 *     var context = canvas.getContext('2d');
 *
 *     canvas.width  = 800;
 *     canvas.height = 600;
 *
 *     var color   = '#FF0000' ;
 *     var radius  = 25;
 *
 *     var from    = { x : 100 , y : 100 } ;
 *     var to      = { x : 500 , y : 400 } ;
 *     var target  = { x : 0   , y : 0 } ;
 *
 *     var easings = null ;
 *
 *     easings = { x : core.easings.backOut , y : core.easings.sineOut } ;
 *
 *     var tween = new Tween
 *     ({
 *         auto       : false,
 *         duration   : 48 ,
 *         useSeconds : false ,
 *         easing     : core.easings.backOut,
 *         easings    : easings,
 *         from       : from ,
 *         target     : target ,
 *         to         : to
 *     }) ;
 *
 *     //tween.easing = core.easings.cubicOut ;
 *     //tween.easing = core.easings.elasticOut ;
 *     //tween.easing = core.easings.sineOut ;
 *
 *     // tween.fps = 60  ; // use an internal Timer instance or a FrameTimer instance if fps is NaN
 *
 *     tween.looping = true ;
 *
 *     tween.finishIt.connect( finish ) ;
 *     tween.changeIt.connect( change ) ;
 *     tween.startIt.connect( start ) ;
 *
 *     // ----- render
 *
 *     var render = function()
 *     {
 *         var width  = canvas.width ;
 *         var height = canvas.height ;
 *
 *         context.clearRect(0, 0, width, height);
 *
 *         context.fillStyle = '#333333' ;
 *         context.fillRect(0, 0, width, height );
 *
 *         context.beginPath();
 *         context.arc( target.x, target.y, radius, 0, Math.PI * 2, false );
 *         context.closePath();
 *         context.fillStyle = color ;
 *         context.fill();
 *     }
 *
 *     render() ;
 *
 *     tween.run() ;
 * }
 */
var transitions = Object.assign({
  Motion: Motion,
  Transition: Transition,
  Tween: Tween,
  TweenUnit: TweenUnit
});

var version = '1.0.0';
var metas = Object.defineProperties({}, {
  name: { enumerable: true, value: ucFirst('vegas-js-system') },
  description: { enumerable: true, value: "The system package is the root for the VEGAS JS framework. It is the starting point of our RIA framework structure : signals, W3C events, datas and collections (ADT), IoC container (Dependency Injection), logger, tasks, transitions, logics, rules, models, etc." },
  version: { enumerable: true, value: version },
  license: { enumerable: true, value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+" },
  url: { enumerable: true, value: 'https://bitbucket.org/ekameleon/vegas-js-system' }
});

exports.version = version;
exports.metas = metas;
exports.Enum = Enum;
exports.Equatable = Equatable;
exports.Evaluable = Evaluable;
exports.Formattable = Formattable;
exports.isEvaluable = isEvaluable;
exports.isFormattable = isFormattable;
exports.data = data;
exports.errors = errors;
exports.evaluators = evaluators;
exports.events = events;
exports.formatters = formatters;
exports.ioc = ioc;
exports.logging = logging;
exports.logics = logics;
exports.models = models;
exports.numeric = numeric;
exports.process = process;
exports.rules = rules;
exports.signals = signals;
exports.transitions = transitions;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=vegas-system.js.map
